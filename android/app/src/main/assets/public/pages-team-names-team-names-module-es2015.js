(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-team-names-team-names-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/team-names/team-names.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/team-names/team-names.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      <span *ngIf=\"teamId == 1\">{{ 'NAME_HEIM_MANNSCHAFT' | translate }}</span>\n      <span *ngIf=\"teamId == 2\">{{ 'NAME_GAST_MANNSCHAFT' | translate }}</span>\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <!-- <p>\n    <button (click)='back()'>zurück</button>\n  </p> -->\n  <form [formGroup]=\"form\">\n    <ion-row>\n      <ion-col size=\"12\">\n        <div class=\"ion-text-end\">\n        <ion-item>\n          <ion-input type=\"text\" formControlName=\"teamName\" maxlength=\"8\"></ion-input>\n        </ion-item>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row >\n      <ion-col size=\"12\">\n        <div class=\"ion-text-end\">\n          <button (click)='save()' [disabled]=\"!form.valid\" title=\"{{ 'SPEICHERN' | translate }}\">\n            <ion-icon name=\"checkmark\" size=\"large\"></ion-icon>\n          </button>       \n          <button (click)='cancel()' title=\"{{ 'ABBRUCH' | translate }}\">\n            <ion-icon name=\"close\" size=\"large\"></ion-icon>\n          </button>\n        </div>\n      </ion-col>\n    </ion-row>\n  </form>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/team-names/team-names-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/team-names/team-names-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: TeamNamesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamNamesPageRoutingModule", function() { return TeamNamesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _team_names_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./team-names.page */ "./src/app/pages/team-names/team-names.page.ts");




const routes = [
    {
        path: '',
        component: _team_names_page__WEBPACK_IMPORTED_MODULE_3__["TeamNamesPage"]
    }
];
let TeamNamesPageRoutingModule = class TeamNamesPageRoutingModule {
};
TeamNamesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TeamNamesPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/team-names/team-names.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/team-names/team-names.module.ts ***!
  \*******************************************************/
/*! exports provided: TeamNamesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamNamesPageModule", function() { return TeamNamesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _team_names_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./team-names-routing.module */ "./src/app/pages/team-names/team-names-routing.module.ts");
/* harmony import */ var _team_names_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./team-names.page */ "./src/app/pages/team-names/team-names.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let TeamNamesPageModule = class TeamNamesPageModule {
};
TeamNamesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _team_names_routing_module__WEBPACK_IMPORTED_MODULE_5__["TeamNamesPageRoutingModule"]
        ],
        declarations: [_team_names_page__WEBPACK_IMPORTED_MODULE_6__["TeamNamesPage"]]
    })
], TeamNamesPageModule);



/***/ }),

/***/ "./src/app/pages/team-names/team-names.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/team-names/team-names.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RlYW0tbmFtZXMvdGVhbS1uYW1lcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/team-names/team-names.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/team-names/team-names.page.ts ***!
  \*****************************************************/
/*! exports provided: TeamNamesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamNamesPage", function() { return TeamNamesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/model/general-settings */ "./src/app/shared/model/general-settings.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_shared_service_current_game_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/service/current-game.service */ "./src/app/shared/service/current-game.service.ts");
/* harmony import */ var src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/service/protocol.service */ "./src/app/shared/service/protocol.service.ts");
/* harmony import */ var src_app_shared_service_debug_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/service/debug.service */ "./src/app/shared/service/debug.service.ts");
/* harmony import */ var src_app_shared_service_data_storage_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/service/data-storage.service */ "./src/app/shared/service/data-storage.service.ts");
/* harmony import */ var src_app_shared_service_settings_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/shared/service/settings.service */ "./src/app/shared/service/settings.service.ts");










let TeamNamesPage = class TeamNamesPage {
    constructor(formBuilder, router, currentGameService, route, protocolService, debugService, dataStorageService, settingsService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.currentGameService = currentGameService;
        this.route = route;
        this.protocolService = protocolService;
        this.debugService = debugService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
        // this.form = this.formBuilder.group({
        //   teamName: ['', Validators.required]
        // });
        this.form = this.formBuilder.group({
            teamName: ['']
        });
    }
    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.teamId = params.id;
        });
        this.debugService.getdata().subscribe((data) => { this.debugSettings = data; });
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    ionViewWillEnter() {
        this.subscription = this.currentGameService.getdata().subscribe(data => {
            this.currentGame = data;
            if (this.teamId == 2) {
                this.form.controls['teamName'].setValue(this.settingsService.teamNames.guestTeamName);
            }
            else {
                this.form.controls['teamName'].setValue(this.settingsService.teamNames.homeTeamName);
            }
        });
    }
    save() {
        if (this.teamId == 2) {
            this.currentGame.guestTeamName = this.form.controls['teamName'].value;
        }
        else {
            this.currentGame.homeTeamName = this.form.controls['teamName'].value;
        }
        let teamNames = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TeamNames"]();
        teamNames.homeTeamName = this.currentGame.homeTeamName;
        teamNames.guestTeamName = this.currentGame.guestTeamName;
        //this.dataStorageService.setString('HasTimeTransfer', teamNames.homeTeamName);
        this.currentGameService.changeCurrentGameWithDependencies(this.currentGame);
        if (this.debugSettings.debugProtokolWorkflow) {
            debugger;
        }
        this.protocolService.prot29h(teamNames);
        //console.log(this.form.value);
        this.router.navigate(['play']);
    }
    cancel() {
        this.router.navigate(['play']);
    }
    back() {
        this.router.navigate(['play']);
    }
    logForm() {
    }
};
TeamNamesPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_shared_service_current_game_service__WEBPACK_IMPORTED_MODULE_5__["CurrentGameService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_6__["ProtocolService"] },
    { type: src_app_shared_service_debug_service__WEBPACK_IMPORTED_MODULE_7__["DebugService"] },
    { type: src_app_shared_service_data_storage_service__WEBPACK_IMPORTED_MODULE_8__["DataStorageService"] },
    { type: src_app_shared_service_settings_service__WEBPACK_IMPORTED_MODULE_9__["SettingsService"] }
];
TeamNamesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-team-names',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./team-names.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/team-names/team-names.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./team-names.page.scss */ "./src/app/pages/team-names/team-names.page.scss")).default]
    })
], TeamNamesPage);



/***/ })

}]);
//# sourceMappingURL=pages-team-names-team-names-module-es2015.js.map