function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-play-play-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/play/play.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/play/play.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesPlayPlayPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      <span *ngIf=\"isFootball()\">{{ 'FUSSBALL' | translate }}</span>\r\n      <span *ngIf=\"isHockey()\">{{ 'HOCKEY' | translate }}</span>\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-grid>\r\n      <ion-row>\r\n      <ion-col size=\"12\" class=\"\">\r\n        <div class=\"ion-text-center button-style\" >\r\n          <button (click)='manualChange()'  [disabled]=\"manualChangeDisabled()\" class=\"button-timer-style\">\r\n            <span class=\"minuteCounter count-big\">{{getSportReleateMinutes() | number:'2.0' }}</span>\r\n            <span *ngIf=\"gameIsRunning()\" class=\"running count-big\">:</span>\r\n            <span *ngIf=\"!gameIsRunning()\" class=\"pause count-big\">:</span>\r\n            <span class=\"secCounter count-big\">{{currentGameService.currentGame.currentSec  | number:'2.0'}} </span>\r\n          </button>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"\">\r\n        <div class=\"ion-text-center\">\r\n          <span *ngIf=\"isFootballNormal()\">{{ 'HALBZEIT' | translate }}</span>\r\n          <span *ngIf=\"isFootballReneval()\">{{ 'VERLAENGERUNG' | translate }}</span>\r\n          <span *ngIf=\"isHockey()\">{{ 'PERIODE' | translate }}</span>   \r\n          {{getPeriodNumber()}}/{{getTotalPeriods()}}\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n\r\n\r\n    <ion-row>\r\n    \r\n      <ion-col size=\"6\" class=\"{{getSportCellClass1()}}\" style=\"height: 3em;\">\r\n        <div class=\"ion-text-left subtext\">\r\n          {{ 'TEAM_NAME' | translate }}\r\n        </div>\r\n        \r\n        <div class=\"ion-text-center teamname\">\r\n          <a  [routerLink]=\"['/team-names']\" [queryParams]=\"{ id: '1'}\" >{{currentGameService.currentGame.homeTeamName}}</a>  \r\n        </div>\r\n      </ion-col>\r\n      <ion-col size=\"6\" class=\"{{getSportCellClass2()}}\">\r\n        <div class=\"ion-text-left subtext\">\r\n          {{ 'TEAM_NAME' | translate }}\r\n        </div>       \r\n        <div class=\"ion-text-center teamname\">\r\n          <a [routerLink]=\"['/team-names']\" [queryParams]=\"{ id: '2'}\" >{{currentGameService.currentGame.guestTeamName}}</a>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col size=\"2\" class=\"{{getSportCellClass1()}}\">\r\n        <div class=\"ion-text-center\">\r\n          <button (click)='goalTeam1Add()' class=\"{{getSportButtonClass1()}}\">\r\n            <ion-icon name=\"add-circle-outline\" size=\"large\"></ion-icon>\r\n          </button>\r\n        </div>\r\n        </ion-col>\r\n        <ion-col size=\"2\" class=\"{{getSportCellClass1()}}\">\r\n          <div class=\"ion-text-left subtext\">\r\n            {{ 'PUNKTE' | translate }}\r\n          </div>      \r\n          <div class=\"ion-text-center displayGoals\">\r\n          {{currentGame?.homeGoals}}\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"2\" class=\"{{getSportCellClass1()}}\">\r\n          <div class=\"ion-text-center\">\r\n          <button (click)='goalTeam1Substract()' class=\"{{getSportButtonClass1()}}\">\r\n            <ion-icon name=\"remove-circle-outline\" size=\"large\"></ion-icon>\r\n          </button>\r\n        </div>\r\n      </ion-col>\r\n\r\n      <ion-col size=\"2\" class=\"{{getSportCellClass2()}}\">\r\n        <div class=\"ion-text-center\">\r\n          <button (click)='goalTeam2Add()' class=\"{{getSportButtonClass2()}}\">\r\n            <ion-icon name=\"add-circle-outline\" size=\"large\"></ion-icon>\r\n          </button>\r\n        </div>\r\n      </ion-col>\r\n      <ion-col size=\"2\" class=\"{{getSportCellClass2()}}\">\r\n        \r\n        <div class=\"ion-text-left subtext\">\r\n          Punkte\r\n        </div>  \r\n        \r\n        <div class=\"ion-text-center displayGoals\">\r\n          {{currentGame?.guestGoals}}\r\n        </div>\r\n      </ion-col>\r\n      <ion-col size=\"2\" class=\"{{getSportCellClass2()}}\">\r\n        <div class=\"ion-text-center\">\r\n          <button (click)='goalTeam2Substract()' class=\"{{getSportButtonClass2()}}\">\r\n            <ion-icon name=\"remove-circle-outline\" size=\"large\"></ion-icon>\r\n           \r\n            \r\n          </button> \r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"\">\r\n        <div class=\"ion-text-center\">\r\n          \r\n          \r\n          \r\n          <button (click)='confirmFinishGame()' [disabled]=\"finishButtonDisabled()\" title=\"{{ 'ENDE_SPIEL' | translate }}\">\r\n            <img src=\"../assets/icon/NeuesSpiel.png\"  style=\"width: 40px;height: 40px;\" />\r\n            <ion-icon name=\"power\" size=\"large\"></ion-icon>\r\n          </button> \r\n          <button (click)='confirmNextPeriod()' [disabled]=\"nextPeriodDisabled()\" title=\"{{ 'NEUE_PERIODE' | translate }}\"><ion-icon name=\"arrow-forward\" size=\"large\"></ion-icon></button>\r\n          <!-- <button (click)='confirmStopTransmit()' [disabled]=\"sendSignalDisabled()\" title=\"Übertragung beenden\"><ion-icon name=\"power\" size=\"large\"></ion-icon></button> -->\r\n          <!-- <button (click)='deleteCurrentGame()'>Spiel löschen für test</button> -->\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    \r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"\">\r\n        <div class=\"ion-text-center\">\r\n          <button (click)='sendSignal()' [disabled]=\"sendSignalDisabled()\" *ngIf=\"isHockey()\" title=\"{{ 'SIGNAL_SENDEN' | translate }}\">\r\n            <ion-icon name=\"musical-notes\" size=\"large\" ></ion-icon>\r\n            <!-- <span class=\"iconify\" data-icon=\"ion-volume-high-outline\" data-inline=\"false\" ></span> -->\r\n          </button>\r\n          <button (click)='playGo()' [disabled]=\"playButtonDisabled()\" title=\"{{ 'START' | translate }}\"><ion-icon size=\"large\" name=\"play-circle-outline\"></ion-icon></button>\r\n          <button (click)='break()' [disabled]=\"breakButtonDisabled()\" title=\"{{ 'PAUSE' | translate }}\"><ion-icon size=\"large\" name=\"pause-circle-outline\"></ion-icon></button>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n<pre *ngIf=\"debugSettings.displayPreOnHtml\">\r\n  {{currentGameService.currentGame | json}}\r\n</pre>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/pages/play/play-routing.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/play/play-routing.module.ts ***!
    \***************************************************/

  /*! exports provided: PlayPageRoutingModule */

  /***/
  function srcAppPagesPlayPlayRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlayPageRoutingModule", function () {
      return PlayPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _play_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./play.page */
    "./src/app/pages/play/play.page.ts");

    var routes = [{
      path: '',
      component: _play_page__WEBPACK_IMPORTED_MODULE_3__["PlayPage"]
    }];

    var PlayPageRoutingModule = function PlayPageRoutingModule() {
      _classCallCheck(this, PlayPageRoutingModule);
    };

    PlayPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], PlayPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/play/play.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/pages/play/play.module.ts ***!
    \*******************************************/

  /*! exports provided: PlayPageModule */

  /***/
  function srcAppPagesPlayPlayModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlayPageModule", function () {
      return PlayPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _play_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./play-routing.module */
    "./src/app/pages/play/play-routing.module.ts");
    /* harmony import */


    var _play_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./play.page */
    "./src/app/pages/play/play.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

    var PlayPageModule = function PlayPageModule() {
      _classCallCheck(this, PlayPageModule);
    };

    PlayPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _play_routing_module__WEBPACK_IMPORTED_MODULE_5__["PlayPageRoutingModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]],
      declarations: [_play_page__WEBPACK_IMPORTED_MODULE_6__["PlayPage"]]
    })], PlayPageModule);
    /***/
  },

  /***/
  "./src/app/pages/play/play.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/pages/play/play.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesPlayPlayPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".running {\n  padding-left: 3 px;\n  padding-right: 3 px;\n  color: green;\n  font-size: large;\n}\n\n.displayGoals {\n  font-size: 160%;\n  margin-top: -0.2em;\n}\n\n.subtext {\n  font-size: 80%;\n  color: #000000 !important;\n  margin-top: -0.2em;\n}\n\nbutton.button-timer-style:disabled {\n  opacity: 0.9 !important;\n}\n\n.teamname {\n  bottom: 20px !important;\n  font-size: 110% !important;\n  margin-bottom: -0.5em;\n}\n\n.pause {\n  padding-left: 3 px;\n  padding-right: 3 px;\n  color: red;\n}\n\n.count-big {\n  font-size: 200%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGxheS9DOlxcZGV2XFxNb2JpbGVBcHBcXFMtZU1vdGlvbkFwcC9zcmNcXGFwcFxccGFnZXNcXHBsYXlcXHBsYXkucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9wbGF5L3BsYXkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQ0NKOztBRFFBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0FDTEo7O0FEWUE7RUFDSSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQ1RKOztBRGVBO0VBQ0ksdUJBQUE7QUNaSjs7QURnQkE7RUFLSSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EscUJBQUE7QUNqQko7O0FEcUJBO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUNsQko7O0FEcUJBO0VBQ0ksZUFBQTtBQ2xCSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BsYXkvcGxheS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucnVubmluZyB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDMgcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAzIHB4O1xyXG4gICAgY29sb3I6Z3JlZW47XHJcbiAgICBmb250LXNpemU6IGxhcmdlO1xyXG59XHJcbi5taW51dGVDb3VudGVye1xyXG4gICAgLy9jb2xvcjpibGFjaztcclxufVxyXG4uc2VjQ291bnRlcntcclxuICAgIC8vY29sb3I6YmxhY2s7XHJcbn1cclxuXHJcbi5kaXNwbGF5R29hbHMge1xyXG4gICAgZm9udC1zaXplOiAxNjAlO1xyXG4gICAgbWFyZ2luLXRvcDogLTAuMmVtO1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAvLyBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAvLyBtYXJnaW4tYm90dG9tOiAtM3B4O1xyXG5cclxuICAgIFxyXG59XHJcbi5zdWJ0ZXh0IHtcclxuICAgIGZvbnQtc2l6ZTogODAlO1xyXG4gICAgY29sb3I6ICMwMDAwMDAgIWltcG9ydGFudDtcclxuICAgIG1hcmdpbi10b3A6IC0wLjJlbTtcclxuICAgIFxyXG59XHJcblxyXG5cclxuXHJcbmJ1dHRvbi5idXR0b24tdGltZXItc3R5bGU6ZGlzYWJsZWQge1xyXG4gICAgb3BhY2l0eTogMC45ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG4udGVhbW5hbWUge1xyXG4gICAgLy8gZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgLy8gdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgIC8vbWFyZ2luLWJvdHRvbTogMS4yZW0gIWltcG9ydGFudDtcclxuICAgIC8vbWFyZ2luLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgYm90dG9tOiAyMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IDExMCUgIWltcG9ydGFudDtcclxuICAgIG1hcmdpbi1ib3R0b206IC0wLjVlbTtcclxuXHJcbn1cclxuXHJcbi5wYXVzZSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDMgcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAzIHB4O1xyXG4gICAgY29sb3I6cmVkO1xyXG5cclxufVxyXG4uY291bnQtYmlnIHtcclxuICAgIGZvbnQtc2l6ZTogMjAwJTtcclxufSIsIi5ydW5uaW5nIHtcbiAgcGFkZGluZy1sZWZ0OiAzIHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAzIHB4O1xuICBjb2xvcjogZ3JlZW47XG4gIGZvbnQtc2l6ZTogbGFyZ2U7XG59XG5cbi5kaXNwbGF5R29hbHMge1xuICBmb250LXNpemU6IDE2MCU7XG4gIG1hcmdpbi10b3A6IC0wLjJlbTtcbn1cblxuLnN1YnRleHQge1xuICBmb250LXNpemU6IDgwJTtcbiAgY29sb3I6ICMwMDAwMDAgIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogLTAuMmVtO1xufVxuXG5idXR0b24uYnV0dG9uLXRpbWVyLXN0eWxlOmRpc2FibGVkIHtcbiAgb3BhY2l0eTogMC45ICFpbXBvcnRhbnQ7XG59XG5cbi50ZWFtbmFtZSB7XG4gIGJvdHRvbTogMjBweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDExMCUgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogLTAuNWVtO1xufVxuXG4ucGF1c2Uge1xuICBwYWRkaW5nLWxlZnQ6IDMgcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDMgcHg7XG4gIGNvbG9yOiByZWQ7XG59XG5cbi5jb3VudC1iaWcge1xuICBmb250LXNpemU6IDIwMCU7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/play/play.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/pages/play/play.page.ts ***!
    \*****************************************/

  /*! exports provided: PlayPage */

  /***/
  function srcAppPagesPlayPlayPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlayPage", function () {
      return PlayPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/shared/model/general-settings */
    "./src/app/shared/model/general-settings.ts");
    /* harmony import */


    var src_app_shared_service_current_game_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/shared/service/current-game.service */
    "./src/app/shared/service/current-game.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/shared/global-function */
    "./src/app/shared/global-function.ts");
    /* harmony import */


    var src_app_shared_service_football_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/shared/service/football.service */
    "./src/app/shared/service/football.service.ts");
    /* harmony import */


    var src_app_shared_service_hockey_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/shared/service/hockey.service */
    "./src/app/shared/service/hockey.service.ts");
    /* harmony import */


    var src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/shared/service/protocol.service */
    "./src/app/shared/service/protocol.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var src_app_shared_service_debug_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! src/app/shared/service/debug.service */
    "./src/app/shared/service/debug.service.ts");
    /* harmony import */


    var src_app_shared_service_message_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! src/app/shared/service/message.service */
    "./src/app/shared/service/message.service.ts");
    /* harmony import */


    var src_app_shared_service_bluetooth_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! src/app/shared/service/bluetooth.service */
    "./src/app/shared/service/bluetooth.service.ts");
    /* harmony import */


    var src_app_shared_service_configuration_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! src/app/shared/service/configuration.service */
    "./src/app/shared/service/configuration.service.ts");
    /* harmony import */


    var src_app_shared_service_game_time_counter_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! src/app/shared/service/game-time-counter.service */
    "./src/app/shared/service/game-time-counter.service.ts");

    var PlayPage = /*#__PURE__*/function () {
      function PlayPage(configurationService, currentGameService, footballService, hockeyService, router, protocolService, alertController, translate, debugService, messageService, bluetoothService, gameTimeCounterService) {
        var _this = this;

        _classCallCheck(this, PlayPage);

        this.configurationService = configurationService;
        this.currentGameService = currentGameService;
        this.footballService = footballService;
        this.hockeyService = hockeyService;
        this.router = router;
        this.protocolService = protocolService;
        this.alertController = alertController;
        this.translate = translate;
        this.debugService = debugService;
        this.messageService = messageService;
        this.bluetoothService = bluetoothService;
        this.gameTimeCounterService = gameTimeCounterService;
        this.classame = this.constructor.name; // Immer wenn sich der Wert ändert wird das Protokoll prot22h ausgelöst. 

        this.oldPeriod = 0;
        this.translate.setDefaultLang('de');
        this.debugService.getdata().subscribe(function (data) {
          _this.debugSettings = data;
        });
      }

      _createClass(PlayPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this2 = this;

          console.log("".concat(this.classame, "  ngOnInit"));
          this.subscriptionRes = this.translate.get(['NAECHSTE_PERIODE', 'UEBERTRAGUNG_BEENDEN', 'SPIEL_BEENDEN', 'JA', 'NEIN', 'SPIEL_BEENDET', "BLUETOOTH_GERAET_NICHT_VERFUEGBAR"]).subscribe(function (translation) {
            _this2.naechste_periode_res = translation['NAECHSTE_PERIODE'];
            _this2.uebertragung_beenden_res = translation['UEBERTRAGUNG_BEENDEN'];
            _this2.spiel_beenden_res = translation['SPIEL_BEENDEN'];
            _this2.ja_res = translation['JA'];
            _this2.nein_res = translation['NEIN'];
            _this2.spiel_beendet_res = translation['SPIEL_BEENDET'];
            _this2.bluetooth_error_res = translation['BLUETOOTH_GERAET_NICHT_VERFUEGBAR'];
          });
          this.subscription = this.currentGameService.getdata().subscribe(function (data) {
            _this2.reactOnCurrentSettingChanges(data);
          }); // in einigen Fällen besser auf diesen Wed die Daten aktualisieren.

          this.subscription = this.currentGameService.GameChanges.subscribe(function (data) {
            _this2.reactOnCurrentSettingChanges(data);
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subscription.unsubscribe();
          this.subscriptionRes.unsubscribe();
        }
      }, {
        key: "confirmFinishGame",
        value: function confirmFinishGame() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this3 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.alertController.create({
                      header: this.spiel_beenden_res,
                      buttons: [{
                        text: this.ja_res,
                        handler: function handler() {
                          return _this3.finishGame();
                        }
                      }, {
                        text: this.nein_res,
                        handler: function handler() {
                          return _this3.cancel();
                        }
                      }]
                    });

                  case 2:
                    alert = _context.sent;
                    _context.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "confirmNextPeriod",
        value: function confirmNextPeriod() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this4 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.alertController.create({
                      header: this.naechste_periode_res,
                      buttons: [{
                        text: this.ja_res,
                        handler: function handler() {
                          return _this4.nextPeriod();
                        }
                      }, {
                        text: this.nein_res,
                        handler: function handler() {
                          return _this4.cancel();
                        }
                      }]
                    });

                  case 2:
                    alert = _context2.sent;
                    _context2.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "confirmStopTransmit",
        value: function confirmStopTransmit() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this5 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.alertController.create({
                      header: this.uebertragung_beenden_res,
                      buttons: [{
                        text: this.ja_res,
                        handler: function handler() {
                          return _this5.stopTransmit();
                        }
                      }, {
                        text: this.nein_res,
                        handler: function handler() {
                          return _this5.cancel();
                        }
                      }]
                    });

                  case 2:
                    alert = _context3.sent;
                    _context3.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "cancel",
        value: function cancel() {
          console.log("Cancel");
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          // todo: not clear if this works as expected
          if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.currentGameService.currentGame)) {
            this.router.navigate(['settings']);
          } else if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.currentGameService.currentGame.gameStatus)) {
            this.router.navigate(['settings-advanced']);
          } //this.messageService.presentToasterMiddleMessage(this.bluetooth_error_res);


          console.log("".concat(this.classame, "  ionViewDidEnter"));
        }
      }, {
        key: "reactOnCurrentSettingChanges",
        value: function reactOnCurrentSettingChanges(data) {
          var _a, _b, _c;

          this.pageGameStatus = data.gameStatus;
          this.currentGame = data;

          if (this.oldPeriod != this.currentGame.period) {
            // senden der Periodeninformation an die Anzeige
            // nur wenn das Spiel noch nicht angefangen hat. 
            // Im anderen Fall wird von den Setting aus das erste Mal das Protokoll aus aufgerufen
            if (this.oldPeriod != 0) {
              // senden der Periode
              // beim Fussball keine Bedetung, wird aber trotzdem mit übertragen  
              var setting22H = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["Setting22H"]();
              setting22H.period = this.currentGame.period.toString();

              if ((_a = this.debugSettings) === null || _a === void 0 ? void 0 : _a.debugProtokolWorkflow) {
                debugger;
              }

              this.protocolService.prot22h(setting22H);
            } // wenn der Timer nich gestartet ist, dann Timer starten.


            if (!this.protocolService.transmitTimeIsActive) {
              if ((_b = this.debugSettings) === null || _b === void 0 ? void 0 : _b.debugProtokolWorkflow) {
                debugger;
              } // Beginn der sekündlichen Übertragung der stehenden und laufenden Zeit.


              this.protocolService.startTransmitTimer();
            } // Die Uhrzeit aus den Settings wieder aktualisieren.


            var setting21H = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["Setting21H"]();
            setting21H.flagMid = "2"; // Pause 

            setting21H.signalEnd = "0"; // Kein Signal

            setting21H = src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].setSettings21HFromCurrentGame(this.currentGameService.currentGame, setting21H); // Damit müssten die Werte bei der nächsten Protokollübertragung aktualisiert werden

            this.protocolService.setting21H = setting21H;
          }

          this.oldPeriod = this.currentGame.period;

          if (data.gameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].pause || data.gameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].periodFinished || data.gameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished || data.gameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].init) {
            // Den Timer der Seite anhalten.
            this.pauseTimer();
          } //Bei Hockey nach der Beendigung einer Periode die Hupe auslösen


          if (this.currentGameService.hockeyPeriodFinished()) {
            if ((_c = this.debugSettings) === null || _c === void 0 ? void 0 : _c.debugProtokolWorkflow) {
              debugger;
            } // Signal wird nur ausgelöst, wenn Zeit automatisch runter läuft und nicht manuell eine neue Periode gestartet wird. 


            this.protocolService.sentTimeWithSignal();
          } // Eine Nachricht anzeigen, falls eine Ressource dafür vorhanden ist.


          if (data.gameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished && !src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.spiel_beendet_res)) {
            if (data.period != 0) {
              this.messageService.presentToasterMessage(this.spiel_beendet_res);
            }
          }
        }
      }, {
        key: "getSportReleateMinutes",
        value: function getSportReleateMinutes() {
          return this.currentGameService.getSportReleateMinutesService();
        }
      }, {
        key: "isFootball",
        value: function isFootball() {
          var _a, _b;

          return ((_b = (_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame) === null || _b === void 0 ? void 0 : _b.gameType) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football;
        }
      }, {
        key: "isHockey",
        value: function isHockey() {
          var _a, _b;

          return ((_b = (_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame) === null || _b === void 0 ? void 0 : _b.gameType) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey;
        }
      }, {
        key: "gameIsRunning",
        value: function gameIsRunning() {
          if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.pageGameStatus)) {
            return false;
          }

          return this.pageGameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].play;
        }
      }, {
        key: "isFootballNormal",
        value: function isFootballNormal() {
          var _a, _b, _c, _d;

          return ((_b = (_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame) === null || _b === void 0 ? void 0 : _b.gameType) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football && ((_d = (_c = this.currentGameService) === null || _c === void 0 ? void 0 : _c.currentGame) === null || _d === void 0 ? void 0 : _d.timePeriod) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal;
        }
      }, {
        key: "isFootballReneval",
        value: function isFootballReneval() {
          var _a, _b, _c, _d;

          return ((_b = (_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame) === null || _b === void 0 ? void 0 : _b.gameType) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football && ((_d = (_c = this.currentGameService) === null || _c === void 0 ? void 0 : _c.currentGame) === null || _d === void 0 ? void 0 : _d.timePeriod) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].renewal;
        }
      }, {
        key: "getTotalPeriods",
        value: function getTotalPeriods() {
          var _a, _b, _c, _d;

          if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty((_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame)) {
            return 0;
          } else {
            if (((_b = this.currentGameService) === null || _b === void 0 ? void 0 : _b.currentGame.timePeriod) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) {
              return (_c = this.currentGameService) === null || _c === void 0 ? void 0 : _c.currentGame.regularTimeNumPeriods;
            } else {
              return (_d = this.currentGameService) === null || _d === void 0 ? void 0 : _d.currentGame.extraTimeNumPeriods;
            }
          }
        }
      }, {
        key: "getPeriodNumber",
        value: function getPeriodNumber() {
          var _a, _b, _c, _d, _e;

          if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty((_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame)) {
            return 0;
          }

          if (((_b = this.currentGameService) === null || _b === void 0 ? void 0 : _b.currentGame.timePeriod) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) {
            return (_c = this.currentGameService) === null || _c === void 0 ? void 0 : _c.currentGame.period;
          } else {
            return ((_d = this.currentGameService) === null || _d === void 0 ? void 0 : _d.currentGame.period) - ((_e = this.currentGameService) === null || _e === void 0 ? void 0 : _e.currentGame.regularTimeNumPeriods);
          }
        }
      }, {
        key: "goalTeam1Add",
        value: function goalTeam1Add() {
          this.currentGameService.goalTeam1Add();
          this.changeGoalHandling();
        }
      }, {
        key: "goalTeam1Substract",
        value: function goalTeam1Substract() {
          this.currentGameService.goalTeam1Substract();
          this.changeGoalHandling();
        }
      }, {
        key: "goalTeam2Add",
        value: function goalTeam2Add() {
          this.currentGameService.goalTeam2Add();
          this.changeGoalHandling();
        }
      }, {
        key: "goalTeam2Substract",
        value: function goalTeam2Substract() {
          this.currentGameService.goalTeam2Substract();
          this.changeGoalHandling();
        }
      }, {
        key: "changeGoalHandling",
        value: function changeGoalHandling() {
          var _a;

          if ((_a = this.debugSettings) === null || _a === void 0 ? void 0 : _a.debugProtokolWorkflow) {
            debugger;
          }

          this.protocolService.prot23h(this.currentGameService.currentGame.homeGoals, this.currentGameService.currentGame.guestGoals);
        } // deaktivert wenn das spiel läuft oder das ende der möglichen Verlängerung erreicht ist.

      }, {
        key: "nextPeriodDisabled",
        value: function nextPeriodDisabled() {
          var _a, _b, _c;

          if (this.pageGameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].play) {
            return true;
          }

          var maxPeriod = 0;

          if (this.currentGame.extraTimeAvailable == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["ExtraTimeAvailable"].No) {
            maxPeriod = (_a = this.currentGame) === null || _a === void 0 ? void 0 : _a.regularTimeNumPeriods;
          } else {
            maxPeriod = ((_b = this.currentGame) === null || _b === void 0 ? void 0 : _b.regularTimeNumPeriods) + ((_c = this.currentGame) === null || _c === void 0 ? void 0 : _c.extraTimeNumPeriods);
          }

          if (!isNaN(maxPeriod)) {
            if (this.currentGame.period >= maxPeriod) {
              return true;
            }
          }

          return false;
        }
      }, {
        key: "manualChangeDisabled",
        value: function manualChangeDisabled() {
          if (this.pageGameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].play) {
            return true;
          }

          return false;
        }
      }, {
        key: "finishButtonDisabled",
        value: function finishButtonDisabled() {
          if (this.pageGameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].play) {
            return true;
          }

          return false;
        }
      }, {
        key: "playDisabled",
        value: function playDisabled() {
          if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.pageGameStatus)) {
            return true;
          }

          if (this.pageGameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].periodFinished) {
            return true;
          }

          if (this.pageGameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished) {
            return true;
          }

          if (this.pageGameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].play) {
            return true;
          }

          return false;
        }
      }, {
        key: "playButtonDisabled",
        value: function playButtonDisabled() {
          return this.playDisabled();
        }
      }, {
        key: "breakButtonDisabled",
        value: function breakButtonDisabled() {
          if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.pageGameStatus)) {
            return true;
          }

          if (this.pageGameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].pause) {
            return true;
          }

          if (this.pageGameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished) {
            return true;
          }

          if (this.pageGameStatus == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].periodFinished) {
            return true;
          }

          return false;
        }
      }, {
        key: "nextPeriod",
        value: function nextPeriod() {
          debugger;
          this.pauseTimer();
          this.currentGameService.startNextPeriod();
        }
      }, {
        key: "manualChange",
        value: function manualChange() {
          this.router.navigate(['period']);
        }
      }, {
        key: "sendSignalDisabled",
        value: function sendSignalDisabled() {// Beim Fußball gibt es die Signanübertragund nicht.
          // besser ausblenden des Buttons.
        } // für 5 Sekunden muss das Flag SigEnde auf "1" stehen danach wieder auf "0" setzen
        // todo: fix review : obwohl die Methode async ist 
        // läuft der Service zum verschicken (prot22h) nicht weiter

      }, {
        key: "sendSignal",
        value: function sendSignal() {
          var _a;

          if ((_a = this.debugSettings) === null || _a === void 0 ? void 0 : _a.debugProtokolWorkflow) {
            debugger;
          }

          this.protocolService.sentTimeWithSignal();
        } // am besten noch mal in einem extra step. 

      }, {
        key: "stopTransmit",
        value: function stopTransmit() {
          var _a;

          this.gameTimeCounterService.clearTimer(); // damit wird auch die Übertragung beendet!

          this.currentGameService.finishGame();
          var setting21H = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["Setting21H"]();
          setting21H.flagMid = src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_8__["ProtocolService"].setting21GameIsNotRunningFlagASCII; // damit müsste das Protokoll aktualisiert werden

          this.protocolService.setting21H = setting21H; // Die Übertragung wird nach Spielende ebenfalls beendet. 

          if ((_a = this.debugSettings) === null || _a === void 0 ? void 0 : _a.debugProtokolWorkflow) {
            debugger;
          }

          this.protocolService.endTransmitTimer(); // // zurück zur Hauptseite

          if (this.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey) {
            if (this.hockeyService.hockeySettings.length > 1) {
              this.router.navigate(['settings-advanced']);
            } else {
              this.router.navigate(['settings']);
            }
          } else if (this.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football) {
            if (this.footballService.footballSettings.length > 1) {
              this.router.navigate(['settings-advanced']);
            } else {
              this.router.navigate(['settings']);
            }
          } else {
            this.router.navigate(['settings']);
          } // Timer anhalten
          // if ( GlobalFunction.isEmpty(this.currentGameService.currentGame)){
          //   this.router.navigate(['settings']);
          // } else if (this.currentGameService.currentGame.period == 0) {
          //   this.router.navigate(['settings-advanced']);
          // }

        }
      }, {
        key: "getSportCellClass1",
        value: function getSportCellClass1() {
          var _a;

          if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty((_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame)) {
            return "cell-class";
          } else {
            if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football) {
              return "football1 cell-class";
            } else {
              return "hockey1 cell-class";
            }
          }
        }
      }, {
        key: "getSportCellClass2",
        value: function getSportCellClass2() {
          var _a;

          if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty((_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame)) {
            return "cell-class";
          } else {
            if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football) {
              return "football2 cell-class";
            } else {
              return "hockey2 cell-class";
            }
          }
        }
      }, {
        key: "playGo",
        value: function playGo() {
          // Zusätzliche Bedingung für Weiterführung des Spiels wird hier noch einmal überrprüft
          if (this.playDisabled()) {
            return;
          } // timer transmit time stop for a short moment


          this.protocolService.endTransmitTimer();
          this.gameTimeCounterService.SetIntervalTimer();
          this.currentGameService.currentGame.gameStatus = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].play;
          this.pageGameStatus = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].play; // timer transmit start againg after second count.

          this.protocolService.startTransmitTimer();
        }
      }, {
        key: "break",
        value: function _break() {
          this.pauseTimer();
          this.currentGameService.currentGame.gameStatus = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].pause;
          this.pageGameStatus = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].pause;
        }
      }, {
        key: "pauseTimer",
        value: function pauseTimer() {
          this.gameTimeCounterService.clearTimer();
          var setting21H = this.protocolService.setting21H;
          setting21H.flagMid = src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_8__["ProtocolService"].setting21GameIsNotRunningFlagASCII; // damit müsste das Protokoll aktualisiert werden

          this.protocolService.setting21H = setting21H;
        } // spiel beenden

      }, {
        key: "finishGame",
        value: function finishGame() {
          this.gameTimeCounterService.clearTimer(); // damit wird auch die Übertragung beendet!

          this.currentGameService.finishGame();
          var setting21H = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["Setting21H"]();
          setting21H.flagMid = src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_8__["ProtocolService"].setting21GameIsNotRunningFlagASCII; // damit müsste das Protokoll aktualisiert werden

          this.protocolService.setting21H = setting21H; //this.currentGameService.setEndGameSettings();
          // Die Übertragung wird nach Spielende nictht beendet. 
          // Problem hierbei ist, dass Hockey am Ende einer Periode 5 Sekundein ein Signal schickt.

          this.protocolService.endTransmitTimer(); // zurück zur Hauptseite

          if (this.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey) {
            if (this.hockeyService.hockeySettings.length > 1) {
              this.router.navigate(['settings-advanced']);
            } else {
              this.router.navigate(['settings']);
            }
          } else if (this.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football) {
            if (this.footballService.footballSettings.length > 1) {
              this.router.navigate(['settings-advanced']);
            } else {
              this.router.navigate(['settings']);
            }
          } else {
            this.router.navigate(['settings']);
          }
        } // wird zur Zeit nicht verwendet.

      }, {
        key: "deleteCurrentGame",
        value: function deleteCurrentGame() {
          this.currentGameService.deleteCurrentGame();
          this.router.navigate(['settings']);
        }
      }, {
        key: "getSportButtonClass1",
        value: function getSportButtonClass1() {
          var _a;

          if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty((_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame)) {
            return "";
          } else {
            if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football) {
              return "football1";
            } else {
              return "hockey1";
            }
          }
        }
      }, {
        key: "getSportButtonClass2",
        value: function getSportButtonClass2() {
          var _a;

          if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty((_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame)) {
            return "";
          } else {
            if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football) {
              return "football2";
            } else {
              return "hockey2";
            }
          }
        }
      }]);

      return PlayPage;
    }();

    PlayPage.ctorParameters = function () {
      return [{
        type: src_app_shared_service_configuration_service__WEBPACK_IMPORTED_MODULE_14__["ConfigurationService"]
      }, {
        type: src_app_shared_service_current_game_service__WEBPACK_IMPORTED_MODULE_3__["CurrentGameService"]
      }, {
        type: src_app_shared_service_football_service__WEBPACK_IMPORTED_MODULE_6__["FootballService"]
      }, {
        type: src_app_shared_service_hockey_service__WEBPACK_IMPORTED_MODULE_7__["HockeyService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_8__["ProtocolService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["AlertController"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__["TranslateService"]
      }, {
        type: src_app_shared_service_debug_service__WEBPACK_IMPORTED_MODULE_11__["DebugService"]
      }, {
        type: src_app_shared_service_message_service__WEBPACK_IMPORTED_MODULE_12__["MessageService"]
      }, {
        type: src_app_shared_service_bluetooth_service__WEBPACK_IMPORTED_MODULE_13__["BluetoothService"]
      }, {
        type: src_app_shared_service_game_time_counter_service__WEBPACK_IMPORTED_MODULE_15__["GameTimeCounterService"]
      }];
    };

    PlayPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-play',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./play.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/play/play.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./play.page.scss */
      "./src/app/pages/play/play.page.scss"))["default"]]
    })], PlayPage);
    /***/
  },

  /***/
  "./src/app/shared/service/game-time-counter.service.ts":
  /*!*************************************************************!*\
    !*** ./src/app/shared/service/game-time-counter.service.ts ***!
    \*************************************************************/

  /*! exports provided: GameTimeCounterService */

  /***/
  function srcAppSharedServiceGameTimeCounterServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GameTimeCounterService", function () {
      return GameTimeCounterService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _current_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./current-game.service */
    "./src/app/shared/service/current-game.service.ts");
    /* harmony import */


    var _protocol_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./protocol.service */
    "./src/app/shared/service/protocol.service.ts");
    /* harmony import */


    var _model_general_settings__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../model/general-settings */
    "./src/app/shared/model/general-settings.ts");
    /* harmony import */


    var _global_function__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../global-function */
    "./src/app/shared/global-function.ts");

    var GameTimeCounterService = /*#__PURE__*/function () {
      function GameTimeCounterService(currentGameService, protocolService) {
        _classCallCheck(this, GameTimeCounterService);

        this.currentGameService = currentGameService;
        this.protocolService = protocolService;
      }

      _createClass(GameTimeCounterService, [{
        key: "clearTimer",
        value: function clearTimer() {
          clearInterval(this.IntervalTimer);
        }
      }, {
        key: "SetIntervalTimer",
        value: function SetIntervalTimer() {
          var _this6 = this;

          this.IntervalTimer = setInterval(function () {
            _this6.currentGameService.tick();

            var setting21H = new _model_general_settings__WEBPACK_IMPORTED_MODULE_4__["Setting21H"]();
            setting21H.flagMid = _protocol_service__WEBPACK_IMPORTED_MODULE_3__["ProtocolService"].setting21GameIsRunningFlagASCII;
            setting21H = _global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].setSettings21HFromCurrentGame(_this6.currentGameService.currentGame, setting21H); // damit müsste das Protokoll aktualisiert werden

            _this6.protocolService.setting21H = setting21H;
          }, 1000);
        }
      }]);

      return GameTimeCounterService;
    }();

    GameTimeCounterService.ctorParameters = function () {
      return [{
        type: _current_game_service__WEBPACK_IMPORTED_MODULE_2__["CurrentGameService"]
      }, {
        type: _protocol_service__WEBPACK_IMPORTED_MODULE_3__["ProtocolService"]
      }];
    };

    GameTimeCounterService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], GameTimeCounterService);
    /***/
  }
}]);
//# sourceMappingURL=pages-play-play-module-es5.js.map