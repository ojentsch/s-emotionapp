(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-settings-advanced-settings-advanced-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/settings-advanced/settings-advanced.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/settings-advanced/settings-advanced.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      \r\n      <span *ngIf=\"isFootball()\">{{ 'FUSSBALL' | translate }}</span>\r\n      <span *ngIf=\"isHockey()\">{{ 'HOCKEY' | translate }}</span>\r\n      <!-- <span *ngIf=\"isUndefined()\">Erweiterte Einstellungen</span> -->\r\n      \r\n      </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-grid>\r\n    <ion-row *ngFor=\"let setting of advancedSportSettings\">\r\n      <ion-col size=\"1\" style='width:5px'></ion-col>\r\n      <ion-col size=\"5\" block class=\"{{getSportCellClass1()}}\">\r\n        <div class=\"ion-text-left button-style\"  (click)=\"setSettings(setting.periodSettingId)\" >      \r\n          {{setting.settingName}}\r\n        </div> \r\n      </ion-col>\r\n     \r\n    <ion-col size=\"4\" block class=\"{{getSportCellClass1()}}\">\r\n        <div class=\"ion-text-center\" *ngIf=\"isFootball()\" >\r\n          {{setting.periodAmounts}} x {{setting.durationMin}} {{ 'MINUTEN' | translate }}\r\n        </div>\r\n        <div class=\"ion-text-center\" *ngIf=\"isHockey()\" >\r\n          {{setting.periodAmounts}} x {{setting.durationMin}} {{ 'MINUTEN' | translate }}\r\n        </div>\r\n      </ion-col>\r\n      <ion-col size=\"2\" class=\"{{getSportCellClass1()}}\">\r\n      <button  (click)=\"deleteSetting(setting.periodSettingId)\" class=\"{{getSportButtonClass1()}}\">\r\n        <div class=\"ion-text-right\">\r\n          <ion-icon name=\"trash\" size=\"large\"></ion-icon>\r\n        </div>\r\n      </button>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\" style='height: 3em;'></ion-col>\r\n  </ion-row>\r\n  <form [formGroup]=\"form\">\r\n    <ion-row>\r\n      <ion-col size=\"1\" style='width:5px'></ion-col>\r\n      <ion-col size=\"4\" style=\"padding-top: 1em;\">\r\n        {{ 'NEUE_SPIELEINSTELLUNG' | translate }}\r\n      </ion-col>\r\n      <ion-col size=\"7\">\r\n        <ion-input type=\"text\" formControlName=\"periodName\" maxlength=\"30\"></ion-input>\r\n      </ion-col>\r\n  </ion-row>\r\n\r\n\r\n    <ion-row>\r\n      <ion-col size=\"1\" style='width:5px'></ion-col>\r\n      <ion-col size=\"4\">\r\n        <div class=\"ion-text-left\" style=\"padding-top: 1em;\">\r\n          <span *ngIf=\"isFootball()\">{{ 'HALBZEIT_LAENGE' | translate }} {{ 'MIN' | translate }}</span>\r\n          <span *ngIf=\"isHockey()\">{{ 'PERIODE_LAENGE' | translate }} {{ 'MIN' | translate }}</span>\r\n      </div>\r\n      </ion-col>\r\n      <ion-col size=\"2\" >\r\n        <div class=\"ion-text-right\">\r\n        <button (click)='minNormalTimeSubstract()'>\r\n          <ion-icon name=\"remove-circle-outline\" size=\"large\"></ion-icon>\r\n          </button>\r\n        </div>\r\n      </ion-col>\r\n\r\n      <ion-col size=\"2\">\r\n        <div class=\"ion-text-center\" style=\"padding-top: 0.5em\">\r\n          {{regularTimeTargetMinutes}}\r\n        </div>\r\n      </ion-col>\r\n  \r\n      <ion-col size=\"2\">\r\n        <div class=\"ion-text-left\">\r\n        <button (click)='minNormalTimeAdd()'>\r\n          <ion-icon name=\"add-circle-outline\" size=\"large\"></ion-icon>\r\n        </button>\r\n      </div>\r\n      </ion-col>\r\n      <ion-col size=\"1\" ></ion-col>\r\n    </ion-row>\r\n\r\n\r\n    <ion-row *ngIf=\"isFootball()\">\r\n      <ion-col size=\"1\" style='width:5px'></ion-col>\r\n      <ion-col size=\"4\" >\r\n        <div class=\"ion-text-left\" style=\"padding-top: 0.5em\">\r\n          {{ 'VERLAENGERUNG' | translate }} {{ 'MIN' | translate }}\r\n      </div>\r\n      </ion-col>\r\n      <ion-col size=\"2\" >\r\n        <div class=\"ion-text-right\">\r\n        <button (click)='minExtraTimeSubstract()'>\r\n          <ion-icon name=\"remove-circle-outline\" size=\"large\"></ion-icon>\r\n          </button>\r\n        </div>\r\n      </ion-col>\r\n\r\n      <ion-col size=\"2\">\r\n        <div class=\"ion-text-center\" style=\"padding-top: 0.5em\"> \r\n          {{extraTimeTargetMinutes}}\r\n        </div>\r\n      </ion-col>\r\n  \r\n      <ion-col size=\"2\">\r\n        <div class=\"ion-text-left\">\r\n        <button (click)='minExtraTimeAdd()'>\r\n          <ion-icon name=\"add-circle-outline\" size=\"large\"></ion-icon>\r\n        </button>\r\n      </div>\r\n      </ion-col>\r\n      <ion-col size=\"1\" ></ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row *ngIf=\"isHockey()\">\r\n      <ion-col size=\"1\" style='width:5px'></ion-col>\r\n      <ion-col size=\"4\">\r\n        <div class=\"ion-text-left\" style=\"padding-top: 0.5em;\">\r\n          {{ 'PERIODE_ANZAHL' | translate }}\r\n      </div>\r\n      </ion-col>\r\n      <ion-col size=\"2\" >\r\n        <div class=\"ion-text-right\">\r\n        <button (click)='periodSubstract()' [disabled]=\"regularTimeNumPeriods < 1\">\r\n          <ion-icon name=\"remove-circle-outline\" size=\"large\"></ion-icon>\r\n          </button>\r\n        </div>\r\n      </ion-col>\r\n\r\n      <ion-col size=\"2\">\r\n        <div class=\"ion-text-center\" style=\"padding-top: 0.5em\">\r\n          {{regularTimeNumPeriods}}\r\n        </div>\r\n      </ion-col>\r\n  \r\n      <ion-col size=\"2\">\r\n        <div class=\"ion-text-left\">\r\n        <button (click)='periodAdd()'>\r\n          <ion-icon name=\"add-circle-outline\" size=\"large\"></ion-icon>\r\n        </button>\r\n      </div>\r\n      </ion-col>\r\n      <ion-col size=\"1\" ></ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row *ngIf=\"isHockey()\">\r\n      <ion-col size=\"1\" style='width:5px'></ion-col>\r\n      <ion-col size=\"4\">\r\n        <div class=\"ion-text-left\" style=\"padding-top: 0.5em;\">\r\n          {{ 'VORWAERTSZAEHLER' | translate }}\r\n      </div>\r\n      </ion-col>\r\n      <ion-col size=\"2\" >\r\n        <div class=\"ion-text-right\" style=\"margin-right: 13px;\">\r\n          <ion-checkbox class=\"checkbox\" color=\"light\" formControlName=\"isForwardCounter\" checked=\"false\"></ion-checkbox>\r\n        </div>\r\n      </ion-col>\r\n      <ion-col size=\"5\" ></ion-col>\r\n  </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <div class=\"ion-text-end\">\r\n          <button (click)='saveNewGame()' [disabled]=\"((!form.valid) || (regularTimeTargetMinutes < 1) || (regularTimeNumPeriods < 1))\" title=\"{{ 'SPEICHERN' | translate }}\">\r\n            <ion-icon name=\"checkmark\" size=\"large\"></ion-icon>\r\n          </button>   \r\n          <!-- <button (click)='cancel()' title=\"{{ 'ABBRUCH' | translate }}\">\r\n            <ion-icon name=\"close\" size=\"large\"></ion-icon>\r\n          </button> -->\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </form>\r\n\r\n    \r\n    <ion-row>\r\n      <ion-col size=\"12\" >\r\n      <button (click)='goToSettings()' title=\"{{ 'ZURUECK' | translate }}\"><ion-icon name=\"arrow-back\" size=\"large\"></ion-icon></button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n\r\n  <!-- <pre>\r\n    {{footballService.footballSettingsFiltered | json}}\r\n  </pre> -->\r\n\r\n  <pre *ngIf=\"debugSettings.displayPreOnHtml\">\r\n    {{advancedSportSettings | json}}\r\n  </pre>\r\n\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/settings-advanced/settings-advanced-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/settings-advanced/settings-advanced-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: SettingsAdvancedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsAdvancedPageRoutingModule", function() { return SettingsAdvancedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _settings_advanced_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./settings-advanced.page */ "./src/app/pages/settings-advanced/settings-advanced.page.ts");




const routes = [
    {
        path: '',
        component: _settings_advanced_page__WEBPACK_IMPORTED_MODULE_3__["SettingsAdvancedPage"]
    }
];
let SettingsAdvancedPageRoutingModule = class SettingsAdvancedPageRoutingModule {
};
SettingsAdvancedPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SettingsAdvancedPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/settings-advanced/settings-advanced.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/settings-advanced/settings-advanced.module.ts ***!
  \*********************************************************************/
/*! exports provided: SettingsAdvancedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsAdvancedPageModule", function() { return SettingsAdvancedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _settings_advanced_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./settings-advanced-routing.module */ "./src/app/pages/settings-advanced/settings-advanced-routing.module.ts");
/* harmony import */ var _settings_advanced_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./settings-advanced.page */ "./src/app/pages/settings-advanced/settings-advanced.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let SettingsAdvancedPageModule = class SettingsAdvancedPageModule {
};
SettingsAdvancedPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _settings_advanced_routing_module__WEBPACK_IMPORTED_MODULE_5__["SettingsAdvancedPageRoutingModule"]
        ],
        declarations: [_settings_advanced_page__WEBPACK_IMPORTED_MODULE_6__["SettingsAdvancedPage"]]
    })
], SettingsAdvancedPageModule);



/***/ }),

/***/ "./src/app/pages/settings-advanced/settings-advanced.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/settings-advanced/settings-advanced.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".hockey1 {\n  background-color: #4ABED5;\n}\n\n.hockey2 {\n  background-color: #3D9DAD;\n}\n\n.football1 {\n  background-color: #3A9463;\n}\n\n.football2 {\n  background-color: #337F53;\n}\n\n.black1 {\n  background-color: #354556;\n}\n\n.black2 {\n  background-color: #354556;\n}\n\nbody {\n  color: #FFFFFF;\n  background-color: red;\n}\n\n.hockey1 {\n  background-color: #4ABED5;\n}\n\n.hockey2 {\n  background-color: #3D9DAD;\n}\n\n.football1 {\n  background-color: #3A9463;\n}\n\n.football2 {\n  background-color: #337F53;\n}\n\n.black1 {\n  background-color: #354556;\n}\n\n.black2 {\n  background-color: #354556;\n}\n\nbody {\n  color: #FFFFFF;\n  background-color: powderblue;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2V0dGluZ3MtYWR2YW5jZWQvQzpcXGRldlxcTW9iaWxlQXBwXFxTLWVNb3Rpb25BcHAvc3JjXFxhcHBcXHBhZ2VzXFxzZXR0aW5ncy1hZHZhbmNlZFxcc2V0dGluZ3MtYWR2YW5jZWQucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9zZXR0aW5ncy1hZHZhbmNlZC9zZXR0aW5ncy1hZHZhbmNlZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR0E7RUFDSSx5QkFBQTtBQ0ZKOztBRElBO0VBQ0kseUJBQUE7QUNESjs7QURLQTtFQUNJLHlCQUFBO0FDRko7O0FESUE7RUFDSSx5QkFBQTtBQ0RKOztBRElBO0VBQ0kseUJBQUE7QUNESjs7QURHQTtFQUNJLHlCQUFBO0FDQUo7O0FERUE7RUFDSSxjQUFBO0VBQ0EscUJBQUE7QUNDSjs7QURHQTtFQUNJLHlCQUFBO0FDQUo7O0FERUE7RUFDSSx5QkFBQTtBQ0NKOztBREdBO0VBQ0kseUJBQUE7QUNBSjs7QURFQTtFQUNJLHlCQUFBO0FDQ0o7O0FERUE7RUFDSSx5QkFBQTtBQ0NKOztBRENBO0VBQ0kseUJBQUE7QUNFSjs7QURBQTtFQUNJLGNBQUE7RUFDQSw0QkFBQTtBQ0dKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc2V0dGluZ3MtYWR2YW5jZWQvc2V0dGluZ3MtYWR2YW5jZWQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcblxyXG5cclxuLmhvY2tleTF7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNEFCRUQ1O1xyXG59XHJcbi5ob2NrZXkye1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNEOURBRDtcclxufVxyXG5cclxuXHJcbi5mb290YmFsbDF7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM0E5NDYzO1xyXG59XHJcbi5mb290YmFsbDJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAgIzMzN0Y1M1xyXG4gICBcclxufVxyXG4uYmxhY2sxe1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojMzU0NTU2XHJcbn1cclxuLmJsYWNrMntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IzM1NDU1NlxyXG59XHJcbmJvZHl7XHJcbiAgICBjb2xvcjojRkZGRkZGO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjpyZWQ7XHJcblxyXG59XHJcblxyXG4uaG9ja2V5MXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM0QUJFRDU7XHJcbn1cclxuLmhvY2tleTJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM0Q5REFEO1xyXG59XHJcblxyXG5cclxuLmZvb3RiYWxsMXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzQTk0NjM7XHJcbn1cclxuLmZvb3RiYWxsMntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICAjMzM3RjUzXHJcbiAgIFxyXG59XHJcbi5ibGFjazF7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiMzNTQ1NTZcclxufVxyXG4uYmxhY2sye1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojMzU0NTU2XHJcbn1cclxuYm9keSB7XHJcbiAgICBjb2xvcjojRkZGRkZGO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcG93ZGVyYmx1ZTtcclxuXHJcbn0iLCIuaG9ja2V5MSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0QUJFRDU7XG59XG5cbi5ob2NrZXkyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNEOURBRDtcbn1cblxuLmZvb3RiYWxsMSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzQTk0NjM7XG59XG5cbi5mb290YmFsbDIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzM3RjUzO1xufVxuXG4uYmxhY2sxIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM1NDU1Njtcbn1cblxuLmJsYWNrMiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNTQ1NTY7XG59XG5cbmJvZHkge1xuICBjb2xvcjogI0ZGRkZGRjtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xufVxuXG4uaG9ja2V5MSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0QUJFRDU7XG59XG5cbi5ob2NrZXkyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNEOURBRDtcbn1cblxuLmZvb3RiYWxsMSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzQTk0NjM7XG59XG5cbi5mb290YmFsbDIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzM3RjUzO1xufVxuXG4uYmxhY2sxIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM1NDU1Njtcbn1cblxuLmJsYWNrMiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNTQ1NTY7XG59XG5cbmJvZHkge1xuICBjb2xvcjogI0ZGRkZGRjtcbiAgYmFja2dyb3VuZC1jb2xvcjogcG93ZGVyYmx1ZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/settings-advanced/settings-advanced.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/settings-advanced/settings-advanced.page.ts ***!
  \*******************************************************************/
/*! exports provided: SettingsAdvancedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsAdvancedPage", function() { return SettingsAdvancedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/model/general-settings */ "./src/app/shared/model/general-settings.ts");
/* harmony import */ var src_app_shared_service_football_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/service/football.service */ "./src/app/shared/service/football.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_shared_service_current_game_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/service/current-game.service */ "./src/app/shared/service/current-game.service.ts");
/* harmony import */ var src_app_shared_service_hockey_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/service/hockey.service */ "./src/app/shared/service/hockey.service.ts");
/* harmony import */ var src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/service/protocol.service */ "./src/app/shared/service/protocol.service.ts");
/* harmony import */ var src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/global-function */ "./src/app/shared/global-function.ts");
/* harmony import */ var src_app_shared_service_debug_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/shared/service/debug.service */ "./src/app/shared/service/debug.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_shared_service_settings_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/shared/service/settings.service */ "./src/app/shared/service/settings.service.ts");












let SettingsAdvancedPage = class SettingsAdvancedPage {
    constructor(footballService, hockeyService, router, currentGameService, protocolService, debugService, formBuilder, settingsService) {
        this.footballService = footballService;
        this.hockeyService = hockeyService;
        this.router = router;
        this.currentGameService = currentGameService;
        this.protocolService = protocolService;
        this.debugService = debugService;
        this.formBuilder = formBuilder;
        this.settingsService = settingsService;
        this.classame = this.constructor.name;
        this.regularTimeTargetMinutes = 0;
        this.regularTimeNumPeriods = 0;
        this.extraTimeTargetMinutes = 0;
        this.forwardCounter = false;
        this.debugService.getdata().subscribe((data) => { this.debugSettings = data; });
        this.form = this.formBuilder.group({
            periodName: "",
            isForwardCounter: 0
        });
    }
    ngOnInit() {
        this.teamNames = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TeamNames"];
    }
    ionViewWillEnter() {
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_8__["GlobalFunction"].isEmpty(this.currentGameService.currentGame)) {
            this.router.navigate(['settings']);
        }
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_8__["GlobalFunction"].isEmpty(this.currentGameService.currentGame.gameType)) {
            this.router.navigate(['settings']);
        }
        // his.currentGame.gameType
        console.log(`${this.classame}  ionViewDidEnter`);
        if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey) {
            this.hockeyService.getdata().subscribe((data) => { this.advancedSportSettings = data; });
        }
        else {
            this.footballService.getdata().subscribe((data) => { this.advancedSportSettings = data; });
            this.regularTimeNumPeriods = 2;
        }
    }
    setSettings(periodSettingId) {
        if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football) {
            this.currentGameService.initFootballDetailValues(periodSettingId);
            // Kennung Fußball
            if (this.debugSettings.debugProtokolWorkflow) {
                debugger;
            }
            this.protocolService.prot20h(0x31, 0x30);
        }
        else { //hockey
            let advancedSetting = this.advancedSportSettings.find(x => x.periodSettingId == periodSettingId);
            if (this.debugSettings.debugProtokolWorkflow) {
                debugger;
            }
            this.currentGameService.initHockeyDetailValues(periodSettingId);
            //nachsteuern des Timers
            if (!src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_8__["GlobalFunction"].isEmpty(advancedSetting.timerDirection)) {
                debugger;
                this.currentGameService.currentGame.timerDirection = advancedSetting.timerDirection;
                if (this.currentGameService.currentGame.timerDirection == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Forward) {
                    this.currentGameService.currentGame.currentMinutes = 0;
                }
            }
            else if (this.forwardCounter) {
                debugger;
                this.currentGameService.currentGame.timerDirection = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Forward;
                this.currentGameService.currentGame.currentMinutes = 0;
            }
            //
            this.protocolService.prot20h(0x30, 0x36);
            // sicherstellen, das die Zeit wieder auf 0 gestellt wird!
        }
        if (this.debugSettings.debugProtokolWorkflow) {
            debugger;
        }
        // senden der Periode
        // beim Fussball keine Bedetung, wird aber trotzdem mit übertragen  
        let setting22H = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["Setting22H"]();
        setting22H.period = "1";
        this.protocolService.prot22h(setting22H);
        // Die Uhrzeit aus den Settings wieder aktualisieren.
        let setting21H = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["Setting21H"]();
        setting21H.flagMid = "2"; // Pause 
        setting21H.signalEnd = "0"; // Kein Signal
        setting21H = src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_8__["GlobalFunction"].setSettings21HFromCurrentGame(this.currentGameService.currentGame, setting21H);
        // Damit müssten die Werte bei der nächsten Protokollübertragung aktualisiert werden
        this.protocolService.setting21H = setting21H;
        // refresh the goals. 
        // there might be settings from the old game.
        this.protocolService.prot23h(this.currentGameService.currentGame.homeGoals, this.currentGameService.currentGame.guestGoals);
        this.teamNames.homeTeamName = this.settingsService.teamNames.homeTeamName;
        this.teamNames.guestTeamName = this.settingsService.teamNames.guestTeamName;
        this.protocolService.prot29h(this.teamNames);
        this.router.navigate(['play']);
    }
    goToSettings() {
        this.router.navigate(['settings']);
    }
    isUndefined() {
        if (!this.isFootball() && !this.isHockey()) {
            return true;
        }
        return false;
    }
    getSportCellClass1() {
        if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey) {
            return "hockey1 cell-class";
        }
        else {
            return "football1 cell-class";
        }
    }
    getSportButtonClass1() {
        var _a;
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_8__["GlobalFunction"].isEmpty((_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame)) {
            return "";
        }
        else {
            if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football) {
                return "football1";
            }
            else {
                return "hockey1";
            }
        }
    }
    getSportButtonClass2() {
        var _a;
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_8__["GlobalFunction"].isEmpty((_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame)) {
            return "";
        }
        else {
            if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football) {
                return "football2";
            }
            else {
                return "hockey2";
            }
        }
    }
    isFootball() {
        var _a, _b;
        return (((_b = (_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame) === null || _b === void 0 ? void 0 : _b.gameType) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football);
    }
    isHockey() {
        var _a, _b;
        return (((_b = (_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame) === null || _b === void 0 ? void 0 : _b.gameType) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey);
    }
    minNormalTimeAdd() {
        if (this.regularTimeTargetMinutes > 48) {
            this.regularTimeTargetMinutes = 1;
        }
        else {
            this.regularTimeTargetMinutes++;
        }
    }
    minNormalTimeSubstract() {
        if (this.regularTimeTargetMinutes < 1) {
            this.regularTimeTargetMinutes = 49;
        }
        else {
            this.regularTimeTargetMinutes--;
        }
    }
    minExtraTimeAdd() {
        if (this.extraTimeTargetMinutes > 48) {
            this.extraTimeTargetMinutes = 1;
        }
        else {
            this.extraTimeTargetMinutes++;
        }
    }
    minExtraTimeSubstract() {
        if (this.extraTimeTargetMinutes < 1) {
            this.extraTimeTargetMinutes = 49;
        }
        else {
            this.extraTimeTargetMinutes--;
        }
    }
    periodAdd() {
        this.regularTimeNumPeriods++;
    }
    periodSubstract() {
        this.regularTimeNumPeriods--;
    }
    deleteSetting(settingId) {
        if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey) {
            this.hockeyService.deleteSetting(settingId);
        }
        else {
            this.footballService.deleteSetting(settingId);
        }
    }
    saveNewGame() {
        // muss Größer 0 sein.
        if (this.regularTimeTargetMinutes == 0) {
            return;
        }
        let periodName = this.form.controls['periodName'].value;
        this.forwardCounter = this.form.controls['isForwardCounter'].value;
        debugger;
        let normalTimeSetting = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["PeriodSettings"]();
        normalTimeSetting.durationMin = this.regularTimeTargetMinutes;
        normalTimeSetting.settingName = periodName;
        if (this.currentGameService.currentGame.gameType == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey) {
            // Ist nur für Hockey dynamisch
            normalTimeSetting.periodAmounts = this.regularTimeNumPeriods;
            this.hockeyService.addNewNormalPeriod(normalTimeSetting);
            this.setSettings(this.hockeyService.newSettingId);
        }
        else {
            // fussball json hinzufügen
            normalTimeSetting.periodAmounts = this.regularTimeNumPeriods;
            this.footballService.addNewNormalPeriod(normalTimeSetting);
            // Standard 2 Halbzeiten
            if (this.extraTimeTargetMinutes > 0) {
                let extraTimeSetting = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_2__["ExtraTimeSettings"]();
                // Standard 2 Verlängerungen
                extraTimeSetting.periodAmounts = 2;
                extraTimeSetting.durationMin = this.extraTimeTargetMinutes;
                this.footballService.addNewExtraPeriod(extraTimeSetting);
            }
            this.setSettings(this.footballService.newSettingId);
        }
    }
};
SettingsAdvancedPage.ctorParameters = () => [
    { type: src_app_shared_service_football_service__WEBPACK_IMPORTED_MODULE_3__["FootballService"] },
    { type: src_app_shared_service_hockey_service__WEBPACK_IMPORTED_MODULE_6__["HockeyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_shared_service_current_game_service__WEBPACK_IMPORTED_MODULE_5__["CurrentGameService"] },
    { type: src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_7__["ProtocolService"] },
    { type: src_app_shared_service_debug_service__WEBPACK_IMPORTED_MODULE_9__["DebugService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormBuilder"] },
    { type: src_app_shared_service_settings_service__WEBPACK_IMPORTED_MODULE_11__["SettingsService"] }
];
SettingsAdvancedPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-settings-advanced',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./settings-advanced.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/settings-advanced/settings-advanced.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./settings-advanced.page.scss */ "./src/app/pages/settings-advanced/settings-advanced.page.scss")).default]
    })
], SettingsAdvancedPage);



/***/ })

}]);
//# sourceMappingURL=pages-settings-advanced-settings-advanced-module-es2015.js.map