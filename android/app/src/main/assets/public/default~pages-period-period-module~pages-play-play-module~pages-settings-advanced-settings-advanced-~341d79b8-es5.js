function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-period-period-module~pages-play-play-module~pages-settings-advanced-settings-advanced-~341d79b8"], {
  /***/
  "./src/app/shared/service/current-game.service.ts":
  /*!********************************************************!*\
    !*** ./src/app/shared/service/current-game.service.ts ***!
    \********************************************************/

  /*! exports provided: CurrentGameService */

  /***/
  function srcAppSharedServiceCurrentGameServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CurrentGameService", function () {
      return CurrentGameService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _model_general_settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../model/general-settings */
    "./src/app/shared/model/general-settings.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _football_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./football.service */
    "./src/app/shared/service/football.service.ts");
    /* harmony import */


    var _hockey_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./hockey.service */
    "./src/app/shared/service/hockey.service.ts");
    /* harmony import */


    var _global_function__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../global-function */
    "./src/app/shared/global-function.ts");
    /* harmony import */


    var _protocol_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./protocol.service */
    "./src/app/shared/service/protocol.service.ts");
    /* harmony import */


    var _settings_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./settings.service */
    "./src/app/shared/service/settings.service.ts");

    var CurrentGameService = /*#__PURE__*/function () {
      function CurrentGameService(footballService, hockeyService, protocolService, settingsService) {
        _classCallCheck(this, CurrentGameService);

        this.footballService = footballService;
        this.hockeyService = hockeyService;
        this.protocolService = protocolService;
        this.settingsService = settingsService;
        this.GameChanges = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();

        if (this.currentGame == null) {
          this.currentGame = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["CurrentGame"]();
        }
      }

      _createClass(CurrentGameService, [{
        key: "changeHandler",
        value: function changeHandler() {
          this.GameChanges.next(this.currentGame);
        }
      }, {
        key: "setTeamNames",
        value: function setTeamNames() {
          this.currentGame.homeTeamName = this.settingsService.teamNames.homeTeamName;
          this.currentGame.guestTeamName = this.settingsService.teamNames.guestTeamName;
        } // Verlängerung hockey bisher nicht eingeplant.

      }, {
        key: "internHockeySetter",
        value: function internHockeySetter(hockeySetting) {
          if (!_global_function__WEBPACK_IMPORTED_MODULE_6__["GlobalFunction"].isEmpty(hockeySetting)) {
            this.currentGame.regularTimeTargetMinutes = 0;
            this.currentGame.periodMinutes = hockeySetting.durationMin;
            this.currentGame.currentMinutes = hockeySetting.durationMin;
            this.currentGame.regularTimeTargetMinutes = hockeySetting.durationMin;
            this.currentGame.regularTimeNumPeriods = hockeySetting.periodAmounts;
            this.currentGame.extraTimeTargetMinutes = 0;
            this.currentGame.extraTimeNumPeriods = 0;
            this.currentGame.extraTimeAvailable = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["ExtraTimeAvailable"].No;
            this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].init;
            this.setTeamNames();
          }
        }
      }, {
        key: "internFootballSetter",
        value: function internFootballSetter(footballSetting, periodSettingId) {
          this.currentGame.periodMinutes = footballSetting.durationMin;
          this.currentGame.targetMinutes = footballSetting.durationMin;
          this.currentGame.regularTimeTargetMinutes = footballSetting.durationMin;
          this.currentGame.regularTimeNumPeriods = footballSetting.periodAmounts;
          this.setTeamNames(); // zeiten fuer die moegliche verlanderung schon zu beginn des Spiels laden

          var extraTime = this.footballService.footballSettingsExtraTime.find(function (x) {
            return x.relatedSettingId == periodSettingId;
          });

          if (!_global_function__WEBPACK_IMPORTED_MODULE_6__["GlobalFunction"].isEmpty(extraTime)) {
            this.currentGame.extraTimeAvailable = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["ExtraTimeAvailable"].Yes;
            this.currentGame.extraTimeTargetMinutes = extraTime.durationMin;
            this.currentGame.extraTimeNumPeriods = extraTime.periodAmounts;
          }
        }
      }, {
        key: "getdata",
        value: function getdata() {
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(this.currentGame);
        }
      }, {
        key: "initHockeyValues",
        value: function initHockeyValues() {
          this.currentGame.sumPeriods = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].No;
          this.currentGame.gameType = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey;
          this.currentGame.period = 1;
          this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].init;
          this.currentGame.timerDirection = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Backward;
          this.currentGame.timePeriod = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal;
          this.currentGame.currentSec = 0;
          this.currentGame.homeGoals = 0;
          this.currentGame.guestGoals = 0; // es ist nur eine Spielzeit vorgesehen. Daher wird direkt initialisiert. 

          if (this.hockeyService.hockeySettings.length == 1) {
            var hockeySetting = this.hockeyService.hockeySettings[0];
            this.internHockeySetter(hockeySetting);
          }

          this.changeHandler();
        } // noch nicht getstet. bei meheren Spielzeiten für hockey relevant

      }, {
        key: "initHockeyDetailValues",
        value: function initHockeyDetailValues(periodSettingId) {
          var hockeySetting = this.hockeyService.hockeySettings.find(function (x) {
            return x.periodSettingId == periodSettingId;
          });
          this.internHockeySetter(hockeySetting);
        }
      }, {
        key: "initFootballValues",
        value: function initFootballValues() {
          this.currentGame = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["CurrentGame"]();
          this.currentGame.gameType = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football;
          this.currentGame.sumPeriods = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].Yes;
          this.changeHandler();
        }
      }, {
        key: "initFootballDetailValues",
        value: function initFootballDetailValues(periodSettingId) {
          this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].init;
          this.currentGame.sumPeriods = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].Yes;
          this.currentGame.timePeriod = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal;
          this.currentGame.timerDirection = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Forward;
          this.currentGame.currentSec = 0;
          this.currentGame.currentMinutes = 0;
          this.currentGame.period = 1;
          this.currentGame.homeGoals = 0;
          this.currentGame.guestGoals = 0;
          var footballSetting = this.footballService.footballSettings.find(function (x) {
            return x.periodSettingId == periodSettingId;
          });
          this.internFootballSetter(footballSetting, periodSettingId);
          this.changeHandler();
        }
      }, {
        key: "hockeyPeriodFinished",
        value: function hockeyPeriodFinished() {
          if (_global_function__WEBPACK_IMPORTED_MODULE_6__["GlobalFunction"].isEmpty(this.currentGame)) {
            return 0;
          }

          if (this.currentGame.gameType == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey) {
            if (this.currentGame.gameStatus == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].periodFinished || this.currentGame.gameStatus == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished) {
              return true;
            }
          }

          return false;
        } // Ausgabe in Abhängigkeit von Vorwartszähler Rückwärtszähler
        // und Ausgabe accumuiliert oder nicht.
        // nicht geprüft ob Rückwärtszähler akkumuliert ebenfalls funktioniert.

      }, {
        key: "getSportReleateMinutesValues",
        value: function getSportReleateMinutesValues(currentGame) {
          if (_global_function__WEBPACK_IMPORTED_MODULE_6__["GlobalFunction"].isEmpty(currentGame)) {
            return 0;
          }

          var minFromPeriod = 0;
          var sumperiods = currentGame.sumPeriods; // akumuliert

          if (sumperiods == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].Yes) {
            if (currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) {
              //normale Spielzeit
              if (currentGame.period > 1) {
                minFromPeriod = (currentGame.period - 1) * currentGame.regularTimeTargetMinutes;
              }
            } else {
              // Verlängerung
              minFromPeriod = (currentGame.period - 1 - currentGame.regularTimeNumPeriods) * this.currentGame.extraTimeTargetMinutes;
            }

            var totalMin = this.currentGame.currentMinutes + minFromPeriod;
            return totalMin;
          }

          if (currentGame.currentMinutes > 0) {
            return currentGame.currentMinutes;
          }

          return 0;
        }
      }, {
        key: "getSportReleateMinutesService",
        value: function getSportReleateMinutesService() {
          var currentGame = this.currentGame;
          return this.getSportReleateMinutesValues(currentGame);
        } // Die Anzeige wird auf Ende des Spiel gesetzt.
        // Wird zur Zeit nicht benutzt.

      }, {
        key: "setEndGameSettings",
        value: function setEndGameSettings() {
          this.currentGame.currentSec = 0;

          if (this.currentGame.timerDirection == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Backward) {
            this.currentGame.currentMinutes = 0;
            this.currentGame.currentSec = 0;

            if (this.currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) {
              //normale Spielzeit
              this.currentGame.period = this.currentGame.regularTimeNumPeriods;
            } else {
              this.currentGame.period = this.currentGame.extraTimeNumPeriods;
            }
          } else {
            if (this.currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) {
              //normale Spielzeit
              this.currentGame.period = this.currentGame.regularTimeNumPeriods;

              if (this.currentGame.sumPeriods == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].Yes) {
                this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
              } else {
                this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
              }
            } else {
              if (this.currentGame.sumPeriods == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].Yes) {
                this.currentGame.currentMinutes = this.currentGame.extraTimeNumPeriods * this.currentGame.extraTimeTargetMinutes;
              } else {
                this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
              }
            }
          }

          this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished;
        }
      }, {
        key: "finishGame",
        value: function finishGame() {
          // Die Anzeige wird nicht verändert.
          this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished;
          this.currentGame.currentMinutes = 0;
          this.currentGame.currentSec = 0;
          this.currentGame.period = 0;
          this.changeHandler();
        }
      }, {
        key: "deleteCurrentGame",
        value: function deleteCurrentGame() {
          this.currentGame = null;
          this.changeHandler();
        }
      }, {
        key: "changeCurrentGameWithDependencies",
        value: function changeCurrentGameWithDependencies(currentGame) {
          var teamNames = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TeamNames"]();
          teamNames.homeTeamName = currentGame.homeTeamName;
          teamNames.guestTeamName = currentGame.guestTeamName;
          currentGame.guestTeamName;
          this.settingsService.updateTeamNames(teamNames);
          this.currentGame = currentGame;
          this.changeHandler();
        }
      }, {
        key: "startNextPeriod",
        value: function startNextPeriod() {
          this.currentGame.period++;
          this.currentGame.currentSec = 0;

          if (this.currentGame.timerDirection == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Backward) {
            //rueckwarszähler z.B start bei 20:00 
            if (this.currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) {
              this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
            } else {
              this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
            }
          } else {
            //vorwärts
            this.currentGame.currentMinutes = 0;
          }

          if (this.currentGame.period > this.currentGame.regularTimeNumPeriods) {
            this.currentGame.timePeriod = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].renewal;
            this.currentGame.targetMinutes = this.currentGame.extraTimeTargetMinutes;
          } else {
            this.currentGame.targetMinutes = this.currentGame.regularTimeTargetMinutes;
          }

          this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].pause; // hack refresh the display 
          // not perfect from this point, but this short solution works.   

          var setting21H = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["Setting21H"]();
          setting21H.flagMid = _protocol_service__WEBPACK_IMPORTED_MODULE_7__["ProtocolService"].setting21GameIsNotRunningFlagASCII;
          setting21H.signalEnd = this.protocolService.setting21H.signalEnd;
          setting21H = _global_function__WEBPACK_IMPORTED_MODULE_6__["GlobalFunction"].setSettings21HFromCurrentGame(this.currentGame, setting21H);
          this.protocolService.setting21H = setting21H;
          this.changeHandler();
        }
      }, {
        key: "goalTeam1Add",
        value: function goalTeam1Add() {
          if (this.currentGame.homeGoals < 99) {
            this.currentGame.homeGoals++;
          } else {
            this.currentGame.homeGoals = 0;
          }

          this.changeHandler();
        }
      }, {
        key: "goalTeam2Add",
        value: function goalTeam2Add() {
          if (this.currentGame.guestGoals < 99) {
            this.currentGame.guestGoals++;
          } else {
            this.currentGame.guestGoals = 0;
          }

          this.changeHandler();
        }
      }, {
        key: "goalTeam1Substract",
        value: function goalTeam1Substract() {
          if (this.currentGame.homeGoals > 0) {
            this.currentGame.homeGoals--;
          } else {
            this.currentGame.homeGoals = 99;
          }

          this.changeHandler();
        }
      }, {
        key: "goalTeam2Substract",
        value: function goalTeam2Substract() {
          if (this.currentGame.guestGoals > 0) {
            this.currentGame.guestGoals--;
          } else {
            this.currentGame.guestGoals = 99;
          }

          this.changeHandler();
        } // sekundenzaehler.

      }, {
        key: "tick",
        value: function tick() {
          if (this.currentGame.timerDirection == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Backward) {
            if (this.currentGame.currentSec > 0) {
              //nur sekundenzähler
              this.currentGame.currentSec--;
            } else {
              if (this.currentGame.currentMinutes > 0) {
                //miuntenzähler
                this.currentGame.currentMinutes--;
                this.currentGame.currentSec = 59;
              } else {
                //Periode oder Spiel beendet
                var numTotalPeriods = 0;

                if (this.currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) {
                  numTotalPeriods = this.currentGame.regularTimeNumPeriods; //this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
                } else {
                  // noch nicht getestet
                  numTotalPeriods = this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods; //this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
                }

                if (this.currentGame.period < numTotalPeriods) {
                  // Periode beendet.
                  this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].periodFinished;
                } else {
                  // Spiel oder Verlaengerung beendet
                  this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished;
                }

                this.changeHandler();
              }

              ;
            }
          } else {
            // forward fussball
            if (this.currentGame.currentSec < 59) {
              this.currentGame.currentSec++;
            } else {
              this.currentGame.currentSec = 0;
              this.currentGame.currentMinutes++;

              if (this.currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal && this.currentGame.currentMinutes < this.currentGame.regularTimeTargetMinutes) {// normale spielezeit
              } else if (this.currentGame.currentMinutes < this.currentGame.extraTimeTargetMinutes) {// verlängerung
              } else {
                // spiel oder Verlängerung beendet
                if (this.currentGame.period == this.currentGame.regularTimeNumPeriods || this.currentGame.period == this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods) {
                  // Spiel oder Verlängerung beendet.
                  this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished;
                } else {
                  // Spielzeit beendet
                  this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].periodFinished;
                }

                this.changeHandler();
              }

              ;
            }
          } // ende fussball

        }
      }]);

      return CurrentGameService;
    }();

    CurrentGameService.ctorParameters = function () {
      return [{
        type: _football_service__WEBPACK_IMPORTED_MODULE_4__["FootballService"]
      }, {
        type: _hockey_service__WEBPACK_IMPORTED_MODULE_5__["HockeyService"]
      }, {
        type: _protocol_service__WEBPACK_IMPORTED_MODULE_7__["ProtocolService"]
      }, {
        type: _settings_service__WEBPACK_IMPORTED_MODULE_8__["SettingsService"]
      }];
    };

    CurrentGameService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], CurrentGameService);
    /***/
  },

  /***/
  "./src/app/shared/service/debug.service.ts":
  /*!*************************************************!*\
    !*** ./src/app/shared/service/debug.service.ts ***!
    \*************************************************/

  /*! exports provided: DebugService */

  /***/
  function srcAppSharedServiceDebugServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DebugService", function () {
      return DebugService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _model_general_settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../model/general-settings */
    "./src/app/shared/model/general-settings.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var DebugService = /*#__PURE__*/function () {
      function DebugService() {
        _classCallCheck(this, DebugService);

        this.debugSettings = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["DebugSettings"]();
      }

      _createClass(DebugService, [{
        key: "getdata",
        value: function getdata() {
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(this.debugSettings);
        }
      }]);

      return DebugService;
    }();

    DebugService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], DebugService);
    /***/
  },

  /***/
  "./src/app/shared/service/football.service.ts":
  /*!****************************************************!*\
    !*** ./src/app/shared/service/football.service.ts ***!
    \****************************************************/

  /*! exports provided: FootballService */

  /***/
  function srcAppSharedServiceFootballServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FootballService", function () {
      return FootballService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _data_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./data-storage.service */
    "./src/app/shared/service/data-storage.service.ts");
    /* harmony import */


    var _global_function__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../global-function */
    "./src/app/shared/global-function.ts");
    /* harmony import */


    var _debug_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./debug.service */
    "./src/app/shared/service/debug.service.ts"); // https://edupala.com/ionic-capacitor-storage/


    var FootballService = /*#__PURE__*/function () {
      function FootballService(dataStorageService, debugService) {
        var _this = this;

        _classCallCheck(this, FootballService);

        this.dataStorageService = dataStorageService;
        this.debugService = debugService;
        this.footballSettings = [];
        this.footballSettingsFiltered = [];
        this.footballSettingsExtraTime = [];
        this.getFootballSettings();
        this.getFootballSettingsExtraTime();
        this.debugService.getdata().subscribe(function (data) {
          _this.debugSettings = data;
        });
      } // Initialisieren der Werte, werden in den Local Storage geschrieben. 


      _createClass(FootballService, [{
        key: "initBasicSettingsFromConfig",
        value: function initBasicSettingsFromConfig() {
          var _this2 = this;

          fetch('./assets/data/footballSettings.json').then(function (res) {
            return res.json();
          }).then(function (json) {
            _this2.footballSettings = json;
            var jsonString = JSON.stringify(_this2.footballSettings);

            _this2.dataStorageService.setString('footballSettings', jsonString);
          });
        } // Initialisieren der Werte für die Verlängerung, werden in den Local Storage geschrieben. 

      }, {
        key: "initExtraSettingsFromConfig",
        value: function initExtraSettingsFromConfig() {
          var _this3 = this;

          fetch('./assets/data/footballSettingsExtraTime.json').then(function (res) {
            return res.json();
          }).then(function (json) {
            _this3.footballSettingsExtraTime = json;
            var jsonString = JSON.stringify(_this3.footballSettingsExtraTime);

            _this3.dataStorageService.setString('footballSettingsExtraTime', jsonString);
          });
        }
      }, {
        key: "getFootballSettings",
        value: function getFootballSettings() {
          var _this4 = this;

          this.dataStorageService.getString('footballSettings').then(function (data) {
            if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null) {
              _this4.footballSettings = JSON.parse(data.value); //this.filterdata();

              if (_this4.debugSettings.displayJson) {
                console.log(_this4.footballSettings);
              }
            } else {
              _this4.initBasicSettingsFromConfig();
            }
          });
        }
      }, {
        key: "getFootballSettingsExtraTime",
        value: function getFootballSettingsExtraTime() {
          var _this5 = this;

          this.dataStorageService.getString('footballSettingsExtraTime').then(function (data) {
            if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null) {
              _this5.footballSettingsExtraTime = JSON.parse(data.value);

              if (_this5.debugSettings.displayJson) {
                console.log(_this5.footballSettingsExtraTime);
              }
            } else {
              _this5.initExtraSettingsFromConfig();
            }
          });
        } // currently not used, but filterint the data with search would be possible.

      }, {
        key: "filterdata",
        value: function filterdata() {
          var filterSettings1 = "U 11";
          var filterSettings2 = "U 12";
          this.footballSettingsFiltered = this.footballSettings.filter(function (x) {
            return x.settingName.indexOf(filterSettings1) !== -1 || x.settingName.indexOf(filterSettings2) !== -1;
          });
        }
      }, {
        key: "getdata",
        value: function getdata() {
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.footballSettings);
        }
      }, {
        key: "deleteCustomConfiguration",
        value: function deleteCustomConfiguration() {
          this.dataStorageService.removeItem("footballSettings");
          this.dataStorageService.removeItem("footballSettingsExtraTime");
        }
      }, {
        key: "deleteSetting",
        value: function deleteSetting(settingID) {
          // elemente aus dem Array entfernen.
          var index = this.footballSettings.findIndex(function (x) {
            return x.periodSettingId == settingID;
          });

          if (index == -1) {
            return;
          }

          this.footballSettings.splice(index, 1); // alten Konfigurationswerte überschreiben

          var jsonStr = JSON.stringify(this.footballSettings);
          this.dataStorageService.setString('footballSettings', jsonStr); // ggf. Verlängerungen ebenfalls löschen 

          var indexExtraTime = this.footballSettingsExtraTime.findIndex(function (x) {
            return x.relatedSettingId == settingID;
          });

          if (indexExtraTime != -1) {
            this.footballSettingsExtraTime.splice(indexExtraTime, 1);
            jsonStr = JSON.stringify(this.footballSettingsExtraTime);
            this.dataStorageService.setString('footballSettingsExtraTime', jsonStr);
          }
        }
      }, {
        key: "addNewNormalPeriod",
        value: function addNewNormalPeriod(footballSetting) {
          this.newSettingId = Math.max.apply(Math, _toConsumableArray(this.footballSettings.map(function (o) {
            return o.periodSettingId;
          }))) + 1; //footballSetting.settingName = footballSetting.durationMin.toString();

          footballSetting.periodSettingId = this.newSettingId;
          this.footballSettings.push(footballSetting); // alten Konfigurationswert überschreiben

          var jsonStr = JSON.stringify(this.footballSettings);
          this.dataStorageService.setString('footballSettings', jsonStr);
        }
      }, {
        key: "addNewExtraPeriod",
        value: function addNewExtraPeriod(footballExtraTimeSetting) {
          this.newExraTimeSettingId = Math.max.apply(Math, _toConsumableArray(this.footballSettingsExtraTime.map(function (o) {
            return o.extraTimeSettingId;
          }))) + 1;
          footballExtraTimeSetting.relatedSettingId = this.newSettingId;
          footballExtraTimeSetting.extraTimeSettingId = this.newExraTimeSettingId;
          this.footballSettingsExtraTime.push(footballExtraTimeSetting); // alten Konfigurationswert überschreiben

          var jsonString = JSON.stringify(this.footballSettingsExtraTime);
          this.dataStorageService.setString('footballSettingsExtraTime', jsonString);
        }
      }]);

      return FootballService;
    }();

    FootballService.ctorParameters = function () {
      return [{
        type: _data_storage_service__WEBPACK_IMPORTED_MODULE_3__["DataStorageService"]
      }, {
        type: _debug_service__WEBPACK_IMPORTED_MODULE_5__["DebugService"]
      }];
    };

    FootballService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], FootballService);
    /***/
  },

  /***/
  "./src/app/shared/service/hockey.service.ts":
  /*!**************************************************!*\
    !*** ./src/app/shared/service/hockey.service.ts ***!
    \**************************************************/

  /*! exports provided: HockeyService */

  /***/
  function srcAppSharedServiceHockeyServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HockeyService", function () {
      return HockeyService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _data_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./data-storage.service */
    "./src/app/shared/service/data-storage.service.ts");
    /* harmony import */


    var _global_function__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../global-function */
    "./src/app/shared/global-function.ts");
    /* harmony import */


    var _debug_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./debug.service */
    "./src/app/shared/service/debug.service.ts");

    var HockeyService = /*#__PURE__*/function () {
      function HockeyService(dataStorageService, debugService) {
        var _this6 = this;

        _classCallCheck(this, HockeyService);

        this.dataStorageService = dataStorageService;
        this.debugService = debugService;
        this.hockeySettings = [];
        this.hockeySettingsExtraTime = [];
        this.getBasicSettings();
        this.debugService.getdata().subscribe(function (data) {
          _this6.debugSettings = data;
        });
      } // Initialisieren der Werte: werden in den Local Storage geschrieben. 


      _createClass(HockeyService, [{
        key: "initBasicSettingsFromConfig",
        value: function initBasicSettingsFromConfig() {
          var _this7 = this;

          fetch('./assets/data/hockeySettings.json').then(function (res) {
            return res.json();
          }).then(function (json) {
            _this7.hockeySettings = json;
            var jsonString = JSON.stringify(_this7.hockeySettings);

            _this7.dataStorageService.setString('hockeySettings', jsonString);
          });
        }
      }, {
        key: "getdata",
        value: function getdata() {
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.hockeySettings);
        }
      }, {
        key: "getBasicSettings",
        value: function getBasicSettings() {
          var _this8 = this;

          this.dataStorageService.getString('hockeySettings').then(function (data) {
            if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null) {
              _this8.hockeySettings = JSON.parse(data.value);

              if (_this8.debugSettings.displayJson) {
                console.log(_this8.hockeySettings);
              }
            } else {
              _this8.initBasicSettingsFromConfig();
            }
          });
        }
      }, {
        key: "deleteCustomConfiguration",
        value: function deleteCustomConfiguration() {
          this.dataStorageService.removeItem("hockeySettings");
        }
      }, {
        key: "deleteSetting",
        value: function deleteSetting(settingID) {
          var index = this.hockeySettings.findIndex(function (x) {
            return x.periodSettingId == settingID;
          });

          if (index == -1) {
            return;
          }

          this.hockeySettings.splice(index, 1); // alten Konfigurationswert überschreiben

          var jsonStr = JSON.stringify(this.hockeySettings);
          this.dataStorageService.setString('hockeySettings', jsonStr);
        }
      }, {
        key: "addNewNormalPeriod",
        value: function addNewNormalPeriod(periodSetting) {
          this.newSettingId = Math.max.apply(Math, _toConsumableArray(this.hockeySettings.map(function (o) {
            return o.periodSettingId;
          }))) + 1;
          periodSetting.periodSettingId = this.newSettingId;
          this.hockeySettings.push(periodSetting); // alten Konfigurationswert überschreiben

          var jsonStr = JSON.stringify(this.hockeySettings);
          this.dataStorageService.setString('hockeySettings', jsonStr);
        }
      }]);

      return HockeyService;
    }();

    HockeyService.ctorParameters = function () {
      return [{
        type: _data_storage_service__WEBPACK_IMPORTED_MODULE_3__["DataStorageService"]
      }, {
        type: _debug_service__WEBPACK_IMPORTED_MODULE_5__["DebugService"]
      }];
    };

    HockeyService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], HockeyService);
    /***/
  },

  /***/
  "./src/app/shared/service/protocol.service.ts":
  /*!****************************************************!*\
    !*** ./src/app/shared/service/protocol.service.ts ***!
    \****************************************************/

  /*! exports provided: ProtocolService */

  /***/
  function srcAppSharedServiceProtocolServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProtocolService", function () {
      return ProtocolService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _model_general_settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../model/general-settings */
    "./src/app/shared/model/general-settings.ts");
    /* harmony import */


    var _global_function__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../global-function */
    "./src/app/shared/global-function.ts");
    /* harmony import */


    var _debug_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./debug.service */
    "./src/app/shared/service/debug.service.ts");
    /* harmony import */


    var _bluetooth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./bluetooth.service */
    "./src/app/shared/service/bluetooth.service.ts");
    /* harmony import */


    var _settings_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./settings.service */
    "./src/app/shared/service/settings.service.ts");

    var SETTING21_GAME_SIGNAL_ENDE_Off_FLAG_ASCII = "0";
    var SETTING21_GAME_SIGNAL_ENDE_ON_FLAG_ASCII = "1";
    var SETTING22_Pfeil = " ";
    var SETTING22_FLAG_LINKS = " ";
    var SETTING22_FLAG_RECHTS = " ";

    var ProtocolService = /*#__PURE__*/function () {
      function ProtocolService(debugService, bluetoothService, settingsService) {
        var _this9 = this;

        _classCallCheck(this, ProtocolService);

        this.debugService = debugService;
        this.bluetoothService = bluetoothService;
        this.settingsService = settingsService; // Verbindung zur Anzeige

        this.isConnected = false; // Bestimmt die Decodierung

        this.decodeCounter = 0; // steuert die Zeitübertragung jede Sekunde

        this.interval = 1000; // Erste Zeichen, das übertragen wird: ASCII STX

        this.startByte = 0x2; // Letzete Zeichen, das übertragen wird: ASCII ETX

        this.endByte = 0x3; // Uhr aktiv / nicht aktiv

        this.setting20HUhrAktiv = "1"; // hex 31

        this.setting20HUhrNichtAktiv = "0"; // hex 30
        // Zeahler senden Hupensignal 

        this.signalSentCounter = 0; // die 1/10 Sekunden werden nicht übertragen.

        this.setting21SecDiv10FlagASCII = " "; // zeigt an, ob die sekündliche Übertragung der Zeit aktiv ist.

        this.transmitTimeIsActive = false; // Zeiteinstellungen (settings) Protokoll für die sekündliche Übertragung.

        this.setting21H = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["Setting21H"](); // Decoderungstabelle

        this.decodeValuesAr = [{
          "index": 0,
          "identifier": 0x30,
          "byte1": 0x85,
          "byte2": 0xBA,
          "byte3": 0x90,
          "byte4": 0xA5
        }, {
          "index": 1,
          "identifier": 0x31,
          "byte1": 0xB1,
          "byte2": 0xA2,
          "byte3": 0x9F,
          "byte4": 0x8B
        }, {
          "index": 2,
          "identifier": 0x32,
          "byte1": 0xAA,
          "byte2": 0xB2,
          "byte3": 0xA7,
          "byte4": 0x99
        }, {
          "index": 3,
          "identifier": 0x33,
          "byte1": 0x97,
          "byte2": 0xA6,
          "byte3": 0xB3,
          "byte4": 0x85
        }, {
          "index": 4,
          "identifier": 0x34,
          "byte1": 0x9D,
          "byte2": 0x84,
          "byte3": 0xBC,
          "byte4": 0xAD
        }, {
          "index": 5,
          "identifier": 0x35,
          "byte1": 0x8F,
          "byte2": 0xA8,
          "byte3": 0xBD,
          "byte4": 0xB1
        }, {
          "index": 6,
          "identifier": 0x36,
          "byte1": 0x9C,
          "byte2": 0xA0,
          "byte3": 0x87,
          "byte4": 0xBA
        }, {
          "index": 7,
          "identifier": 0x37,
          "byte1": 0xC1,
          "byte2": 0xC7,
          "byte3": 0x99,
          "byte4": 0x88
        }, {
          "index": 8,
          "identifier": 0x38,
          "byte1": 0xB2,
          "byte2": 0x86,
          "byte3": 0x95,
          "byte4": 0x84
        }, {
          "index": 9,
          "identifier": 0x39,
          "byte1": 0x83,
          "byte2": 0xB7,
          "byte3": 0x81,
          "byte4": 0xB0
        }];
        this.debugService.getdata().subscribe(function (data) {
          _this9.debugSettings = data;
        }); // react on connection changes

        this.isConnected = false;
        this.subscription = this.bluetoothService.ConnectionChanges.subscribe(function (data) {
          _this9.isConnected = data;
        }); // Solange eine Übertragung besteht
        // müssen beim Neustart die alten Werte übernommen werden
        // Settings für die sekündliche Übertragung

        this.setting21H.min10 = "0";
        this.setting21H.min1 = "0";
        this.setting21H.sec10 = "0";
        this.setting21H.sec1 = "0";
        this.setting21H.secDiv10 = " ";
        this.setting21H.flagMid = "2"; //Pausiert

        this.setting21H.signalEnd = "0"; //Kein Signal
      }

      _createClass(ProtocolService, [{
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subscription.unsubscribe();
        } // nur der Inhalt wird decodiert

      }, {
        key: "decodeValues",
        value: function decodeValues(numArray, start, end) {
          if (end > numArray.length) {
            // error
            return null;
          } // das dritte Element enthält immer den Decodierungsschlüssel.


          var decodeElement = this.decodeValuesAr.find(function (x) {
            return x.identifier == numArray[2];
          });

          if (_global_function__WEBPACK_IMPORTED_MODULE_3__["GlobalFunction"].isEmpty(decodeElement)) {
            // error
            return null;
          } // decodierung is deakiviert. Zu Testzwecken eingebaut.


          if (!this.debugSettings.decodeProtocol) {
            return numArray;
          }

          var count = 1;

          for (var index = start; index <= end; index++) {
            switch (count) {
              case 1:
                numArray[index] = _global_function__WEBPACK_IMPORTED_MODULE_3__["GlobalFunction"].xor(numArray[index], decodeElement.byte1);
                break;

              case 2:
                numArray[index] = _global_function__WEBPACK_IMPORTED_MODULE_3__["GlobalFunction"].xor(numArray[index], decodeElement.byte2);
                break;

              case 3:
                numArray[index] = _global_function__WEBPACK_IMPORTED_MODULE_3__["GlobalFunction"].xor(numArray[index], decodeElement.byte3);
                break;

              case 4:
                numArray[index] = _global_function__WEBPACK_IMPORTED_MODULE_3__["GlobalFunction"].xor(numArray[index], decodeElement.byte4);
                break;
            }

            count++;

            if (count == 5) {
              // es sind nur 4 Schlüssel vorhanden => Beginne wieder mit dem ersten Schlüssel
              count = 1;
            }
          }

          return numArray;
        } // Start der übertragung an die Anzeige

      }, {
        key: "startTransmitTimer",
        value: function startTransmitTimer() {
          var _this10 = this;

          // sekündliche Übertragung wird gestartet
          if (this.transmitTimeIsActive) {
            return;
          }

          if (this.debugSettings.deactiveSentProtocolTimer) {
            return;
          } // Intevall für die sekündliche Übertragung der Zeit


          this.intervalTimer = setInterval(function () {
            _this10.prot21h();
          }, this.interval);
          this.transmitTimeIsActive = true;
        } /// Ende der Übertragung an die Anzeige

      }, {
        key: "endTransmitTimer",
        value: function endTransmitTimer() {
          if (this.debugSettings.deactiveSentProtocolTimer) {
            return;
          }

          clearInterval(this.intervalTimer);
          this.transmitTimeIsActive = false;

          if (this.debugSettings.logAdditionalInformation) {
            console.log("Die sekündliche Übertragung der stehenden oder laufenden Zeit wurde beendet.");
            console.log("Damit müsste sich die Anzeige auch ausschalten.");
          }
        } // Aktion wird bei Ende einer Periode Hockey und manuelles auslösen bei Hockey aktiviert.

      }, {
        key: "sentTimeWithSignal",
        value: function sentTimeWithSignal() {
          this.signalSentCounter = 5;
        }
      }, {
        key: "getIdentDecodeFromIndex",
        value: function getIdentDecodeFromIndex(index) {
          var element = this.decodeValuesAr.find(function (x) {
            return x.index == index;
          });

          if (element) {
            return element.identifier;
          }

          return 0;
        } // Erkennung der Sportart und Synchronisieren der Uhr

      }, {
        key: "prot20h",
        value: function prot20h(sportTypeVal1, sportTypeVal2) {
          var intArray = [];
          intArray[0] = this.startByte;
          intArray[1] = 0x20;
          intArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich
          // 4-5 Sportart

          intArray[3] = sportTypeVal1;
          intArray[4] = sportTypeVal2; // ob Uhrzeit uebertragen wird. Wenn nicht schaltet sich die Anzeige nach Beenden des Spiels ab, ansonsten läuft die Uhr weiter

          if (this.settingsService.hasTimeTransfer) {
            intArray[5] = this.setting20HUhrAktiv.charCodeAt(0); // todo: recheck and make sure that the ASCII of 1 is correct at this point
          } else {
            intArray[5] = this.setting20HUhrNichtAktiv.charCodeAt(0);
          } // Uhrzeit Stunden und Minuten werden übertragen.


          var hourStr = "";
          var minStr = "";
          var d = new Date();
          var h = d.getHours();

          if (h < 10) {
            hourStr = "0" + h;
          } else {
            hourStr = "" + h;
          }

          var m = d.getMinutes();

          if (m < 10) {
            minStr = "0" + m;
          } else {
            minStr = "" + m;
          } // 7-10: Stunden Minuten 10er 1er


          intArray[6] = hourStr.charCodeAt(0);
          intArray[7] = hourStr.charCodeAt(1);
          intArray[8] = minStr.charCodeAt(0);
          intArray[9] = minStr.charCodeAt(1);
          intArray[10] = this.endByte;

          if (this.debugSettings.logPreProtocolContentSettings) {
            console.log("sportTypeVal1:" + sportTypeVal1);
            console.log("sportTypeVal2:" + sportTypeVal2);
            console.log("hourStr:" + hourStr);
            console.log("minStr:" + hourStr);
          }

          var sendArray = this.decodeValues(intArray, 3, 9);
          this.sentProtokoll(sendArray, "prot20h");
        } // Spielzeit und Hupe
        // muss jede Sekunde übertragen werden, egal ob Uhr läuft oder nicht.

      }, {
        key: "prot21h",
        value: function prot21h() {
          this.setting21H.secDiv10 = this.setting21SecDiv10FlagASCII; // Falls aktiviert das signal 5 mal senden.

          if (this.signalSentCounter > 0) {
            this.setting21H.signalEnd = SETTING21_GAME_SIGNAL_ENDE_ON_FLAG_ASCII;
          } else {
            this.setting21H.signalEnd = SETTING21_GAME_SIGNAL_ENDE_Off_FLAG_ASCII;
          }

          if (this.setting21H.min10 == undefined) {
            this.setting21H.min10 = "0";
          }

          if (this.setting21H.min1 == undefined) {
            this.setting21H.min1 = "0";
          }

          if (this.setting21H.sec10 == undefined) {
            this.setting21H.sec10 = "0";
          }

          if (this.setting21H.sec1 == undefined) {
            this.setting21H.sec1 = "0";
          } // Pausiert


          if (this.setting21H.flagMid == undefined) {
            this.setting21H.flagMid = "2";
          }

          if (this.setting21H.signalEnd == undefined) {
            this.setting21H.signalEnd = "0";
          }

          var intArray = [];
          intArray[0] = this.startByte;
          intArray[1] = 0x21;
          intArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich

          intArray[3] = this.setting21H.min10.charCodeAt(0);
          intArray[4] = this.setting21H.min1.charCodeAt(0);
          intArray[5] = this.setting21H.sec10.charCodeAt(0);
          intArray[6] = this.setting21H.sec1.charCodeAt(0);
          intArray[7] = this.setting21H.secDiv10.charCodeAt(0); //20h: zehntel Sekunden werden nicht übertragen.

          intArray[8] = this.setting21H.flagMid.charCodeAt(0); // 31: Spiel läuft 32: Spiel pausiert

          intArray[9] = this.setting21H.signalEnd.charCodeAt(0); // 31: Hupe an     30: Hupe aus

          if (this.setting21H.signalEnd == "1") {
            // hupe test
            debugger;
          }

          if (this.setting21H.flagMid == "1") {
            console.log("Spiel läuft");
          }

          if (this.setting21H.flagMid == "2") {
            console.log("Spiel angehalten");
          }

          intArray[10] = this.endByte;
          var sentArray = this.decodeValues(intArray, 3, 9);

          if (this.debugSettings.logPreProtocolContentSettings) {
            console.log(this.setting21H);
          }

          this.sentProtokoll(sentArray, "prot21h"); // senden des signals wieder herunter zählen.

          if (this.signalSentCounter > 0) {
            this.signalSentCounter--;
          }
        } // Periode
        // hat beim Fußball keine Bedeutung
        // Ansonsten auch nur die Periode relevant
        // Für andere Parameter leere Werte übertragen

      }, {
        key: "prot22h",
        value: function prot22h(setting22H) {
          setting22H.flagLeft = SETTING22_FLAG_LINKS;
          setting22H.flagRight = SETTING22_FLAG_RECHTS;
          setting22H.pfeil = SETTING22_Pfeil;
          var numArray = [];
          numArray[0] = this.startByte;
          numArray[1] = 0x22;
          numArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); //30h-39h möglich

          numArray[3] = setting22H.period.charCodeAt(0);
          numArray[4] = setting22H.pfeil.charCodeAt(0);
          numArray[5] = setting22H.flagLeft.charCodeAt(0);
          numArray[6] = setting22H.flagRight.charCodeAt(0);
          numArray[7] = this.endByte;

          if (this.debugSettings.logPreProtocolContentSettings) {
            console.log(setting22H);
          }

          var sendArray = this.decodeValues(numArray, 3, 6);
          this.sentProtokoll(sendArray, "prot22h");
        } // Spielstand

      }, {
        key: "prot23h",
        value: function prot23h(homeGoals, guestGoals) {
          // als 100er Stelle wird immer eine 0 übertragen
          var strHomeGoals = "";
          var strGuestGoals = "";

          if (homeGoals < 10) {
            strHomeGoals = "00" + homeGoals.toString();
          } else {
            strHomeGoals = "0" + homeGoals.toString();
          }

          if (guestGoals < 10) {
            strGuestGoals = "00" + guestGoals.toString();
          } else {
            strGuestGoals = "0" + guestGoals.toString();
          }

          var numArray = [];
          numArray[0] = this.startByte;
          numArray[1] = 0x23;
          numArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich
          // 4-6 Übertragung Tore Heimmanschaft

          numArray[3] = strHomeGoals.charCodeAt(0);
          numArray[4] = strHomeGoals.charCodeAt(1);
          numArray[5] = strHomeGoals.charCodeAt(2); // 4-6 Übertragung Tore Gastmannschaft
          // 10er, 1er, 100er

          numArray[6] = strGuestGoals.charCodeAt(0);
          numArray[7] = strGuestGoals.charCodeAt(1);
          numArray[8] = strGuestGoals.charCodeAt(2);
          numArray[9] = this.endByte;

          if (this.debugSettings.logPreProtocolContentSettings) {
            console.log("Zahlenwerte von :");
            console.log("strHomeGoals:" + strHomeGoals);
            console.log("strGuestGoals:" + strGuestGoals);
          }

          var sendArray = this.decodeValues(numArray, 3, 8);
          this.sentProtokoll(sendArray, "prot23h");
        } //Mannschaftsnamen

      }, {
        key: "prot29h",
        value: function prot29h(teamNames) {
          // auffüllen beider Mannschaftsnamen mit Leerzeichen am Ende auf insgesamt 8 Zeichen für die Übertragung.
          var addCharsHomeNum = 8 - teamNames.homeTeamName.length;
          var addCharsGuestNum = 8 - teamNames.guestTeamName.length;
          var homeTeamWithFilledChars = teamNames.homeTeamName;

          for (var index = 0; index < addCharsHomeNum; index++) {
            homeTeamWithFilledChars = homeTeamWithFilledChars + " ";
          }

          var guestTeamWithFilledChars = teamNames.guestTeamName;

          for (var _index = 0; _index < addCharsGuestNum; _index++) {
            guestTeamWithFilledChars = guestTeamWithFilledChars + " ";
          }

          var numArray = [];
          numArray[0] = this.startByte;
          numArray[1] = 0x29;
          numArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich
          // Heimmanschaft 8 Zeichen (4-11)

          numArray[3] = homeTeamWithFilledChars.charCodeAt(0);
          numArray[4] = homeTeamWithFilledChars.charCodeAt(1);
          numArray[5] = homeTeamWithFilledChars.charCodeAt(2);
          numArray[6] = homeTeamWithFilledChars.charCodeAt(3);
          numArray[7] = homeTeamWithFilledChars.charCodeAt(4);
          numArray[8] = homeTeamWithFilledChars.charCodeAt(5);
          numArray[9] = homeTeamWithFilledChars.charCodeAt(6);
          numArray[10] = homeTeamWithFilledChars.charCodeAt(7); // Gästemanschaft 8 Zeichen (12-18)

          numArray[11] = guestTeamWithFilledChars.charCodeAt(0);
          numArray[12] = guestTeamWithFilledChars.charCodeAt(1);
          numArray[13] = guestTeamWithFilledChars.charCodeAt(2);
          numArray[14] = guestTeamWithFilledChars.charCodeAt(3);
          numArray[15] = guestTeamWithFilledChars.charCodeAt(4);
          numArray[16] = guestTeamWithFilledChars.charCodeAt(5);
          numArray[17] = guestTeamWithFilledChars.charCodeAt(6);
          numArray[18] = guestTeamWithFilledChars.charCodeAt(7);
          numArray[19] = this.endByte;

          if (this.debugSettings.logPreProtocolContentSettings) {
            console.log("Zahlenwerte von :");
            console.log("homeTeamWithFilledChars:" + homeTeamWithFilledChars);
            console.log("guestTeamWithFilledChars:" + guestTeamWithFilledChars);
          } // Der Databereich wird verschlüsselt.


          var sendArray = this.decodeValues(numArray, 3, 18);
          this.sentProtokoll(sendArray, "prot29h");
        }
      }, {
        key: "sentProtokoll",
        value: function sentProtokoll(sendArray, name) {
          var charArray = [];

          if (this.debugSettings.debugProtokolWorkflow) {
            debugger;
          }

          for (var index = 0; index < sendArray.length; index++) {
            charArray[index] = String.fromCharCode(sendArray[index]);
          }

          var str = "";

          if (this.debugSettings.logProtocolContent) {
            console.log("Send Prot:" + name);
            console.log(charArray);
          }

          for (var _index2 = 0; _index2 < charArray.length; _index2++) {
            str += charArray[_index2];
          } // counter zum decodieren hochzählen


          this.decodeCounter++;

          if (this.decodeCounter == 10) {
            //letzte Element erreicht
            this.decodeCounter = 0;
          }

          if (this.isConnected) {
            this.bluetoothService.sentIfConnected(str);
          }
        }
      }]);

      return ProtocolService;
    }(); // zwei mögliche Einstellungen: Spiel läuft oder läuft nicht.


    ProtocolService.setting21GameIsRunningFlagASCII = "1"; //hex 31

    ProtocolService.setting21GameIsNotRunningFlagASCII = "2"; // hex 32
    // Stuerung der Hupe

    ProtocolService.setting21GameSigEndeOnFlagASCII = "30";

    ProtocolService.ctorParameters = function () {
      return [{
        type: _debug_service__WEBPACK_IMPORTED_MODULE_4__["DebugService"]
      }, {
        type: _bluetooth_service__WEBPACK_IMPORTED_MODULE_5__["BluetoothService"]
      }, {
        type: _settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"]
      }];
    };

    ProtocolService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], ProtocolService);
    /***/
  }
}]);
//# sourceMappingURL=default~pages-period-period-module~pages-play-play-module~pages-settings-advanced-settings-advanced-~341d79b8-es5.js.map