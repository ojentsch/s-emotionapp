(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-bluetooth-bluetooth-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bluetooth/bluetooth.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bluetooth/bluetooth.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>{{ 'CONFIGURATION' | translate }}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <form [formGroup]=\"form\">\r\n<ion-grid>\r\n  \r\n  \r\n  <ion-row >\r\n    <ion-col size=\"12\" class=\"subtitle\"> {{ 'BLUETOOTH' | translate }}</ion-col>\r\n  </ion-row>\r\n\r\n  \r\n \r\n    <ion-row >\r\n      <ion-col size=\"12\">\r\n        <div *ngIf=\"isConnected\">\r\n          {{ 'IS_CONNECT_WITH_SELECTED_BLUETOOTH' | translate }}{{bluetoothService.selectedDeviceName}}\r\n        </div>\r\n        <div *ngIf=\"!isConnected\">\r\n          {{ 'BLUETOOTH_NOT_CONNECTED_MESSAGE' | translate }}         \r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col size=\"8\">\r\n          {{ 'BLUETOOTH_AUTO_CONNECT_OPTION' | translate }}  \r\n    </ion-col>\r\n      <ion-col size=\"4\">\r\n        <ion-toggle formControlName=\"autoConnect\" (ionChange)=\"updateAutoConnect($event)\"></ion-toggle>\r\n    </ion-col>\r\n    </ion-row>\r\n      <div style=\"padding-top: 2em;\" *ngIf=\"hasSelectedDevice()\">\r\n    \t<ion-row >\r\n        <ion-col size=\"8\">\r\n          {{ 'BLUETOOTH_CONNECTION_SAVED' | translate }} {{bluetoothService.selectedDeviceName}}\r\n        </ion-col>\r\n        <ion-col size=\"4\">\r\n          <button (click)='disconnect()' *ngIf=\"isConnected\"  >\r\n            {{ 'BLUETOOTH_STOP_CONNECTION' | translate }}\r\n          </button> \r\n        <button (click)='connect(bluetoothService.selectedDeviceID)'  *ngIf=\"!isConnected\">\r\n          {{ 'BLUETOOTH_START_CONNECTION' | translate }}\r\n        </button> \r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <!-- <ion-row>\r\n      <ion-col size=\"12\"><button (click)='clearBluetoothConnectTimer()'>\r\n        Clear Connect Timer If Exists\r\n      </button>\r\n      </ion-col>\r\n    </ion-row> -->\r\n    \r\n  </div>\r\n \r\n  <div style=\"padding-top: 2em;\">\r\n  <ion-row>\r\n    <ion-col size=\"8\">{{ 'SCAN_BLUETOOTH_DEVICES' | translate }}</ion-col>\r\n    <ion-col size=\"4\">\r\n      <button (click)='scanBluetoothDevices()'>\r\n        <ion-icon name=\"search\" size=\"large\"></ion-icon>\r\n      </button>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n</div>\r\n<ng-template [ngIf]=\"bluetoothScan\">\r\n  <ion-row *ngIf=\"bluetoothDevices.length== 0\">\r\n    <ion-col size=\"12\">{{ 'BLUETOOTH_DEVICE_LIST_EMPTY' | translate }}</ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row *ngIf=\"bluetoothDevices.length > 0\">\r\n    <ion-col size=\"12\">{{ 'BLUETOOTH_DEVICE_LIST_HEADER' | translate }}</ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row *ngFor=\"let device of bluetoothDevices\">\r\n    <ion-col size=\"8\" *ngIf=\"hasDeviceName(device)\">\r\n      {{device.name}}<br>\r\n    </ion-col>\r\n    <ion-col size=\"4\" *ngIf=\"hasDeviceName(device)\">\r\n      <button (click)='connect(device.id)' >\r\n        <ion-icon name=\"bluetooth\" size=\"large\"></ion-icon>\r\n      </button>  \r\n    </ion-col>\r\n  </ion-row>\r\n</ng-template>\r\n  </ion-grid>\r\n\r\n\r\n  <ion-row >\r\n    <ion-col size=\"12\" class=\"subtitle\"> {{ 'TIME' | translate }}</ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row >\r\n    <ion-col size=\"8\">{{ 'CHECK_TIME_TRANSFER' | translate }}</ion-col>\r\n    <ion-col size=\"2\">\r\n      <div class=\"ion-text-left\" style=\"margin-left: 13px;\">\r\n        <ion-checkbox class=\"checkbox\" (ionChange)=\"onChangeTimeTransfer($event)\" formControlName=\"checkTimeTransfer\" ></ion-checkbox>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  \r\n  <!-- <pre >\r\n    {{IsTimeTransfer}}\r\n  </pre>\r\n  <pre >\r\n    {{checkForm}}\r\n  </pre> -->\r\n  <!-- <pre >\r\n    {{devices | json}}\r\n  </pre> -->\r\n</form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/bluetooth/bluetooth-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/bluetooth/bluetooth-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: BluetoothPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BluetoothPageRoutingModule", function() { return BluetoothPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _bluetooth_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bluetooth.page */ "./src/app/pages/bluetooth/bluetooth.page.ts");




const routes = [
    {
        path: '',
        component: _bluetooth_page__WEBPACK_IMPORTED_MODULE_3__["BluetoothPage"]
    }
];
let BluetoothPageRoutingModule = class BluetoothPageRoutingModule {
};
BluetoothPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BluetoothPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/bluetooth/bluetooth.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/bluetooth/bluetooth.module.ts ***!
  \*****************************************************/
/*! exports provided: BluetoothPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BluetoothPageModule", function() { return BluetoothPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _bluetooth_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bluetooth-routing.module */ "./src/app/pages/bluetooth/bluetooth-routing.module.ts");
/* harmony import */ var _bluetooth_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bluetooth.page */ "./src/app/pages/bluetooth/bluetooth.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let BluetoothPageModule = class BluetoothPageModule {
};
BluetoothPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _bluetooth_routing_module__WEBPACK_IMPORTED_MODULE_5__["BluetoothPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]
        ],
        declarations: [_bluetooth_page__WEBPACK_IMPORTED_MODULE_6__["BluetoothPage"]]
    })
], BluetoothPageModule);



/***/ }),

/***/ "./src/app/pages/bluetooth/bluetooth.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/bluetooth/bluetooth.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JsdWV0b290aC9ibHVldG9vdGgucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/bluetooth/bluetooth.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/bluetooth/bluetooth.page.ts ***!
  \***************************************************/
/*! exports provided: BluetoothPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BluetoothPage", function() { return BluetoothPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_shared_service_bluetooth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/service/bluetooth.service */ "./src/app/shared/service/bluetooth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/global-function */ "./src/app/shared/global-function.ts");
/* harmony import */ var src_app_shared_service_data_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/service/data-storage.service */ "./src/app/shared/service/data-storage.service.ts");
/* harmony import */ var src_app_shared_service_settings_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/service/settings.service */ "./src/app/shared/service/settings.service.ts");







let BluetoothPage = class BluetoothPage {
    // the configuration service must be loaded first.
    constructor(bluetoothService, formBuilder, dataStorageService, settingsService) {
        this.bluetoothService = bluetoothService;
        this.formBuilder = formBuilder;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
        this.bluetoothDevices = [];
        //public IsTimeTransfer = true;
        this.bluetoothScan = false;
        this.checkForm = "";
        this.form = this.formBuilder.group({
            testSend: "",
            autoConnect: false,
            checkTimeTransfer: false,
            checkDummy: false
        });
        this.isConnected = false;
    }
    ngOnInit() {
        this.checkForm = this.checkForm + " ngOnInit";
        this.bluetoothScan = false;
        this.subscription = this.bluetoothService.ConnectionChanges.subscribe(connected => {
            this.isConnected = connected;
        });
        this.bluetoothDevices = this.bluetoothService.devices;
        this.dataStorageService.getString('BluetoothAutoConnect').then((data) => {
            if ((!src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                this.form.controls['autoConnect'].setValue(JSON.parse(data.value));
            }
        });
        this.subscriptionBluetooth = this.bluetoothService.BluetoothDevicesChanges.subscribe(devices => {
            this.bluetoothDevices = devices;
        });
    }
    ionViewWillEnter() {
        // check status of connection when entering the page.
        this.isConnected = this.bluetoothService.IsConnected;
        // nur beim ersten mal
        this.form.controls['checkTimeTransfer'].setValue(this.settingsService.hasTimeTransfer);
        // this.dataStorageService.getString('HasTimeTransfer').then((data: any) => {
        //   if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
        //     this.form.controls['checkTimeTransfer'].setValue(JSON.parse(data.value));
        //     this.IsTimeTransfer =  JSON.parse(data.value)
        //     this.checkForm = this.checkForm + " checkTimeTransfer init";
        //    }
        // });
        // get value of transfering time from session if exists 
        // this.dataStorageService.getString('HasTimeTransfer').then((data: any) => {
        //   if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
        //     this.TimeTransferActive =  JSON.parse(data.value)
        //     this.form.controls['checkTimeTransfer'].setValue(JSON.parse(data.value));
        //     this.checkForm = this.checkForm + " checkTimeTransfer from session";
        //   } else { // default value is true
        //     this.form.controls['checkTimeTransfer'].setValue(true);
        //     this.TimeTransferActive =  true;
        //     this.checkForm = this.checkForm + " checkTimeTransfer init";
        //   }
        // });
    }
    updateAutoConnect(e) {
        this.bluetoothService.setBlueToothAutoConnect(e.detail.checked);
    }
    ngOnDestroy() {
        //this.subscription.unsubscribe();
        this.subscriptionBluetooth.unsubscribe();
    }
    scanBluetoothDevices() {
        this.bluetoothScan = true;
        this.bluetoothService.scan();
    }
    disconnect() {
        this.bluetoothService.disconnect();
    }
    clearBluetoothConnectTimer() {
        this.bluetoothService.clearConnectionIntervalTimer();
    }
    onChangeTimeTransfer(e) {
        //this.IsTimeTransfer = e.detail.checked;
        this.checkForm = this.checkForm + " new value of HasTimeTransfer " + e.detail.checked;
        this.settingsService.updateTimeTransfer(e.detail.checked);
    }
    checkIsConnected() {
        return this.bluetoothService.IsConnected;
    }
    hasSelectedDevice() {
        if (!src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(this.bluetoothService.selectedDeviceID)) {
            return true;
        }
        return false;
    }
    hasDeviceName(device) {
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(device.name)) {
            return false;
        }
        return (true);
    }
    connect(deviceId) {
        this.bluetoothService.connect(deviceId);
    }
};
BluetoothPage.ctorParameters = () => [
    { type: src_app_shared_service_bluetooth_service__WEBPACK_IMPORTED_MODULE_2__["BluetoothService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: src_app_shared_service_data_storage_service__WEBPACK_IMPORTED_MODULE_5__["DataStorageService"] },
    { type: src_app_shared_service_settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"] }
];
BluetoothPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-bluetooth',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./bluetooth.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bluetooth/bluetooth.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./bluetooth.page.scss */ "./src/app/pages/bluetooth/bluetooth.page.scss")).default]
    })
], BluetoothPage);



/***/ })

}]);
//# sourceMappingURL=pages-bluetooth-bluetooth-module-es2015.js.map