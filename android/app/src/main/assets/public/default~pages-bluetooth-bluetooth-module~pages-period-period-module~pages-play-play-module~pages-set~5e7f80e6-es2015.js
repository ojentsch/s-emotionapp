(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-bluetooth-bluetooth-module~pages-period-period-module~pages-play-play-module~pages-set~5e7f80e6"],{

/***/ "./src/app/shared/global-function.ts":
/*!*******************************************!*\
  !*** ./src/app/shared/global-function.ts ***!
  \*******************************************/
/*! exports provided: GlobalFunction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalFunction", function() { return GlobalFunction; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _model_general_settings__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./model/general-settings */ "./src/app/shared/model/general-settings.ts");


class GlobalFunction {
    static isEmpty(val) {
        return (val === undefined || val == null || val.length <= 0) ? true : false;
    }
    //Soll eine XOR Verknüpfung des HexArrays mit Zeichen.
    static xor(val1, val2) {
        // todo: xor val1 and val2
        return val1 ^ val2;
    }
    static stringToBytes(str) {
        var array = new Uint8Array(str.length);
        for (let i = 0, l = str.length; i < l; i++) {
            array[i] = str.charCodeAt(i);
        }
        return array;
    }
    // ursprungsfunktion
    static stringToBytesBuffer(str) {
        var array = new Uint8Array(str.length);
        for (let i = 0, l = str.length; i < l; i++) {
            array[i] = str.charCodeAt(i);
        }
        return array.buffer;
    }
    static wait(ms) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            var start = new Date().getTime();
            var end = start;
            while (end < start + ms) {
                end = new Date().getTime();
            }
        });
    }
    static charFromASCII(numArray) {
        let strArr = [];
        for (let index = 0; index < numArray.length; index++) {
            strArr[index] = String.fromCharCode(index);
        }
        return (strArr);
    }
    static setSettings21HFromCurrentGame(currentGame, setting21H) {
        if (currentGame.currentSec > 9) {
            let str = currentGame.currentSec.toString();
            setting21H.sec10 = str.charAt(0);
            setting21H.sec1 = str.charAt(1);
        }
        else {
            setting21H.sec10 = "0";
            setting21H.sec1 = currentGame.currentSec.toString();
        }
        // todo: modify the minutes with period in
        let modifiedMin = 0;
        if (currentGame.sumPeriods == _model_general_settings__WEBPACK_IMPORTED_MODULE_1__["SumPeriods"].Yes) {
            //modifiedMin = currentGame.currentMinutes;
            if (currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_1__["TimePeriod"].normal) { //normale Spielzeit
                if (currentGame.period > 1) {
                    modifiedMin = ((currentGame.period - 1) * (currentGame.regularTimeTargetMinutes)) + currentGame.currentMinutes;
                }
                else {
                    modifiedMin = currentGame.currentMinutes;
                }
            }
            else { // Verlängerung
                modifiedMin = ((currentGame.period - 1 - currentGame.regularTimeNumPeriods) * currentGame.extraTimeTargetMinutes) + currentGame.currentMinutes;
            }
        }
        else {
            modifiedMin = currentGame.currentMinutes;
        }
        if (modifiedMin > 9) {
            let str = modifiedMin.toString();
            setting21H.min10 = str.charAt(0);
            setting21H.min1 = str.charAt(1);
        }
        else {
            setting21H.min10 = "0";
            setting21H.min1 = modifiedMin.toString();
        }
        return setting21H;
    }
}


/***/ }),

/***/ "./src/app/shared/model/general-settings.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/model/general-settings.ts ***!
  \**************************************************/
/*! exports provided: ConfigSettings, DebugSettings, TimerDirection, GameType, SumPeriods, ExtraTimeAvailable, GameStatus, TimePeriod, TeamNames, Setting21H, Setting22H, DecodeValues, CurrentGame, GeneralSettings, PeriodSettings, ExtraTimeSettings */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigSettings", function() { return ConfigSettings; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DebugSettings", function() { return DebugSettings; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimerDirection", function() { return TimerDirection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameType", function() { return GameType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SumPeriods", function() { return SumPeriods; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtraTimeAvailable", function() { return ExtraTimeAvailable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameStatus", function() { return GameStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimePeriod", function() { return TimePeriod; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamNames", function() { return TeamNames; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Setting21H", function() { return Setting21H; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Setting22H", function() { return Setting22H; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DecodeValues", function() { return DecodeValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentGame", function() { return CurrentGame; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeneralSettings", function() { return GeneralSettings; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeriodSettings", function() { return PeriodSettings; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtraTimeSettings", function() { return ExtraTimeSettings; });
class ConfigSettings {
}
class DebugSettings {
    constructor() {
        this.debugProtokolWorkflow = false;
        this.debugProtokolSent = true;
        this.decodeProtocol = true; // todo: must be set to true at the end.
        this.logProtocolTimerSettings = true;
        this.logAdditionalInformation = true;
        this.logProtocolContent = true;
        this.logPreProtocolContentSettings = true;
        this.deactiveSentProtocolTimer = false; //todo: must be set to false at the end.
        this.displayPreOnHtml = false;
        this.sendWithBluetooth = false;
        this.displayJson = false;
    }
}
var TimerDirection;
(function (TimerDirection) {
    TimerDirection[TimerDirection["Forward"] = 1] = "Forward";
    TimerDirection[TimerDirection["Backward"] = 2] = "Backward";
})(TimerDirection || (TimerDirection = {}));
var GameType;
(function (GameType) {
    GameType[GameType["Football"] = 1] = "Football";
    GameType[GameType["Hockey"] = 2] = "Hockey";
})(GameType || (GameType = {}));
var SumPeriods;
(function (SumPeriods) {
    SumPeriods[SumPeriods["Yes"] = 1] = "Yes";
    SumPeriods[SumPeriods["No"] = 2] = "No";
})(SumPeriods || (SumPeriods = {}));
var ExtraTimeAvailable;
(function (ExtraTimeAvailable) {
    ExtraTimeAvailable[ExtraTimeAvailable["Yes"] = 1] = "Yes";
    ExtraTimeAvailable[ExtraTimeAvailable["No"] = 2] = "No";
})(ExtraTimeAvailable || (ExtraTimeAvailable = {}));
var GameStatus;
(function (GameStatus) {
    GameStatus[GameStatus["init"] = 1] = "init";
    GameStatus[GameStatus["play"] = 2] = "play";
    GameStatus[GameStatus["pause"] = 3] = "pause";
    GameStatus[GameStatus["periodFinished"] = 4] = "periodFinished";
    GameStatus[GameStatus["gameFinished"] = 5] = "gameFinished";
})(GameStatus || (GameStatus = {}));
var TimePeriod;
(function (TimePeriod) {
    TimePeriod[TimePeriod["normal"] = 1] = "normal";
    TimePeriod[TimePeriod["renewal"] = 2] = "renewal";
    TimePeriod[TimePeriod["Backward"] = 3] = "Backward";
})(TimePeriod || (TimePeriod = {}));
class TeamNames {
    constructor() {
        this.homeTeamName = "";
        this.guestTeamName = "";
    }
    ;
}
class Setting21H {
}
//todo: mit richtigen standardwerten definieren
class Setting22H {
    constructor() {
        this.pfeil = " ";
        this.flagLeft = " ";
        this.flagRight = " ";
    }
}
// todo: muss in zahlen umbewendelt werden
// werden nur die HexWerte in string geschrieben
class DecodeValues {
}
class CurrentGame {
    constructor() {
        this.period = 0;
        this.homeGoals = 0;
        this.guestGoals = 0;
        this.currentMinutes = 0;
        this.currentSec = 0;
        this.totalSec = 0;
        this.timePeriod = TimePeriod.normal;
        this.extraTimeAvailable = ExtraTimeAvailable.No;
    }
}
class GeneralSettings {
}
class PeriodSettings {
}
class ExtraTimeSettings {
}


/***/ }),

/***/ "./src/app/shared/service/bluetooth.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/service/bluetooth.service.ts ***!
  \*****************************************************/
/*! exports provided: BluetoothService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BluetoothService", function() { return BluetoothService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_ble_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/ble/ngx */ "./node_modules/@ionic-native/ble/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _message_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./message.service */ "./src/app/shared/service/message.service.ts");
/* harmony import */ var _global_function__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../global-function */ "./src/app/shared/global-function.ts");
/* harmony import */ var _data_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./data-storage.service */ "./src/app/shared/service/data-storage.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _configuration_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./configuration.service */ "./src/app/shared/service/configuration.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");



// es gibt zwei verschiedene BLEs

//import { BLE } from '@ionic-native/ble';







// ServiceIDs für AVR-BLE_4DDA und RN4871-EBAD
const serviceUUID = '49535343-fe7d-4ae5-8fa9-9fafd205e455';
// dev-kid für AVR-BLE_4DDA und RN4871-EBAD
const characteristicUUID = '49535343-1e4d-4bd9-ba61-23c647249616';
let BluetoothService = class BluetoothService {
    constructor(configurationService, ble, ngZone, messageService, dataStorageService, translate, platform) {
        this.configurationService = configurationService;
        this.ble = ble;
        this.ngZone = ngZone;
        this.messageService = messageService;
        this.dataStorageService = dataStorageService;
        this.translate = translate;
        this.platform = platform;
        this.devices = [];
        this.configSettingsbluetoothDeviceName = "";
        this.peripheral = {};
        this.sendArray = new Uint8Array();
        this.BluetoothDevicesChanges = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
        this.ConnectionChanges = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
        this.intervalTimer = 3000;
        this.IsConnected = false;
        this.selectedDeviceID = "";
        // handle the device name
        this.subscription = this.configurationService.ConfigLoaded.subscribe(data => {
            // First load from config
            this.configSettings = data;
            if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(this.configSettings.bluetoothDeviceName)) {
                this.configSettingsbluetoothDeviceName = this.configSettings.bluetoothDeviceName;
                this.selectedDeviceName = this.configSettings.bluetoothDeviceName;
            }
            this.dataStorageService.getString('deviceName').then((data) => {
                if ((!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                    this.selectedDeviceName = data.value;
                }
            });
        });
        this.dataStorageService.getString('BluetoothAutoConnect').then((data) => {
            if ((!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                this.BluetoothAutoConnect = JSON.parse(data.value);
            }
        });
        // use session connection if there is something else from the last connection.
        this.dataStorageService.getString('deviceId').then((data) => {
            if ((!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                this.selectedDeviceID = data.value;
                this.dataStorageService.getString('BluetoothAutoConnect').then((data) => {
                    if ((!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                        let autoconnect = JSON.parse(data.value);
                        if (autoconnect) {
                            // connect to device if already stored.
                            this.connect(this.selectedDeviceID);
                        }
                    }
                });
            }
        });
        this.resSubscription = this.translate.get(['CONNECTION_CLOSED_MESSAGE', 'BLUETOOTH_CONNECTION_ESTABLISHED', 'BLUETOOTH_TRY_ESTABLISH_CONNECTION', 'BLUETOOTH_SCAN_ON_DEVICE_NOT_POSSIBLE', 'BLUETOOTH_DISPLAY_SEND_STR_DEBUG', 'BLUETOOTH_UNEXPECTED_DISCONNECTED']).subscribe((translation) => {
            this.conClosedMes = translation['CONNECTION_CLOSED_MESSAGE'];
            this.conEstablishedMes = translation['BLUETOOTH_CONNECTION_ESTABLISHED'];
            this.conTryEstablishConMes = translation['BLUETOOTH_TRY_ESTABLISH_CONNECTION'];
            this.scanOnDeviceNotPossibleMes = translation['BLUETOOTH_SCAN_ON_DEVICE_NOT_POSSIBLE'];
            this.sendStrDebugMes = translation['BLUETOOTH_DISPLAY_SEND_STR_DEBUG'];
            this.bluetoothDisconnectedMes = translation['BLUETOOTH_UNEXPECTED_DISCONNECTED'];
        });
    }
    changeBluetoothHandler() {
        this.BluetoothDevicesChanges.next(this.devices);
    }
    changeHandler() {
        this.ConnectionChanges.next(this.IsConnected);
        if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(this.selectedDeviceID)) {
            if (this.IsConnected) {
                this.clearConnectionIntervalTimer();
            }
        }
    }
    setBlueToothAutoConnect(autoConnect) {
        let strAutoConnect = autoConnect ? "true" : "false";
        this.BluetoothAutoConnect = autoConnect;
        this.dataStorageService.setString('BluetoothAutoConnect', strAutoConnect);
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.resSubscription.unsubscribe();
    }
    setConnectionIntervalTimer() {
        this.clearConnectionIntervalTimer();
        this.interval = setInterval(() => {
            this.connect(this.selectedDeviceID);
        }, this.intervalTimer);
    }
    clearConnectionIntervalTimer() {
        clearInterval(this.interval);
    }
    scan() {
        this.devices = []; // clear list
        this.ble.scan([], 30).subscribe(device => this.onDeviceDiscovered(device), error => this.scanError(error));
        //setTimeout(this.setStatus.bind(this), 5000, 'Scan complete');
    }
    disconnect() {
        this.ble.disconnect(this.selectedDeviceID).then(() => this.messageService.presentToasterMessage(this.conClosedMes), e => this.messageService.presentToasterMessage('Fehler: Verbindung konnte nicht beendet werden.'));
        this.IsConnected = false;
        this.changeHandler();
    }
    onDeviceDiscovered(device) {
        console.log('Gefunden:' + JSON.stringify(device, null, 2));
        this.ngZone.run(() => {
            this.devices.push(device);
            // todo: not clear if requred 
            this.changeBluetoothHandler();
        });
    }
    scanError(error) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.setStatus('Error ' + error);
            this.messageService.presentToasterMessage(this.scanOnDeviceNotPossibleMes);
        });
    }
    setStatus(message) {
        console.log(message);
        this.ngZone.run(() => {
            this.statusMessage = message;
        });
    }
    deviceSelected(device) {
        //console.log(JSON.stringify(device) + ' selected');
    }
    connect(deviceId) {
        if (this.conTryEstablishConMes !== "") {
            this.messageService.presentToasterMessage(this.conTryEstablishConMes);
        }
        // This is not a promise, the device can call disconnect after it connects, so it's an observable
        this.ble.connect(deviceId).subscribe(peripheral => this.connectCallback(peripheral), () => this.disconnectCallback());
    }
    // no error handling here as we just want to sent when there is a connection.
    sent(str) {
        //this.messageService.presentToasterMessage('Send if connected:' + str);
        this.sentMessage(str);
    }
    sentIfConnected(str) {
        if (this.IsConnected) {
            //this.messageService.presentToasterMessage('Send if connected:' + str);
            this.ble.isConnected(this.selectedDeviceID).then((e) => this.sentMessage(str));
        }
    }
    sentMessage(str) {
        let byteArray = _global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].stringToBytes(str);
        this.ble.write(this.selectedDeviceID, serviceUUID, characteristicUUID, byteArray.buffer).then((e) => this.bleWriteCallback(str), e => this.bleWriteErrorCallback(str));
    }
    bleWriteCallback(str) {
        if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(this.sendStrDebugMes)) {
            this.messageService.presentToasterMessage(this.sendStrDebugMes + str);
        }
    }
    bleWriteErrorCallback(str) {
        if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(this.sendStrDebugMes)) {
            this.messageService.presentToasterMessage("Fehler: " + str + " wurde nicht übertragen.");
        }
    }
    // Verbindung wird aufgebaut.
    connectCallback(peripheral) {
        this.selectedDeviceID = peripheral.id;
        this.dataStorageService.setString('deviceId', peripheral.id);
        this.peripheral = peripheral;
        // Devicename festlegen.
        if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(peripheral.Name)) {
            this.dataStorageService.setString('deviceName', peripheral.Name);
            this.selectedDeviceName = peripheral.Name;
        }
        else {
            let device = this.devices.find(x => x.id == peripheral.id);
            if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(device)) {
                if ((!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(device.name)) && (device.name !== undefined)) {
                    this.dataStorageService.setString('deviceName', device.name);
                    this.selectedDeviceName = device.name;
                }
                else if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(device.advertising)) {
                    this.dataStorageService.setString('deviceName', device.advertising);
                    this.selectedDeviceName = device.advertising;
                }
            }
        }
        // Verbindungsnachricht
        if (this.conEstablishedMes != "") {
            this.messageService.presentToasterMessage(this.conEstablishedMes + this.selectedDeviceName);
        }
        this.IsConnected = true;
        this.changeHandler();
        // Timer zum Verbingunsaufbau löschen.
        this.clearConnectionIntervalTimer();
    }
    // Die Verbindung kann aus verschiedenen Gründen unterbrochen werden.
    // Entfernung zu weit
    // Verbindung kann erst gar nicht aufgebaut werden.
    disconnectCallback() {
        this.messageService.presentToasterMessage(this.bluetoothDisconnectedMes);
        this.IsConnected = false;
        this.changeHandler();
        // sobald die Verbindung nicht aufgebaut werden kann startet ein Timer der versucht die Verbindung herzustellen
        this.setConnectionIntervalTimer();
    }
};
BluetoothService.ctorParameters = () => [
    { type: _configuration_service__WEBPACK_IMPORTED_MODULE_7__["ConfigurationService"] },
    { type: _ionic_native_ble_ngx__WEBPACK_IMPORTED_MODULE_2__["BLE"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _message_service__WEBPACK_IMPORTED_MODULE_3__["MessageService"] },
    { type: _data_storage_service__WEBPACK_IMPORTED_MODULE_5__["DataStorageService"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["Platform"] }
];
BluetoothService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], BluetoothService);



/***/ }),

/***/ "./src/app/shared/service/configuration.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/shared/service/configuration.service.ts ***!
  \*********************************************************/
/*! exports provided: ConfigurationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigurationService", function() { return ConfigurationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");



let ConfigurationService = class ConfigurationService {
    constructor() {
        this.ConfigLoaded = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.getSettings();
    }
    changeHandler() {
        this.ConfigLoaded.next(this.configSettings);
    }
    getSettings() {
        fetch('./assets/data/configSettings.json').then(res => res.json())
            .then(json => {
            //debugger;
            this.configSettings = json;
            this.changeHandler();
        });
    }
};
ConfigurationService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ConfigurationService);



/***/ }),

/***/ "./src/app/shared/service/data-storage.service.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/service/data-storage.service.ts ***!
  \********************************************************/
/*! exports provided: DataStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataStorageService", function() { return DataStorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");



const { Storage } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"];
let DataStorageService = class DataStorageService {
    constructor() { }
    setString(key, value) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield Storage.set({ key, value });
        });
    }
    getString(key) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return (yield Storage.get({ key }));
        });
    }
    setObject(key, value) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield Storage.set({ key, value: JSON.stringify(value) });
        });
    }
    getObject(key) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const ret = yield Storage.get({ key });
            return JSON.parse(ret.value);
        });
    }
    removeItem(key) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield Storage.remove({ key });
        });
    }
    clear() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield Storage.clear();
        });
    }
};
DataStorageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], DataStorageService);



/***/ }),

/***/ "./src/app/shared/service/message.service.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/service/message.service.ts ***!
  \***************************************************/
/*! exports provided: MessageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageService", function() { return MessageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let MessageService = class MessageService {
    constructor(toastCtrl) {
        this.toastCtrl = toastCtrl;
        this.duration = 3000;
        // top, bottom and middle
        this.position = "bottom";
    }
    presentToasterPostionMessage(str, position) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.position = position;
            this.presentToasterMessage(str);
        });
    }
    presentToasterMiddleMessage(str) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: str,
                duration: this.duration,
                position: "middle"
            });
            toast.present();
        });
    }
    presentToasterMessage(str) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: str,
                duration: this.duration,
            });
            toast.present();
        });
    }
};
MessageService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
MessageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], MessageService);



/***/ }),

/***/ "./src/app/shared/service/settings.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/service/settings.service.ts ***!
  \****************************************************/
/*! exports provided: SettingsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsService", function() { return SettingsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _model_general_settings__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/general-settings */ "./src/app/shared/model/general-settings.ts");
/* harmony import */ var _data_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./data-storage.service */ "./src/app/shared/service/data-storage.service.ts");
/* harmony import */ var _global_function__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../global-function */ "./src/app/shared/global-function.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");







let SettingsService = class SettingsService {
    constructor(dataStorageService, translate) {
        this.dataStorageService = dataStorageService;
        this.translate = translate;
        //languages: unknown;
        this.teamNames = new _model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TeamNames"];
        this.hasTimeTransfer = true;
        this.generalSettings = [];
        this.getGeneralSettings();
        this.subscription = this.translate.get(['TEAM1_DEFAULT_NAME', 'TEAM2_DEFAULT_NAME']).subscribe((translation) => {
            this.teamNames.homeTeamName = translation['TEAM1_DEFAULT_NAME'];
            this.teamNames.guestTeamName = translation['TEAM2_DEFAULT_NAME'];
        });
        // get values from storage
        this.dataStorageService.getString('HomeTeamName').then((data) => {
            if ((!_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                this.teamNames.homeTeamName = data.value;
            }
        });
        this.dataStorageService.getString('GuestTeamName').then((data) => {
            if ((!_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                this.teamNames.guestTeamName = data.value;
            }
        });
        this.dataStorageService.getString('HasTimeTransfer').then((data) => {
            if ((!_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                this.hasTimeTransfer = JSON.parse(data.value);
            }
        });
    }
    getGeneralSettings$() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.generalSettings);
    }
    getGeneralSettings() {
        fetch('./assets/data/generalSettings.json').then(res => res.json())
            .then(json => {
            this.generalSettings = json;
        });
    }
    updateTeamNames(teamNames) {
        this.teamNames.guestTeamName = teamNames.guestTeamName;
        this.teamNames.homeTeamName = teamNames.homeTeamName;
        this.dataStorageService.setString('HomeTeamName', teamNames.homeTeamName);
        this.dataStorageService.setString('GuestTeamName', teamNames.guestTeamName);
    }
    updateTimeTransfer(transferTime) {
        this.hasTimeTransfer = transferTime;
        this.dataStorageService.setString('HasTimeTransfer', String(transferTime));
    }
};
SettingsService.ctorParameters = () => [
    { type: _data_storage_service__WEBPACK_IMPORTED_MODULE_4__["DataStorageService"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"] }
];
SettingsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], SettingsService);



/***/ })

}]);
//# sourceMappingURL=default~pages-bluetooth-bluetooth-module~pages-period-period-module~pages-play-play-module~pages-set~5e7f80e6-es2015.js.map