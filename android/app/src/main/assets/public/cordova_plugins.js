
  cordova.define('cordova/plugin_list', function(require, exports, module) {
    module.exports = [
      {
          "id": "cordova-plugin-ble-central.ble",
          "file": "plugins/cordova-plugin-ble-central/www/ble.js",
          "pluginId": "cordova-plugin-ble-central",
        "clobbers": [
          "ble"
        ]
        },
      {
          "id": "uk.co.workingedge.cordova.plugin.sqliteporter.sqlitePorter",
          "file": "plugins/uk.co.workingedge.cordova.plugin.sqliteporter/www/sqlitePorter.js",
          "pluginId": "uk.co.workingedge.cordova.plugin.sqliteporter",
        "clobbers": [
          "cordova.plugins.sqlitePorter"
        ]
        },
      {
          "id": "cordova-plugin-device.device",
          "file": "plugins/cordova-plugin-device/www/device.js",
          "pluginId": "cordova-plugin-device",
        "clobbers": [
          "device"
        ]
        },
      {
          "id": "cordova-plugin-geolocation.geolocation",
          "file": "plugins/cordova-plugin-geolocation/www/android/geolocation.js",
          "pluginId": "cordova-plugin-geolocation",
        "clobbers": [
          "navigator.geolocation"
        ]
        },
      {
          "id": "cordova-plugin-globalization.globalization",
          "file": "plugins/cordova-plugin-globalization/www/globalization.js",
          "pluginId": "cordova-plugin-globalization",
        "clobbers": [
          "navigator.globalization"
        ]
        },
      {
          "id": "cordova-plugin-background-mode.BackgroundMode",
          "file": "plugins/cordova-plugin-background-mode/www/background-mode.js",
          "pluginId": "cordova-plugin-background-mode",
        "clobbers": [
          "cordova.plugins.backgroundMode",
          "plugin.backgroundMode"
        ]
        },
      {
          "id": "cordova-plugin-globalization.GlobalizationError",
          "file": "plugins/cordova-plugin-globalization/www/GlobalizationError.js",
          "pluginId": "cordova-plugin-globalization",
        "clobbers": [
          "window.GlobalizationError"
        ]
        },
      {
          "id": "at.gofg.sportscomputer.powermanagement.device",
          "file": "plugins/at.gofg.sportscomputer.powermanagement/www/powermanagement.js",
          "pluginId": "at.gofg.sportscomputer.powermanagement",
        "clobbers": [
          "window.powerManagement"
        ]
        },
      {
          "id": "cordova-plugin-statusbar.statusbar",
          "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
          "pluginId": "cordova-plugin-statusbar",
        "clobbers": [
          "window.StatusBar"
        ]
        },
      {
          "id": "cordova-plugin-geolocation.PositionError",
          "file": "plugins/cordova-plugin-geolocation/www/PositionError.js",
          "pluginId": "cordova-plugin-geolocation",
        "runs": true
        }
    ];
    module.exports.metadata =
    // TOP OF METADATA
    {
      "cordova-plugin-background-mode": "0.7.3",
      "cordova-plugin-ble-central": "1.2.5",
      "cordova-plugin-device": "2.0.3",
      "cordova-plugin-geolocation": "4.0.2",
      "cordova-plugin-globalization": "1.11.0",
      "at.gofg.sportscomputer.powermanagement": "1.1.2",
      "uk.co.workingedge.cordova.plugin.sqliteporter": "1.1.1",
      "cordova-plugin-statusbar": "2.4.2",
      "cordova-plugin-whitelist": "1.3.3"
    };
    // BOTTOM OF METADATA
    });
    