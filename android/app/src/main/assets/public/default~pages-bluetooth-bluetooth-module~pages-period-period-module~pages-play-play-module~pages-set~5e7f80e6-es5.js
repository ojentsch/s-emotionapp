function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-bluetooth-bluetooth-module~pages-period-period-module~pages-play-play-module~pages-set~5e7f80e6"], {
  /***/
  "./src/app/shared/global-function.ts":
  /*!*******************************************!*\
    !*** ./src/app/shared/global-function.ts ***!
    \*******************************************/

  /*! exports provided: GlobalFunction */

  /***/
  function srcAppSharedGlobalFunctionTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GlobalFunction", function () {
      return GlobalFunction;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _model_general_settings__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./model/general-settings */
    "./src/app/shared/model/general-settings.ts");

    var GlobalFunction = /*#__PURE__*/function () {
      function GlobalFunction() {
        _classCallCheck(this, GlobalFunction);
      }

      _createClass(GlobalFunction, null, [{
        key: "isEmpty",
        value: function isEmpty(val) {
          return val === undefined || val == null || val.length <= 0 ? true : false;
        } //Soll eine XOR Verknüpfung des HexArrays mit Zeichen.

      }, {
        key: "xor",
        value: function xor(val1, val2) {
          // todo: xor val1 and val2
          return val1 ^ val2;
        }
      }, {
        key: "stringToBytes",
        value: function stringToBytes(str) {
          var array = new Uint8Array(str.length);

          for (var i = 0, l = str.length; i < l; i++) {
            array[i] = str.charCodeAt(i);
          }

          return array;
        } // ursprungsfunktion

      }, {
        key: "stringToBytesBuffer",
        value: function stringToBytesBuffer(str) {
          var array = new Uint8Array(str.length);

          for (var i = 0, l = str.length; i < l; i++) {
            array[i] = str.charCodeAt(i);
          }

          return array.buffer;
        }
      }, {
        key: "wait",
        value: function wait(ms) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var start, end;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    start = new Date().getTime();
                    end = start;

                    while (end < start + ms) {
                      end = new Date().getTime();
                    }

                  case 3:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee);
          }));
        }
      }, {
        key: "charFromASCII",
        value: function charFromASCII(numArray) {
          var strArr = [];

          for (var index = 0; index < numArray.length; index++) {
            strArr[index] = String.fromCharCode(index);
          }

          return strArr;
        }
      }, {
        key: "setSettings21HFromCurrentGame",
        value: function setSettings21HFromCurrentGame(currentGame, setting21H) {
          if (currentGame.currentSec > 9) {
            var str = currentGame.currentSec.toString();
            setting21H.sec10 = str.charAt(0);
            setting21H.sec1 = str.charAt(1);
          } else {
            setting21H.sec10 = "0";
            setting21H.sec1 = currentGame.currentSec.toString();
          } // todo: modify the minutes with period in


          var modifiedMin = 0;

          if (currentGame.sumPeriods == _model_general_settings__WEBPACK_IMPORTED_MODULE_1__["SumPeriods"].Yes) {
            //modifiedMin = currentGame.currentMinutes;
            if (currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_1__["TimePeriod"].normal) {
              //normale Spielzeit
              if (currentGame.period > 1) {
                modifiedMin = (currentGame.period - 1) * currentGame.regularTimeTargetMinutes + currentGame.currentMinutes;
              } else {
                modifiedMin = currentGame.currentMinutes;
              }
            } else {
              // Verlängerung
              modifiedMin = (currentGame.period - 1 - currentGame.regularTimeNumPeriods) * currentGame.extraTimeTargetMinutes + currentGame.currentMinutes;
            }
          } else {
            modifiedMin = currentGame.currentMinutes;
          }

          if (modifiedMin > 9) {
            var _str = modifiedMin.toString();

            setting21H.min10 = _str.charAt(0);
            setting21H.min1 = _str.charAt(1);
          } else {
            setting21H.min10 = "0";
            setting21H.min1 = modifiedMin.toString();
          }

          return setting21H;
        }
      }]);

      return GlobalFunction;
    }();
    /***/

  },

  /***/
  "./src/app/shared/model/general-settings.ts":
  /*!**************************************************!*\
    !*** ./src/app/shared/model/general-settings.ts ***!
    \**************************************************/

  /*! exports provided: ConfigSettings, DebugSettings, TimerDirection, GameType, SumPeriods, ExtraTimeAvailable, GameStatus, TimePeriod, TeamNames, Setting21H, Setting22H, DecodeValues, CurrentGame, GeneralSettings, PeriodSettings, ExtraTimeSettings */

  /***/
  function srcAppSharedModelGeneralSettingsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ConfigSettings", function () {
      return ConfigSettings;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DebugSettings", function () {
      return DebugSettings;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TimerDirection", function () {
      return TimerDirection;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GameType", function () {
      return GameType;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SumPeriods", function () {
      return SumPeriods;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ExtraTimeAvailable", function () {
      return ExtraTimeAvailable;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GameStatus", function () {
      return GameStatus;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TimePeriod", function () {
      return TimePeriod;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TeamNames", function () {
      return TeamNames;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Setting21H", function () {
      return Setting21H;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Setting22H", function () {
      return Setting22H;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DecodeValues", function () {
      return DecodeValues;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CurrentGame", function () {
      return CurrentGame;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GeneralSettings", function () {
      return GeneralSettings;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PeriodSettings", function () {
      return PeriodSettings;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ExtraTimeSettings", function () {
      return ExtraTimeSettings;
    });

    var ConfigSettings = function ConfigSettings() {
      _classCallCheck(this, ConfigSettings);
    };

    var DebugSettings = function DebugSettings() {
      _classCallCheck(this, DebugSettings);

      this.debugProtokolWorkflow = false;
      this.debugProtokolSent = true;
      this.decodeProtocol = true; // todo: must be set to true at the end.

      this.logProtocolTimerSettings = true;
      this.logAdditionalInformation = true;
      this.logProtocolContent = true;
      this.logPreProtocolContentSettings = true;
      this.deactiveSentProtocolTimer = false; //todo: must be set to false at the end.

      this.displayPreOnHtml = false;
      this.sendWithBluetooth = false;
      this.displayJson = false;
    };

    var TimerDirection;

    (function (TimerDirection) {
      TimerDirection[TimerDirection["Forward"] = 1] = "Forward";
      TimerDirection[TimerDirection["Backward"] = 2] = "Backward";
    })(TimerDirection || (TimerDirection = {}));

    var GameType;

    (function (GameType) {
      GameType[GameType["Football"] = 1] = "Football";
      GameType[GameType["Hockey"] = 2] = "Hockey";
    })(GameType || (GameType = {}));

    var SumPeriods;

    (function (SumPeriods) {
      SumPeriods[SumPeriods["Yes"] = 1] = "Yes";
      SumPeriods[SumPeriods["No"] = 2] = "No";
    })(SumPeriods || (SumPeriods = {}));

    var ExtraTimeAvailable;

    (function (ExtraTimeAvailable) {
      ExtraTimeAvailable[ExtraTimeAvailable["Yes"] = 1] = "Yes";
      ExtraTimeAvailable[ExtraTimeAvailable["No"] = 2] = "No";
    })(ExtraTimeAvailable || (ExtraTimeAvailable = {}));

    var GameStatus;

    (function (GameStatus) {
      GameStatus[GameStatus["init"] = 1] = "init";
      GameStatus[GameStatus["play"] = 2] = "play";
      GameStatus[GameStatus["pause"] = 3] = "pause";
      GameStatus[GameStatus["periodFinished"] = 4] = "periodFinished";
      GameStatus[GameStatus["gameFinished"] = 5] = "gameFinished";
    })(GameStatus || (GameStatus = {}));

    var TimePeriod;

    (function (TimePeriod) {
      TimePeriod[TimePeriod["normal"] = 1] = "normal";
      TimePeriod[TimePeriod["renewal"] = 2] = "renewal";
      TimePeriod[TimePeriod["Backward"] = 3] = "Backward";
    })(TimePeriod || (TimePeriod = {}));

    var TeamNames = function TeamNames() {
      _classCallCheck(this, TeamNames);

      this.homeTeamName = "";
      this.guestTeamName = "";
    };

    var Setting21H = function Setting21H() {
      _classCallCheck(this, Setting21H);
    }; //todo: mit richtigen standardwerten definieren


    var Setting22H = function Setting22H() {
      _classCallCheck(this, Setting22H);

      this.pfeil = " ";
      this.flagLeft = " ";
      this.flagRight = " ";
    }; // todo: muss in zahlen umbewendelt werden
    // werden nur die HexWerte in string geschrieben


    var DecodeValues = function DecodeValues() {
      _classCallCheck(this, DecodeValues);
    };

    var CurrentGame = function CurrentGame() {
      _classCallCheck(this, CurrentGame);

      this.period = 0;
      this.homeGoals = 0;
      this.guestGoals = 0;
      this.currentMinutes = 0;
      this.currentSec = 0;
      this.totalSec = 0;
      this.timePeriod = TimePeriod.normal;
      this.extraTimeAvailable = ExtraTimeAvailable.No;
    };

    var GeneralSettings = function GeneralSettings() {
      _classCallCheck(this, GeneralSettings);
    };

    var PeriodSettings = function PeriodSettings() {
      _classCallCheck(this, PeriodSettings);
    };

    var ExtraTimeSettings = function ExtraTimeSettings() {
      _classCallCheck(this, ExtraTimeSettings);
    };
    /***/

  },

  /***/
  "./src/app/shared/service/bluetooth.service.ts":
  /*!*****************************************************!*\
    !*** ./src/app/shared/service/bluetooth.service.ts ***!
    \*****************************************************/

  /*! exports provided: BluetoothService */

  /***/
  function srcAppSharedServiceBluetoothServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BluetoothService", function () {
      return BluetoothService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_native_ble_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic-native/ble/ngx */
    "./node_modules/@ionic-native/ble/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _message_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./message.service */
    "./src/app/shared/service/message.service.ts");
    /* harmony import */


    var _global_function__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../global-function */
    "./src/app/shared/global-function.ts");
    /* harmony import */


    var _data_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./data-storage.service */
    "./src/app/shared/service/data-storage.service.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _configuration_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./configuration.service */
    "./src/app/shared/service/configuration.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js"); // es gibt zwei verschiedene BLEs
    //import { BLE } from '@ionic-native/ble';
    // ServiceIDs für AVR-BLE_4DDA und RN4871-EBAD


    var serviceUUID = '49535343-fe7d-4ae5-8fa9-9fafd205e455'; // dev-kid für AVR-BLE_4DDA und RN4871-EBAD

    var characteristicUUID = '49535343-1e4d-4bd9-ba61-23c647249616';

    var BluetoothService = /*#__PURE__*/function () {
      function BluetoothService(configurationService, ble, ngZone, messageService, dataStorageService, translate, platform) {
        var _this = this;

        _classCallCheck(this, BluetoothService);

        this.configurationService = configurationService;
        this.ble = ble;
        this.ngZone = ngZone;
        this.messageService = messageService;
        this.dataStorageService = dataStorageService;
        this.translate = translate;
        this.platform = platform;
        this.devices = [];
        this.configSettingsbluetoothDeviceName = "";
        this.peripheral = {};
        this.sendArray = new Uint8Array();
        this.BluetoothDevicesChanges = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
        this.ConnectionChanges = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
        this.intervalTimer = 3000;
        this.IsConnected = false;
        this.selectedDeviceID = ""; // handle the device name

        this.subscription = this.configurationService.ConfigLoaded.subscribe(function (data) {
          // First load from config
          _this.configSettings = data;

          if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(_this.configSettings.bluetoothDeviceName)) {
            _this.configSettingsbluetoothDeviceName = _this.configSettings.bluetoothDeviceName;
            _this.selectedDeviceName = _this.configSettings.bluetoothDeviceName;
          }

          _this.dataStorageService.getString('deviceName').then(function (data) {
            if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null) {
              _this.selectedDeviceName = data.value;
            }
          });
        });
        this.dataStorageService.getString('BluetoothAutoConnect').then(function (data) {
          if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null) {
            _this.BluetoothAutoConnect = JSON.parse(data.value);
          }
        }); // use session connection if there is something else from the last connection.

        this.dataStorageService.getString('deviceId').then(function (data) {
          if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null) {
            _this.selectedDeviceID = data.value;

            _this.dataStorageService.getString('BluetoothAutoConnect').then(function (data) {
              if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null) {
                var autoconnect = JSON.parse(data.value);

                if (autoconnect) {
                  // connect to device if already stored.
                  _this.connect(_this.selectedDeviceID);
                }
              }
            });
          }
        });
        this.resSubscription = this.translate.get(['CONNECTION_CLOSED_MESSAGE', 'BLUETOOTH_CONNECTION_ESTABLISHED', 'BLUETOOTH_TRY_ESTABLISH_CONNECTION', 'BLUETOOTH_SCAN_ON_DEVICE_NOT_POSSIBLE', 'BLUETOOTH_DISPLAY_SEND_STR_DEBUG', 'BLUETOOTH_UNEXPECTED_DISCONNECTED']).subscribe(function (translation) {
          _this.conClosedMes = translation['CONNECTION_CLOSED_MESSAGE'];
          _this.conEstablishedMes = translation['BLUETOOTH_CONNECTION_ESTABLISHED'];
          _this.conTryEstablishConMes = translation['BLUETOOTH_TRY_ESTABLISH_CONNECTION'];
          _this.scanOnDeviceNotPossibleMes = translation['BLUETOOTH_SCAN_ON_DEVICE_NOT_POSSIBLE'];
          _this.sendStrDebugMes = translation['BLUETOOTH_DISPLAY_SEND_STR_DEBUG'];
          _this.bluetoothDisconnectedMes = translation['BLUETOOTH_UNEXPECTED_DISCONNECTED'];
        });
      }

      _createClass(BluetoothService, [{
        key: "changeBluetoothHandler",
        value: function changeBluetoothHandler() {
          this.BluetoothDevicesChanges.next(this.devices);
        }
      }, {
        key: "changeHandler",
        value: function changeHandler() {
          this.ConnectionChanges.next(this.IsConnected);

          if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(this.selectedDeviceID)) {
            if (this.IsConnected) {
              this.clearConnectionIntervalTimer();
            }
          }
        }
      }, {
        key: "setBlueToothAutoConnect",
        value: function setBlueToothAutoConnect(autoConnect) {
          var strAutoConnect = autoConnect ? "true" : "false";
          this.BluetoothAutoConnect = autoConnect;
          this.dataStorageService.setString('BluetoothAutoConnect', strAutoConnect);
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subscription.unsubscribe();
          this.resSubscription.unsubscribe();
        }
      }, {
        key: "setConnectionIntervalTimer",
        value: function setConnectionIntervalTimer() {
          var _this2 = this;

          this.clearConnectionIntervalTimer();
          this.interval = setInterval(function () {
            _this2.connect(_this2.selectedDeviceID);
          }, this.intervalTimer);
        }
      }, {
        key: "clearConnectionIntervalTimer",
        value: function clearConnectionIntervalTimer() {
          clearInterval(this.interval);
        }
      }, {
        key: "scan",
        value: function scan() {
          var _this3 = this;

          this.devices = []; // clear list

          this.ble.scan([], 30).subscribe(function (device) {
            return _this3.onDeviceDiscovered(device);
          }, function (error) {
            return _this3.scanError(error);
          }); //setTimeout(this.setStatus.bind(this), 5000, 'Scan complete');
        }
      }, {
        key: "disconnect",
        value: function disconnect() {
          var _this4 = this;

          this.ble.disconnect(this.selectedDeviceID).then(function () {
            return _this4.messageService.presentToasterMessage(_this4.conClosedMes);
          }, function (e) {
            return _this4.messageService.presentToasterMessage('Fehler: Verbindung konnte nicht beendet werden.');
          });
          this.IsConnected = false;
          this.changeHandler();
        }
      }, {
        key: "onDeviceDiscovered",
        value: function onDeviceDiscovered(device) {
          var _this5 = this;

          console.log('Gefunden:' + JSON.stringify(device, null, 2));
          this.ngZone.run(function () {
            _this5.devices.push(device); // todo: not clear if requred 


            _this5.changeBluetoothHandler();
          });
        }
      }, {
        key: "scanError",
        value: function scanError(error) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    this.setStatus('Error ' + error);
                    this.messageService.presentToasterMessage(this.scanOnDeviceNotPossibleMes);

                  case 2:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "setStatus",
        value: function setStatus(message) {
          var _this6 = this;

          console.log(message);
          this.ngZone.run(function () {
            _this6.statusMessage = message;
          });
        }
      }, {
        key: "deviceSelected",
        value: function deviceSelected(device) {//console.log(JSON.stringify(device) + ' selected');
        }
      }, {
        key: "connect",
        value: function connect(deviceId) {
          var _this7 = this;

          if (this.conTryEstablishConMes !== "") {
            this.messageService.presentToasterMessage(this.conTryEstablishConMes);
          } // This is not a promise, the device can call disconnect after it connects, so it's an observable


          this.ble.connect(deviceId).subscribe(function (peripheral) {
            return _this7.connectCallback(peripheral);
          }, function () {
            return _this7.disconnectCallback();
          });
        } // no error handling here as we just want to sent when there is a connection.

      }, {
        key: "sent",
        value: function sent(str) {
          //this.messageService.presentToasterMessage('Send if connected:' + str);
          this.sentMessage(str);
        }
      }, {
        key: "sentIfConnected",
        value: function sentIfConnected(str) {
          var _this8 = this;

          if (this.IsConnected) {
            //this.messageService.presentToasterMessage('Send if connected:' + str);
            this.ble.isConnected(this.selectedDeviceID).then(function (e) {
              return _this8.sentMessage(str);
            });
          }
        }
      }, {
        key: "sentMessage",
        value: function sentMessage(str) {
          var _this9 = this;

          var byteArray = _global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].stringToBytes(str);

          this.ble.write(this.selectedDeviceID, serviceUUID, characteristicUUID, byteArray.buffer).then(function (e) {
            return _this9.bleWriteCallback(str);
          }, function (e) {
            return _this9.bleWriteErrorCallback(str);
          });
        }
      }, {
        key: "bleWriteCallback",
        value: function bleWriteCallback(str) {
          if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(this.sendStrDebugMes)) {
            this.messageService.presentToasterMessage(this.sendStrDebugMes + str);
          }
        }
      }, {
        key: "bleWriteErrorCallback",
        value: function bleWriteErrorCallback(str) {
          if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(this.sendStrDebugMes)) {
            this.messageService.presentToasterMessage("Fehler: " + str + " wurde nicht übertragen.");
          }
        } // Verbindung wird aufgebaut.

      }, {
        key: "connectCallback",
        value: function connectCallback(peripheral) {
          this.selectedDeviceID = peripheral.id;
          this.dataStorageService.setString('deviceId', peripheral.id);
          this.peripheral = peripheral; // Devicename festlegen.

          if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(peripheral.Name)) {
            this.dataStorageService.setString('deviceName', peripheral.Name);
            this.selectedDeviceName = peripheral.Name;
          } else {
            var device = this.devices.find(function (x) {
              return x.id == peripheral.id;
            });

            if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(device)) {
              if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(device.name) && device.name !== undefined) {
                this.dataStorageService.setString('deviceName', device.name);
                this.selectedDeviceName = device.name;
              } else if (!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(device.advertising)) {
                this.dataStorageService.setString('deviceName', device.advertising);
                this.selectedDeviceName = device.advertising;
              }
            }
          } // Verbindungsnachricht


          if (this.conEstablishedMes != "") {
            this.messageService.presentToasterMessage(this.conEstablishedMes + this.selectedDeviceName);
          }

          this.IsConnected = true;
          this.changeHandler(); // Timer zum Verbingunsaufbau löschen.

          this.clearConnectionIntervalTimer();
        } // Die Verbindung kann aus verschiedenen Gründen unterbrochen werden.
        // Entfernung zu weit
        // Verbindung kann erst gar nicht aufgebaut werden.

      }, {
        key: "disconnectCallback",
        value: function disconnectCallback() {
          this.messageService.presentToasterMessage(this.bluetoothDisconnectedMes);
          this.IsConnected = false;
          this.changeHandler(); // sobald die Verbindung nicht aufgebaut werden kann startet ein Timer der versucht die Verbindung herzustellen

          this.setConnectionIntervalTimer();
        }
      }]);

      return BluetoothService;
    }();

    BluetoothService.ctorParameters = function () {
      return [{
        type: _configuration_service__WEBPACK_IMPORTED_MODULE_7__["ConfigurationService"]
      }, {
        type: _ionic_native_ble_ngx__WEBPACK_IMPORTED_MODULE_2__["BLE"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
      }, {
        type: _message_service__WEBPACK_IMPORTED_MODULE_3__["MessageService"]
      }, {
        type: _data_storage_service__WEBPACK_IMPORTED_MODULE_5__["DataStorageService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["Platform"]
      }];
    };

    BluetoothService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], BluetoothService);
    /***/
  },

  /***/
  "./src/app/shared/service/configuration.service.ts":
  /*!*********************************************************!*\
    !*** ./src/app/shared/service/configuration.service.ts ***!
    \*********************************************************/

  /*! exports provided: ConfigurationService */

  /***/
  function srcAppSharedServiceConfigurationServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ConfigurationService", function () {
      return ConfigurationService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var ConfigurationService = /*#__PURE__*/function () {
      function ConfigurationService() {
        _classCallCheck(this, ConfigurationService);

        this.ConfigLoaded = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.getSettings();
      }

      _createClass(ConfigurationService, [{
        key: "changeHandler",
        value: function changeHandler() {
          this.ConfigLoaded.next(this.configSettings);
        }
      }, {
        key: "getSettings",
        value: function getSettings() {
          var _this10 = this;

          fetch('./assets/data/configSettings.json').then(function (res) {
            return res.json();
          }).then(function (json) {
            //debugger;
            _this10.configSettings = json;

            _this10.changeHandler();
          });
        }
      }]);

      return ConfigurationService;
    }();

    ConfigurationService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], ConfigurationService);
    /***/
  },

  /***/
  "./src/app/shared/service/data-storage.service.ts":
  /*!********************************************************!*\
    !*** ./src/app/shared/service/data-storage.service.ts ***!
    \********************************************************/

  /*! exports provided: DataStorageService */

  /***/
  function srcAppSharedServiceDataStorageServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStorageService", function () {
      return DataStorageService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @capacitor/core */
    "./node_modules/@capacitor/core/dist/esm/index.js");

    var Storage = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"].Storage;

    var DataStorageService = /*#__PURE__*/function () {
      function DataStorageService() {
        _classCallCheck(this, DataStorageService);
      }

      _createClass(DataStorageService, [{
        key: "setString",
        value: function setString(key, value) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return Storage.set({
                      key: key,
                      value: value
                    });

                  case 2:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3);
          }));
        }
      }, {
        key: "getString",
        value: function getString(key) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return Storage.get({
                      key: key
                    });

                  case 2:
                    return _context4.abrupt("return", _context4.sent);

                  case 3:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4);
          }));
        }
      }, {
        key: "setObject",
        value: function setObject(key, value) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return Storage.set({
                      key: key,
                      value: JSON.stringify(value)
                    });

                  case 2:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5);
          }));
        }
      }, {
        key: "getObject",
        value: function getObject(key) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var ret;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return Storage.get({
                      key: key
                    });

                  case 2:
                    ret = _context6.sent;
                    return _context6.abrupt("return", JSON.parse(ret.value));

                  case 4:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6);
          }));
        }
      }, {
        key: "removeItem",
        value: function removeItem(key) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    _context7.next = 2;
                    return Storage.remove({
                      key: key
                    });

                  case 2:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7);
          }));
        }
      }, {
        key: "clear",
        value: function clear() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    _context8.next = 2;
                    return Storage.clear();

                  case 2:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8);
          }));
        }
      }]);

      return DataStorageService;
    }();

    DataStorageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], DataStorageService);
    /***/
  },

  /***/
  "./src/app/shared/service/message.service.ts":
  /*!***************************************************!*\
    !*** ./src/app/shared/service/message.service.ts ***!
    \***************************************************/

  /*! exports provided: MessageService */

  /***/
  function srcAppSharedServiceMessageServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MessageService", function () {
      return MessageService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

    var MessageService = /*#__PURE__*/function () {
      function MessageService(toastCtrl) {
        _classCallCheck(this, MessageService);

        this.toastCtrl = toastCtrl;
        this.duration = 3000; // top, bottom and middle

        this.position = "bottom";
      }

      _createClass(MessageService, [{
        key: "presentToasterPostionMessage",
        value: function presentToasterPostionMessage(str, position) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    this.position = position;
                    this.presentToasterMessage(str);

                  case 2:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this);
          }));
        }
      }, {
        key: "presentToasterMiddleMessage",
        value: function presentToasterMiddleMessage(str) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
            var toast;
            return regeneratorRuntime.wrap(function _callee10$(_context10) {
              while (1) {
                switch (_context10.prev = _context10.next) {
                  case 0:
                    _context10.next = 2;
                    return this.toastCtrl.create({
                      message: str,
                      duration: this.duration,
                      position: "middle"
                    });

                  case 2:
                    toast = _context10.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context10.stop();
                }
              }
            }, _callee10, this);
          }));
        }
      }, {
        key: "presentToasterMessage",
        value: function presentToasterMessage(str) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
            var toast;
            return regeneratorRuntime.wrap(function _callee11$(_context11) {
              while (1) {
                switch (_context11.prev = _context11.next) {
                  case 0:
                    _context11.next = 2;
                    return this.toastCtrl.create({
                      message: str,
                      duration: this.duration
                    });

                  case 2:
                    toast = _context11.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context11.stop();
                }
              }
            }, _callee11, this);
          }));
        }
      }]);

      return MessageService;
    }();

    MessageService.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }];
    };

    MessageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], MessageService);
    /***/
  },

  /***/
  "./src/app/shared/service/settings.service.ts":
  /*!****************************************************!*\
    !*** ./src/app/shared/service/settings.service.ts ***!
    \****************************************************/

  /*! exports provided: SettingsService */

  /***/
  function srcAppSharedServiceSettingsServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SettingsService", function () {
      return SettingsService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _model_general_settings__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../model/general-settings */
    "./src/app/shared/model/general-settings.ts");
    /* harmony import */


    var _data_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./data-storage.service */
    "./src/app/shared/service/data-storage.service.ts");
    /* harmony import */


    var _global_function__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../global-function */
    "./src/app/shared/global-function.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

    var SettingsService = /*#__PURE__*/function () {
      function SettingsService(dataStorageService, translate) {
        var _this11 = this;

        _classCallCheck(this, SettingsService);

        this.dataStorageService = dataStorageService;
        this.translate = translate; //languages: unknown;

        this.teamNames = new _model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TeamNames"]();
        this.hasTimeTransfer = true;
        this.generalSettings = [];
        this.getGeneralSettings();
        this.subscription = this.translate.get(['TEAM1_DEFAULT_NAME', 'TEAM2_DEFAULT_NAME']).subscribe(function (translation) {
          _this11.teamNames.homeTeamName = translation['TEAM1_DEFAULT_NAME'];
          _this11.teamNames.guestTeamName = translation['TEAM2_DEFAULT_NAME'];
        }); // get values from storage

        this.dataStorageService.getString('HomeTeamName').then(function (data) {
          if (!_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(data) && data.value != null) {
            _this11.teamNames.homeTeamName = data.value;
          }
        });
        this.dataStorageService.getString('GuestTeamName').then(function (data) {
          if (!_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(data) && data.value != null) {
            _this11.teamNames.guestTeamName = data.value;
          }
        });
        this.dataStorageService.getString('HasTimeTransfer').then(function (data) {
          if (!_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(data) && data.value != null) {
            _this11.hasTimeTransfer = JSON.parse(data.value);
          }
        });
      }

      _createClass(SettingsService, [{
        key: "getGeneralSettings$",
        value: function getGeneralSettings$() {
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.generalSettings);
        }
      }, {
        key: "getGeneralSettings",
        value: function getGeneralSettings() {
          var _this12 = this;

          fetch('./assets/data/generalSettings.json').then(function (res) {
            return res.json();
          }).then(function (json) {
            _this12.generalSettings = json;
          });
        }
      }, {
        key: "updateTeamNames",
        value: function updateTeamNames(teamNames) {
          this.teamNames.guestTeamName = teamNames.guestTeamName;
          this.teamNames.homeTeamName = teamNames.homeTeamName;
          this.dataStorageService.setString('HomeTeamName', teamNames.homeTeamName);
          this.dataStorageService.setString('GuestTeamName', teamNames.guestTeamName);
        }
      }, {
        key: "updateTimeTransfer",
        value: function updateTimeTransfer(transferTime) {
          this.hasTimeTransfer = transferTime;
          this.dataStorageService.setString('HasTimeTransfer', String(transferTime));
        }
      }]);

      return SettingsService;
    }();

    SettingsService.ctorParameters = function () {
      return [{
        type: _data_storage_service__WEBPACK_IMPORTED_MODULE_4__["DataStorageService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"]
      }];
    };

    SettingsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], SettingsService);
    /***/
  }
}]);
//# sourceMappingURL=default~pages-bluetooth-bluetooth-module~pages-period-period-module~pages-play-play-module~pages-set~5e7f80e6-es5.js.map