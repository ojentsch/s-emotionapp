(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-period-period-module~pages-play-play-module~pages-settings-advanced-settings-advanced-~341d79b8"],{

/***/ "./src/app/shared/service/current-game.service.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/service/current-game.service.ts ***!
  \********************************************************/
/*! exports provided: CurrentGameService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentGameService", function() { return CurrentGameService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _model_general_settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/general-settings */ "./src/app/shared/model/general-settings.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _football_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./football.service */ "./src/app/shared/service/football.service.ts");
/* harmony import */ var _hockey_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./hockey.service */ "./src/app/shared/service/hockey.service.ts");
/* harmony import */ var _global_function__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../global-function */ "./src/app/shared/global-function.ts");
/* harmony import */ var _protocol_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./protocol.service */ "./src/app/shared/service/protocol.service.ts");
/* harmony import */ var _settings_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./settings.service */ "./src/app/shared/service/settings.service.ts");









let CurrentGameService = class CurrentGameService {
    constructor(footballService, hockeyService, protocolService, settingsService) {
        this.footballService = footballService;
        this.hockeyService = hockeyService;
        this.protocolService = protocolService;
        this.settingsService = settingsService;
        this.GameChanges = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        if (this.currentGame == null) {
            this.currentGame = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["CurrentGame"]();
        }
    }
    changeHandler() {
        this.GameChanges.next(this.currentGame);
    }
    setTeamNames() {
        this.currentGame.homeTeamName = this.settingsService.teamNames.homeTeamName;
        this.currentGame.guestTeamName = this.settingsService.teamNames.guestTeamName;
    }
    // Verlängerung hockey bisher nicht eingeplant.
    internHockeySetter(hockeySetting) {
        if (!_global_function__WEBPACK_IMPORTED_MODULE_6__["GlobalFunction"].isEmpty(hockeySetting)) {
            this.currentGame.regularTimeTargetMinutes = 0;
            this.currentGame.periodMinutes = hockeySetting.durationMin;
            this.currentGame.currentMinutes = hockeySetting.durationMin;
            this.currentGame.regularTimeTargetMinutes = hockeySetting.durationMin;
            this.currentGame.regularTimeNumPeriods = hockeySetting.periodAmounts;
            this.currentGame.extraTimeTargetMinutes = 0;
            this.currentGame.extraTimeNumPeriods = 0;
            this.currentGame.extraTimeAvailable = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["ExtraTimeAvailable"].No;
            this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].init;
            this.setTeamNames();
        }
    }
    internFootballSetter(footballSetting, periodSettingId) {
        this.currentGame.periodMinutes = footballSetting.durationMin;
        this.currentGame.targetMinutes = footballSetting.durationMin;
        this.currentGame.regularTimeTargetMinutes = footballSetting.durationMin;
        this.currentGame.regularTimeNumPeriods = footballSetting.periodAmounts;
        this.setTeamNames();
        // zeiten fuer die moegliche verlanderung schon zu beginn des Spiels laden
        let extraTime = this.footballService.footballSettingsExtraTime.find(x => x.relatedSettingId == periodSettingId);
        if (!_global_function__WEBPACK_IMPORTED_MODULE_6__["GlobalFunction"].isEmpty(extraTime)) {
            this.currentGame.extraTimeAvailable = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["ExtraTimeAvailable"].Yes;
            this.currentGame.extraTimeTargetMinutes = extraTime.durationMin;
            this.currentGame.extraTimeNumPeriods = extraTime.periodAmounts;
        }
    }
    getdata() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(this.currentGame);
    }
    initHockeyValues() {
        this.currentGame.sumPeriods = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].No;
        this.currentGame.gameType = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey;
        this.currentGame.period = 1;
        this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].init;
        this.currentGame.timerDirection = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Backward;
        this.currentGame.timePeriod = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal;
        this.currentGame.currentSec = 0;
        this.currentGame.homeGoals = 0;
        this.currentGame.guestGoals = 0;
        // es ist nur eine Spielzeit vorgesehen. Daher wird direkt initialisiert. 
        if (this.hockeyService.hockeySettings.length == 1) {
            let hockeySetting = this.hockeyService.hockeySettings[0];
            this.internHockeySetter(hockeySetting);
        }
        this.changeHandler();
    }
    // noch nicht getstet. bei meheren Spielzeiten für hockey relevant
    initHockeyDetailValues(periodSettingId) {
        let hockeySetting = this.hockeyService.hockeySettings.find(x => x.periodSettingId == periodSettingId);
        this.internHockeySetter(hockeySetting);
    }
    initFootballValues() {
        this.currentGame = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["CurrentGame"]();
        this.currentGame.gameType = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Football;
        this.currentGame.sumPeriods = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].Yes;
        this.changeHandler();
    }
    initFootballDetailValues(periodSettingId) {
        this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].init;
        this.currentGame.sumPeriods = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].Yes;
        this.currentGame.timePeriod = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal;
        this.currentGame.timerDirection = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Forward;
        this.currentGame.currentSec = 0;
        this.currentGame.currentMinutes = 0;
        this.currentGame.period = 1;
        this.currentGame.homeGoals = 0;
        this.currentGame.guestGoals = 0;
        let footballSetting = this.footballService.footballSettings.find(x => x.periodSettingId == periodSettingId);
        this.internFootballSetter(footballSetting, periodSettingId);
        this.changeHandler();
    }
    hockeyPeriodFinished() {
        if (_global_function__WEBPACK_IMPORTED_MODULE_6__["GlobalFunction"].isEmpty(this.currentGame)) {
            return 0;
        }
        if (this.currentGame.gameType == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameType"].Hockey) {
            if ((this.currentGame.gameStatus == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].periodFinished)
                || (this.currentGame.gameStatus == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished)) {
                return true;
            }
        }
        return false;
    }
    // Ausgabe in Abhängigkeit von Vorwartszähler Rückwärtszähler
    // und Ausgabe accumuiliert oder nicht.
    // nicht geprüft ob Rückwärtszähler akkumuliert ebenfalls funktioniert.
    getSportReleateMinutesValues(currentGame) {
        if (_global_function__WEBPACK_IMPORTED_MODULE_6__["GlobalFunction"].isEmpty(currentGame)) {
            return 0;
        }
        let minFromPeriod = 0;
        let sumperiods = currentGame.sumPeriods;
        // akumuliert
        if (sumperiods == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].Yes) {
            if (currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) { //normale Spielzeit
                if (currentGame.period > 1) {
                    minFromPeriod = (currentGame.period - 1) * currentGame.regularTimeTargetMinutes;
                }
            }
            else { // Verlängerung
                minFromPeriod = (currentGame.period - 1 - currentGame.regularTimeNumPeriods) * this.currentGame.extraTimeTargetMinutes;
            }
            let totalMin = this.currentGame.currentMinutes + minFromPeriod;
            return (totalMin);
        }
        if (currentGame.currentMinutes > 0) {
            return currentGame.currentMinutes;
        }
        return 0;
    }
    getSportReleateMinutesService() {
        let currentGame = this.currentGame;
        return (this.getSportReleateMinutesValues(currentGame));
    }
    // Die Anzeige wird auf Ende des Spiel gesetzt.
    // Wird zur Zeit nicht benutzt.
    setEndGameSettings() {
        this.currentGame.currentSec = 0;
        if (this.currentGame.timerDirection == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Backward) {
            this.currentGame.currentMinutes = 0;
            this.currentGame.currentSec = 0;
            if (this.currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) { //normale Spielzeit
                this.currentGame.period = this.currentGame.regularTimeNumPeriods;
            }
            else {
                this.currentGame.period = this.currentGame.extraTimeNumPeriods;
            }
        }
        else {
            if (this.currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) { //normale Spielzeit
                this.currentGame.period = this.currentGame.regularTimeNumPeriods;
                if (this.currentGame.sumPeriods == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].Yes) {
                    this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
                }
                else {
                    this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
                }
            }
            else {
                if (this.currentGame.sumPeriods == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["SumPeriods"].Yes) {
                    this.currentGame.currentMinutes = this.currentGame.extraTimeNumPeriods * this.currentGame.extraTimeTargetMinutes;
                }
                else {
                    this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
                }
            }
        }
        this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished;
    }
    finishGame() {
        // Die Anzeige wird nicht verändert.
        this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished;
        this.currentGame.currentMinutes = 0;
        this.currentGame.currentSec = 0;
        this.currentGame.period = 0;
        this.changeHandler();
    }
    deleteCurrentGame() {
        this.currentGame = null;
        this.changeHandler();
    }
    changeCurrentGameWithDependencies(currentGame) {
        let teamNames = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TeamNames"]();
        teamNames.homeTeamName = currentGame.homeTeamName;
        teamNames.guestTeamName = currentGame.guestTeamName;
        currentGame.guestTeamName;
        this.settingsService.updateTeamNames(teamNames);
        this.currentGame = currentGame;
        this.changeHandler();
    }
    startNextPeriod() {
        this.currentGame.period++;
        this.currentGame.currentSec = 0;
        if (this.currentGame.timerDirection == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Backward) { //rueckwarszähler z.B start bei 20:00 
            if (this.currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) {
                this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
            }
            else {
                this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
            }
        }
        else { //vorwärts
            this.currentGame.currentMinutes = 0;
        }
        if (this.currentGame.period > this.currentGame.regularTimeNumPeriods) {
            this.currentGame.timePeriod = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].renewal;
            this.currentGame.targetMinutes = this.currentGame.extraTimeTargetMinutes;
        }
        else {
            this.currentGame.targetMinutes = this.currentGame.regularTimeTargetMinutes;
        }
        this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].pause;
        // hack refresh the display 
        // not perfect from this point, but this short solution works.   
        let setting21H = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["Setting21H"]();
        setting21H.flagMid = _protocol_service__WEBPACK_IMPORTED_MODULE_7__["ProtocolService"].setting21GameIsNotRunningFlagASCII;
        setting21H.signalEnd = this.protocolService.setting21H.signalEnd;
        setting21H = _global_function__WEBPACK_IMPORTED_MODULE_6__["GlobalFunction"].setSettings21HFromCurrentGame(this.currentGame, setting21H);
        this.protocolService.setting21H = setting21H;
        this.changeHandler();
    }
    ;
    goalTeam1Add() {
        if (this.currentGame.homeGoals < 99) {
            this.currentGame.homeGoals++;
        }
        else {
            this.currentGame.homeGoals = 0;
        }
        this.changeHandler();
    }
    goalTeam2Add() {
        if (this.currentGame.guestGoals < 99) {
            this.currentGame.guestGoals++;
        }
        else {
            this.currentGame.guestGoals = 0;
        }
        this.changeHandler();
    }
    goalTeam1Substract() {
        if (this.currentGame.homeGoals > 0) {
            this.currentGame.homeGoals--;
        }
        else {
            this.currentGame.homeGoals = 99;
        }
        this.changeHandler();
    }
    goalTeam2Substract() {
        if (this.currentGame.guestGoals > 0) {
            this.currentGame.guestGoals--;
        }
        else {
            this.currentGame.guestGoals = 99;
        }
        this.changeHandler();
    }
    // sekundenzaehler.
    tick() {
        if (this.currentGame.timerDirection == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimerDirection"].Backward) {
            if (this.currentGame.currentSec > 0) { //nur sekundenzähler
                this.currentGame.currentSec--;
            }
            else {
                if (this.currentGame.currentMinutes > 0) { //miuntenzähler
                    this.currentGame.currentMinutes--;
                    this.currentGame.currentSec = 59;
                }
                else { //Periode oder Spiel beendet
                    let numTotalPeriods = 0;
                    if (this.currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) {
                        numTotalPeriods = this.currentGame.regularTimeNumPeriods;
                        //this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
                    }
                    else { // noch nicht getestet
                        numTotalPeriods = this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods;
                        //this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
                    }
                    if (this.currentGame.period < numTotalPeriods) { // Periode beendet.
                        this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].periodFinished;
                    }
                    else { // Spiel oder Verlaengerung beendet
                        this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished;
                    }
                    this.changeHandler();
                }
                ;
            }
        }
        else { // forward fussball
            if (this.currentGame.currentSec < 59) {
                this.currentGame.currentSec++;
            }
            else {
                this.currentGame.currentSec = 0;
                this.currentGame.currentMinutes++;
                if ((this.currentGame.timePeriod == _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["TimePeriod"].normal) && (this.currentGame.currentMinutes) < this.currentGame.regularTimeTargetMinutes) {
                    // normale spielezeit
                }
                else if ((this.currentGame.currentMinutes) < this.currentGame.extraTimeTargetMinutes) {
                    // verlängerung
                }
                else { // spiel oder Verlängerung beendet
                    if ((this.currentGame.period == this.currentGame.regularTimeNumPeriods) ||
                        (this.currentGame.period == this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods)) {
                        // Spiel oder Verlängerung beendet.
                        this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].gameFinished;
                    }
                    else {
                        // Spielzeit beendet
                        this.currentGame.gameStatus = _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["GameStatus"].periodFinished;
                    }
                    this.changeHandler();
                }
                ;
            }
        } // ende fussball
    }
};
CurrentGameService.ctorParameters = () => [
    { type: _football_service__WEBPACK_IMPORTED_MODULE_4__["FootballService"] },
    { type: _hockey_service__WEBPACK_IMPORTED_MODULE_5__["HockeyService"] },
    { type: _protocol_service__WEBPACK_IMPORTED_MODULE_7__["ProtocolService"] },
    { type: _settings_service__WEBPACK_IMPORTED_MODULE_8__["SettingsService"] }
];
CurrentGameService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], CurrentGameService);



/***/ }),

/***/ "./src/app/shared/service/debug.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/service/debug.service.ts ***!
  \*************************************************/
/*! exports provided: DebugService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DebugService", function() { return DebugService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _model_general_settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/general-settings */ "./src/app/shared/model/general-settings.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");




let DebugService = class DebugService {
    constructor() {
        this.debugSettings = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["DebugSettings"]();
    }
    getdata() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(this.debugSettings);
    }
};
DebugService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], DebugService);



/***/ }),

/***/ "./src/app/shared/service/football.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/service/football.service.ts ***!
  \****************************************************/
/*! exports provided: FootballService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FootballService", function() { return FootballService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./data-storage.service */ "./src/app/shared/service/data-storage.service.ts");
/* harmony import */ var _global_function__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../global-function */ "./src/app/shared/global-function.ts");
/* harmony import */ var _debug_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./debug.service */ "./src/app/shared/service/debug.service.ts");






// https://edupala.com/ionic-capacitor-storage/
let FootballService = class FootballService {
    constructor(dataStorageService, debugService) {
        this.dataStorageService = dataStorageService;
        this.debugService = debugService;
        this.footballSettings = [];
        this.footballSettingsFiltered = [];
        this.footballSettingsExtraTime = [];
        this.getFootballSettings();
        this.getFootballSettingsExtraTime();
        this.debugService.getdata().subscribe((data) => { this.debugSettings = data; });
    }
    // Initialisieren der Werte, werden in den Local Storage geschrieben. 
    initBasicSettingsFromConfig() {
        fetch('./assets/data/footballSettings.json').then(res => res.json())
            .then(json => {
            this.footballSettings = json;
            let jsonString = JSON.stringify(this.footballSettings);
            this.dataStorageService.setString('footballSettings', jsonString);
        });
    }
    // Initialisieren der Werte für die Verlängerung, werden in den Local Storage geschrieben. 
    initExtraSettingsFromConfig() {
        fetch('./assets/data/footballSettingsExtraTime.json').then(res => res.json())
            .then(json => {
            this.footballSettingsExtraTime = json;
            let jsonString = JSON.stringify(this.footballSettingsExtraTime);
            this.dataStorageService.setString('footballSettingsExtraTime', jsonString);
        });
    }
    getFootballSettings() {
        this.dataStorageService.getString('footballSettings').then((data) => {
            if ((!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                this.footballSettings = JSON.parse(data.value);
                //this.filterdata();
                if (this.debugSettings.displayJson) {
                    console.log(this.footballSettings);
                }
            }
            else {
                this.initBasicSettingsFromConfig();
            }
        });
    }
    getFootballSettingsExtraTime() {
        this.dataStorageService.getString('footballSettingsExtraTime').then((data) => {
            if ((!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                this.footballSettingsExtraTime = JSON.parse(data.value);
                if (this.debugSettings.displayJson) {
                    console.log(this.footballSettingsExtraTime);
                }
            }
            else {
                this.initExtraSettingsFromConfig();
            }
        });
    }
    // currently not used, but filterint the data with search would be possible.
    filterdata() {
        let filterSettings1 = "U 11";
        let filterSettings2 = "U 12";
        this.footballSettingsFiltered = this.footballSettings.filter(x => x.settingName.indexOf(filterSettings1) !== -1
            || x.settingName.indexOf(filterSettings2) !== -1);
    }
    getdata() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.footballSettings);
    }
    deleteCustomConfiguration() {
        this.dataStorageService.removeItem("footballSettings");
        this.dataStorageService.removeItem("footballSettingsExtraTime");
    }
    deleteSetting(settingID) {
        // elemente aus dem Array entfernen.
        let index = this.footballSettings.findIndex(x => x.periodSettingId == settingID);
        if (index == -1) {
            return;
        }
        this.footballSettings.splice(index, 1);
        // alten Konfigurationswerte überschreiben
        let jsonStr = JSON.stringify(this.footballSettings);
        this.dataStorageService.setString('footballSettings', jsonStr);
        // ggf. Verlängerungen ebenfalls löschen 
        let indexExtraTime = this.footballSettingsExtraTime.findIndex(x => x.relatedSettingId == settingID);
        if (indexExtraTime != -1) {
            this.footballSettingsExtraTime.splice(indexExtraTime, 1);
            jsonStr = JSON.stringify(this.footballSettingsExtraTime);
            this.dataStorageService.setString('footballSettingsExtraTime', jsonStr);
        }
    }
    addNewNormalPeriod(footballSetting) {
        this.newSettingId = Math.max(...this.footballSettings.map(o => o.periodSettingId)) + 1;
        //footballSetting.settingName = footballSetting.durationMin.toString();
        footballSetting.periodSettingId = this.newSettingId;
        this.footballSettings.push(footballSetting);
        // alten Konfigurationswert überschreiben
        let jsonStr = JSON.stringify(this.footballSettings);
        this.dataStorageService.setString('footballSettings', jsonStr);
    }
    addNewExtraPeriod(footballExtraTimeSetting) {
        this.newExraTimeSettingId = Math.max(...this.footballSettingsExtraTime.map(o => o.extraTimeSettingId)) + 1;
        footballExtraTimeSetting.relatedSettingId = this.newSettingId;
        footballExtraTimeSetting.extraTimeSettingId = this.newExraTimeSettingId;
        this.footballSettingsExtraTime.push(footballExtraTimeSetting);
        // alten Konfigurationswert überschreiben
        let jsonString = JSON.stringify(this.footballSettingsExtraTime);
        this.dataStorageService.setString('footballSettingsExtraTime', jsonString);
    }
};
FootballService.ctorParameters = () => [
    { type: _data_storage_service__WEBPACK_IMPORTED_MODULE_3__["DataStorageService"] },
    { type: _debug_service__WEBPACK_IMPORTED_MODULE_5__["DebugService"] }
];
FootballService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], FootballService);



/***/ }),

/***/ "./src/app/shared/service/hockey.service.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/service/hockey.service.ts ***!
  \**************************************************/
/*! exports provided: HockeyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HockeyService", function() { return HockeyService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _data_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./data-storage.service */ "./src/app/shared/service/data-storage.service.ts");
/* harmony import */ var _global_function__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../global-function */ "./src/app/shared/global-function.ts");
/* harmony import */ var _debug_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./debug.service */ "./src/app/shared/service/debug.service.ts");






let HockeyService = class HockeyService {
    constructor(dataStorageService, debugService) {
        this.dataStorageService = dataStorageService;
        this.debugService = debugService;
        this.hockeySettings = [];
        this.hockeySettingsExtraTime = [];
        this.getBasicSettings();
        this.debugService.getdata().subscribe((data) => { this.debugSettings = data; });
    }
    // Initialisieren der Werte: werden in den Local Storage geschrieben. 
    initBasicSettingsFromConfig() {
        fetch('./assets/data/hockeySettings.json').then(res => res.json())
            .then(json => {
            this.hockeySettings = json;
            let jsonString = JSON.stringify(this.hockeySettings);
            this.dataStorageService.setString('hockeySettings', jsonString);
        });
    }
    getdata() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.hockeySettings);
    }
    getBasicSettings() {
        this.dataStorageService.getString('hockeySettings').then((data) => {
            if ((!_global_function__WEBPACK_IMPORTED_MODULE_4__["GlobalFunction"].isEmpty(data) && data.value != null)) {
                this.hockeySettings = JSON.parse(data.value);
                if (this.debugSettings.displayJson) {
                    console.log(this.hockeySettings);
                }
            }
            else {
                this.initBasicSettingsFromConfig();
            }
        });
    }
    deleteCustomConfiguration() {
        this.dataStorageService.removeItem("hockeySettings");
    }
    deleteSetting(settingID) {
        let index = this.hockeySettings.findIndex(x => x.periodSettingId == settingID);
        if (index == -1) {
            return;
        }
        this.hockeySettings.splice(index, 1);
        // alten Konfigurationswert überschreiben
        let jsonStr = JSON.stringify(this.hockeySettings);
        this.dataStorageService.setString('hockeySettings', jsonStr);
    }
    addNewNormalPeriod(periodSetting) {
        this.newSettingId = Math.max(...this.hockeySettings.map(o => o.periodSettingId)) + 1;
        periodSetting.periodSettingId = this.newSettingId;
        this.hockeySettings.push(periodSetting);
        // alten Konfigurationswert überschreiben
        let jsonStr = JSON.stringify(this.hockeySettings);
        this.dataStorageService.setString('hockeySettings', jsonStr);
    }
};
HockeyService.ctorParameters = () => [
    { type: _data_storage_service__WEBPACK_IMPORTED_MODULE_3__["DataStorageService"] },
    { type: _debug_service__WEBPACK_IMPORTED_MODULE_5__["DebugService"] }
];
HockeyService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], HockeyService);



/***/ }),

/***/ "./src/app/shared/service/protocol.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/service/protocol.service.ts ***!
  \****************************************************/
/*! exports provided: ProtocolService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProtocolService", function() { return ProtocolService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _model_general_settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/general-settings */ "./src/app/shared/model/general-settings.ts");
/* harmony import */ var _global_function__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global-function */ "./src/app/shared/global-function.ts");
/* harmony import */ var _debug_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./debug.service */ "./src/app/shared/service/debug.service.ts");
/* harmony import */ var _bluetooth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bluetooth.service */ "./src/app/shared/service/bluetooth.service.ts");
/* harmony import */ var _settings_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./settings.service */ "./src/app/shared/service/settings.service.ts");







const SETTING21_GAME_SIGNAL_ENDE_Off_FLAG_ASCII = "0";
const SETTING21_GAME_SIGNAL_ENDE_ON_FLAG_ASCII = "1";
const SETTING22_Pfeil = " ";
const SETTING22_FLAG_LINKS = " ";
const SETTING22_FLAG_RECHTS = " ";
let ProtocolService = class ProtocolService {
    constructor(debugService, bluetoothService, settingsService) {
        this.debugService = debugService;
        this.bluetoothService = bluetoothService;
        this.settingsService = settingsService;
        // Verbindung zur Anzeige
        this.isConnected = false;
        // Bestimmt die Decodierung
        this.decodeCounter = 0;
        // steuert die Zeitübertragung jede Sekunde
        this.interval = 1000;
        // Erste Zeichen, das übertragen wird: ASCII STX
        this.startByte = 0x2;
        // Letzete Zeichen, das übertragen wird: ASCII ETX
        this.endByte = 0x3;
        // Uhr aktiv / nicht aktiv
        this.setting20HUhrAktiv = "1"; // hex 31
        this.setting20HUhrNichtAktiv = "0"; // hex 30
        // Zeahler senden Hupensignal 
        this.signalSentCounter = 0;
        // die 1/10 Sekunden werden nicht übertragen.
        this.setting21SecDiv10FlagASCII = " ";
        // zeigt an, ob die sekündliche Übertragung der Zeit aktiv ist.
        this.transmitTimeIsActive = false;
        // Zeiteinstellungen (settings) Protokoll für die sekündliche Übertragung.
        this.setting21H = new _model_general_settings__WEBPACK_IMPORTED_MODULE_2__["Setting21H"]();
        // Decoderungstabelle
        this.decodeValuesAr = [
            {
                "index": 0,
                "identifier": 0x30,
                "byte1": 0x85,
                "byte2": 0xBA,
                "byte3": 0x90,
                "byte4": 0xA5,
            },
            {
                "index": 1,
                "identifier": 0x31,
                "byte1": 0xB1,
                "byte2": 0xA2,
                "byte3": 0x9F,
                "byte4": 0x8B,
            },
            {
                "index": 2,
                "identifier": 0x32,
                "byte1": 0xAA,
                "byte2": 0xB2,
                "byte3": 0xA7,
                "byte4": 0x99,
            },
            {
                "index": 3,
                "identifier": 0x33,
                "byte1": 0x97,
                "byte2": 0xA6,
                "byte3": 0xB3,
                "byte4": 0x85,
            },
            {
                "index": 4,
                "identifier": 0x34,
                "byte1": 0x9D,
                "byte2": 0x84,
                "byte3": 0xBC,
                "byte4": 0xAD,
            },
            {
                "index": 5,
                "identifier": 0x35,
                "byte1": 0x8F,
                "byte2": 0xA8,
                "byte3": 0xBD,
                "byte4": 0xB1,
            },
            {
                "index": 6,
                "identifier": 0x36,
                "byte1": 0x9C,
                "byte2": 0xA0,
                "byte3": 0x87,
                "byte4": 0xBA,
            },
            {
                "index": 7,
                "identifier": 0x37,
                "byte1": 0xC1,
                "byte2": 0xC7,
                "byte3": 0x99,
                "byte4": 0x88,
            },
            {
                "index": 8,
                "identifier": 0x38,
                "byte1": 0xB2,
                "byte2": 0x86,
                "byte3": 0x95,
                "byte4": 0x84,
            },
            {
                "index": 9,
                "identifier": 0x39,
                "byte1": 0x83,
                "byte2": 0xB7,
                "byte3": 0x81,
                "byte4": 0xB0,
            }
        ];
        this.debugService.getdata().subscribe((data) => { this.debugSettings = data; });
        // react on connection changes
        this.isConnected = false;
        this.subscription = this.bluetoothService.ConnectionChanges.subscribe((data) => { this.isConnected = data; });
        // Solange eine Übertragung besteht
        // müssen beim Neustart die alten Werte übernommen werden
        // Settings für die sekündliche Übertragung
        this.setting21H.min10 = "0";
        this.setting21H.min1 = "0";
        this.setting21H.sec10 = "0";
        this.setting21H.sec1 = "0";
        this.setting21H.secDiv10 = " ";
        this.setting21H.flagMid = "2"; //Pausiert
        this.setting21H.signalEnd = "0"; //Kein Signal
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    // nur der Inhalt wird decodiert
    decodeValues(numArray, start, end) {
        if (end > numArray.length) {
            // error
            return (null);
        }
        // das dritte Element enthält immer den Decodierungsschlüssel.
        let decodeElement = this.decodeValuesAr.find(x => x.identifier == numArray[2]);
        if (_global_function__WEBPACK_IMPORTED_MODULE_3__["GlobalFunction"].isEmpty(decodeElement)) {
            // error
            return (null);
        }
        // decodierung is deakiviert. Zu Testzwecken eingebaut.
        if (!this.debugSettings.decodeProtocol) {
            return (numArray);
        }
        let count = 1;
        for (let index = start; index <= end; index++) {
            switch (count) {
                case 1:
                    numArray[index] = _global_function__WEBPACK_IMPORTED_MODULE_3__["GlobalFunction"].xor(numArray[index], decodeElement.byte1);
                    break;
                case 2:
                    numArray[index] = _global_function__WEBPACK_IMPORTED_MODULE_3__["GlobalFunction"].xor(numArray[index], decodeElement.byte2);
                    break;
                case 3:
                    numArray[index] = _global_function__WEBPACK_IMPORTED_MODULE_3__["GlobalFunction"].xor(numArray[index], decodeElement.byte3);
                    break;
                case 4:
                    numArray[index] = _global_function__WEBPACK_IMPORTED_MODULE_3__["GlobalFunction"].xor(numArray[index], decodeElement.byte4);
                    break;
            }
            count++;
            if (count == 5) { // es sind nur 4 Schlüssel vorhanden => Beginne wieder mit dem ersten Schlüssel
                count = 1;
            }
        }
        return (numArray);
    }
    // Start der übertragung an die Anzeige
    startTransmitTimer() {
        // sekündliche Übertragung wird gestartet
        if (this.transmitTimeIsActive) {
            return;
        }
        if (this.debugSettings.deactiveSentProtocolTimer) {
            return;
        }
        // Intevall für die sekündliche Übertragung der Zeit
        this.intervalTimer = setInterval(() => {
            this.prot21h();
        }, this.interval);
        this.transmitTimeIsActive = true;
    }
    /// Ende der Übertragung an die Anzeige
    endTransmitTimer() {
        if (this.debugSettings.deactiveSentProtocolTimer) {
            return;
        }
        clearInterval(this.intervalTimer);
        this.transmitTimeIsActive = false;
        if (this.debugSettings.logAdditionalInformation) {
            console.log("Die sekündliche Übertragung der stehenden oder laufenden Zeit wurde beendet.");
            console.log("Damit müsste sich die Anzeige auch ausschalten.");
        }
    }
    // Aktion wird bei Ende einer Periode Hockey und manuelles auslösen bei Hockey aktiviert.
    sentTimeWithSignal() {
        this.signalSentCounter = 5;
    }
    getIdentDecodeFromIndex(index) {
        let element = this.decodeValuesAr.find(x => x.index == index);
        if (element) {
            return (element.identifier);
        }
        return 0;
    }
    // Erkennung der Sportart und Synchronisieren der Uhr
    prot20h(sportTypeVal1, sportTypeVal2) {
        let intArray = [];
        intArray[0] = this.startByte;
        intArray[1] = 0x20;
        intArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich
        // 4-5 Sportart
        intArray[3] = sportTypeVal1;
        intArray[4] = sportTypeVal2;
        // ob Uhrzeit uebertragen wird. Wenn nicht schaltet sich die Anzeige nach Beenden des Spiels ab, ansonsten läuft die Uhr weiter
        if (this.settingsService.hasTimeTransfer) {
            intArray[5] = this.setting20HUhrAktiv.charCodeAt(0); // todo: recheck and make sure that the ASCII of 1 is correct at this point
        }
        else {
            intArray[5] = this.setting20HUhrNichtAktiv.charCodeAt(0);
        }
        // Uhrzeit Stunden und Minuten werden übertragen.
        let hourStr = "";
        let minStr = "";
        var d = new Date();
        var h = d.getHours();
        if (h < 10) {
            hourStr = "0" + h;
        }
        else {
            hourStr = "" + h;
        }
        var m = d.getMinutes();
        if (m < 10) {
            minStr = "0" + m;
        }
        else {
            minStr = "" + m;
        }
        // 7-10: Stunden Minuten 10er 1er
        intArray[6] = hourStr.charCodeAt(0);
        intArray[7] = hourStr.charCodeAt(1);
        intArray[8] = minStr.charCodeAt(0);
        intArray[9] = minStr.charCodeAt(1);
        intArray[10] = this.endByte;
        if (this.debugSettings.logPreProtocolContentSettings) {
            console.log("sportTypeVal1:" + sportTypeVal1);
            console.log("sportTypeVal2:" + sportTypeVal2);
            console.log("hourStr:" + hourStr);
            console.log("minStr:" + hourStr);
        }
        let sendArray = this.decodeValues(intArray, 3, 9);
        this.sentProtokoll(sendArray, "prot20h");
    }
    // Spielzeit und Hupe
    // muss jede Sekunde übertragen werden, egal ob Uhr läuft oder nicht.
    prot21h() {
        this.setting21H.secDiv10 = this.setting21SecDiv10FlagASCII;
        // Falls aktiviert das signal 5 mal senden.
        if (this.signalSentCounter > 0) {
            this.setting21H.signalEnd = SETTING21_GAME_SIGNAL_ENDE_ON_FLAG_ASCII;
        }
        else {
            this.setting21H.signalEnd = SETTING21_GAME_SIGNAL_ENDE_Off_FLAG_ASCII;
        }
        if (this.setting21H.min10 == undefined) {
            this.setting21H.min10 = "0";
        }
        if (this.setting21H.min1 == undefined) {
            this.setting21H.min1 = "0";
        }
        if (this.setting21H.sec10 == undefined) {
            this.setting21H.sec10 = "0";
        }
        if (this.setting21H.sec1 == undefined) {
            this.setting21H.sec1 = "0";
        }
        // Pausiert
        if (this.setting21H.flagMid == undefined) {
            this.setting21H.flagMid = "2";
        }
        if (this.setting21H.signalEnd == undefined) {
            this.setting21H.signalEnd = "0";
        }
        let intArray = [];
        intArray[0] = this.startByte;
        intArray[1] = 0x21;
        intArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich
        intArray[3] = this.setting21H.min10.charCodeAt(0);
        intArray[4] = this.setting21H.min1.charCodeAt(0);
        intArray[5] = this.setting21H.sec10.charCodeAt(0);
        intArray[6] = this.setting21H.sec1.charCodeAt(0);
        intArray[7] = this.setting21H.secDiv10.charCodeAt(0); //20h: zehntel Sekunden werden nicht übertragen.
        intArray[8] = this.setting21H.flagMid.charCodeAt(0); // 31: Spiel läuft 32: Spiel pausiert
        intArray[9] = this.setting21H.signalEnd.charCodeAt(0); // 31: Hupe an     30: Hupe aus
        if (this.setting21H.signalEnd == "1") {
            // hupe test
            debugger;
        }
        if (this.setting21H.flagMid == "1") {
            console.log("Spiel läuft");
        }
        if (this.setting21H.flagMid == "2") {
            console.log("Spiel angehalten");
        }
        intArray[10] = this.endByte;
        let sentArray = this.decodeValues(intArray, 3, 9);
        if (this.debugSettings.logPreProtocolContentSettings) {
            console.log(this.setting21H);
        }
        this.sentProtokoll(sentArray, "prot21h");
        // senden des signals wieder herunter zählen.
        if (this.signalSentCounter > 0) {
            this.signalSentCounter--;
        }
    }
    // Periode
    // hat beim Fußball keine Bedeutung
    // Ansonsten auch nur die Periode relevant
    // Für andere Parameter leere Werte übertragen
    prot22h(setting22H) {
        setting22H.flagLeft = SETTING22_FLAG_LINKS;
        setting22H.flagRight = SETTING22_FLAG_RECHTS;
        setting22H.pfeil = SETTING22_Pfeil;
        let numArray = [];
        numArray[0] = this.startByte;
        numArray[1] = 0x22;
        numArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); //30h-39h möglich
        numArray[3] = setting22H.period.charCodeAt(0);
        numArray[4] = setting22H.pfeil.charCodeAt(0);
        numArray[5] = setting22H.flagLeft.charCodeAt(0);
        numArray[6] = setting22H.flagRight.charCodeAt(0);
        numArray[7] = this.endByte;
        if (this.debugSettings.logPreProtocolContentSettings) {
            console.log(setting22H);
        }
        let sendArray = this.decodeValues(numArray, 3, 6);
        this.sentProtokoll(sendArray, "prot22h");
    }
    // Spielstand
    prot23h(homeGoals, guestGoals) {
        // als 100er Stelle wird immer eine 0 übertragen
        let strHomeGoals = "";
        let strGuestGoals = "";
        if (homeGoals < 10) {
            strHomeGoals = "00" + homeGoals.toString();
        }
        else {
            strHomeGoals = "0" + homeGoals.toString();
        }
        if (guestGoals < 10) {
            strGuestGoals = "00" + guestGoals.toString();
        }
        else {
            strGuestGoals = "0" + guestGoals.toString();
        }
        let numArray = [];
        numArray[0] = this.startByte;
        numArray[1] = 0x23;
        numArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich
        // 4-6 Übertragung Tore Heimmanschaft
        numArray[3] = strHomeGoals.charCodeAt(0);
        numArray[4] = strHomeGoals.charCodeAt(1);
        numArray[5] = strHomeGoals.charCodeAt(2);
        // 4-6 Übertragung Tore Gastmannschaft
        // 10er, 1er, 100er
        numArray[6] = strGuestGoals.charCodeAt(0);
        numArray[7] = strGuestGoals.charCodeAt(1);
        numArray[8] = strGuestGoals.charCodeAt(2);
        numArray[9] = this.endByte;
        if (this.debugSettings.logPreProtocolContentSettings) {
            console.log("Zahlenwerte von :");
            console.log("strHomeGoals:" + strHomeGoals);
            console.log("strGuestGoals:" + strGuestGoals);
        }
        let sendArray = this.decodeValues(numArray, 3, 8);
        this.sentProtokoll(sendArray, "prot23h");
    }
    //Mannschaftsnamen
    prot29h(teamNames) {
        // auffüllen beider Mannschaftsnamen mit Leerzeichen am Ende auf insgesamt 8 Zeichen für die Übertragung.
        let addCharsHomeNum = (8 - teamNames.homeTeamName.length);
        let addCharsGuestNum = (8 - teamNames.guestTeamName.length);
        let homeTeamWithFilledChars = teamNames.homeTeamName;
        for (let index = 0; index < addCharsHomeNum; index++) {
            homeTeamWithFilledChars = homeTeamWithFilledChars + " ";
        }
        let guestTeamWithFilledChars = teamNames.guestTeamName;
        for (let index = 0; index < addCharsGuestNum; index++) {
            guestTeamWithFilledChars = guestTeamWithFilledChars + " ";
        }
        let numArray = [];
        numArray[0] = this.startByte;
        numArray[1] = 0x29;
        numArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich
        // Heimmanschaft 8 Zeichen (4-11)
        numArray[3] = homeTeamWithFilledChars.charCodeAt(0);
        numArray[4] = homeTeamWithFilledChars.charCodeAt(1);
        numArray[5] = homeTeamWithFilledChars.charCodeAt(2);
        numArray[6] = homeTeamWithFilledChars.charCodeAt(3);
        numArray[7] = homeTeamWithFilledChars.charCodeAt(4);
        numArray[8] = homeTeamWithFilledChars.charCodeAt(5);
        numArray[9] = homeTeamWithFilledChars.charCodeAt(6);
        numArray[10] = homeTeamWithFilledChars.charCodeAt(7);
        // Gästemanschaft 8 Zeichen (12-18)
        numArray[11] = guestTeamWithFilledChars.charCodeAt(0);
        numArray[12] = guestTeamWithFilledChars.charCodeAt(1);
        numArray[13] = guestTeamWithFilledChars.charCodeAt(2);
        numArray[14] = guestTeamWithFilledChars.charCodeAt(3);
        numArray[15] = guestTeamWithFilledChars.charCodeAt(4);
        numArray[16] = guestTeamWithFilledChars.charCodeAt(5);
        numArray[17] = guestTeamWithFilledChars.charCodeAt(6);
        numArray[18] = guestTeamWithFilledChars.charCodeAt(7);
        numArray[19] = this.endByte;
        if (this.debugSettings.logPreProtocolContentSettings) {
            console.log("Zahlenwerte von :");
            console.log("homeTeamWithFilledChars:" + homeTeamWithFilledChars);
            console.log("guestTeamWithFilledChars:" + guestTeamWithFilledChars);
        }
        // Der Databereich wird verschlüsselt.
        let sendArray = this.decodeValues(numArray, 3, 18);
        this.sentProtokoll(sendArray, "prot29h");
    }
    sentProtokoll(sendArray, name) {
        let charArray = [];
        if (this.debugSettings.debugProtokolWorkflow) {
            debugger;
        }
        for (let index = 0; index < sendArray.length; index++) {
            charArray[index] = String.fromCharCode(sendArray[index]);
        }
        let str = "";
        if (this.debugSettings.logProtocolContent) {
            console.log("Send Prot:" + name);
            console.log(charArray);
        }
        for (let index = 0; index < charArray.length; index++) {
            str += charArray[index];
        }
        // counter zum decodieren hochzählen
        this.decodeCounter++;
        if (this.decodeCounter == 10) { //letzte Element erreicht
            this.decodeCounter = 0;
        }
        if (this.isConnected) {
            this.bluetoothService.sentIfConnected(str);
        }
    }
};
// zwei mögliche Einstellungen: Spiel läuft oder läuft nicht.
ProtocolService.setting21GameIsRunningFlagASCII = "1"; //hex 31
ProtocolService.setting21GameIsNotRunningFlagASCII = "2"; // hex 32
// Stuerung der Hupe
ProtocolService.setting21GameSigEndeOnFlagASCII = "30";
ProtocolService.ctorParameters = () => [
    { type: _debug_service__WEBPACK_IMPORTED_MODULE_4__["DebugService"] },
    { type: _bluetooth_service__WEBPACK_IMPORTED_MODULE_5__["BluetoothService"] },
    { type: _settings_service__WEBPACK_IMPORTED_MODULE_6__["SettingsService"] }
];
ProtocolService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ProtocolService);



/***/ })

}]);
//# sourceMappingURL=default~pages-period-period-module~pages-play-play-module~pages-settings-advanced-settings-advanced-~341d79b8-es2015.js.map