(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-period-period-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/period/period.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/period/period.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>{{ 'SPIELZEIT' | translate }}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<!-- \n<p>\n<button (click)='goBack()'>zurück</button>\n</p> -->\n<ion-grid>\n  <ion-row>\n    <ion-col size=\"4\" class=\"cell-class-border-right\">\n      <div class=\"ion-text-center\">\n      <span *ngIf=\"isFootballNormal()\">{{ 'HALBZEIT' | translate }}</span>\n      <span *ngIf=\"isFootballReneval()\">{{ 'VERLAENGERUNG' | translate }}</span>\n      <span *ngIf=\"isHockey()\">{{ 'PERIODE' | translate }}</span>\n    </div>\n    </ion-col>\n\n    <ion-col size=\"4\" >\n      <div class=\"ion-text-center\">\n      Min\n      </div>\n    </ion-col>\n\n    <ion-col size=\"4\">\n      <div class=\"ion-text-center\">\n      Sek\n    </div>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col size=\"4\" class=\"cell-class-border-right\">\n      <div class=\"ion-text-center\">\n      <button (click)='periodAdd()' [disabled]=\"periodAddDisabled()\">\n        <ion-icon name=\"add-circle-outline\" size=\"large\"></ion-icon>\n        </button>\n      </div>\n    </ion-col>\n    <ion-col size=\"4\">\n      <div class=\"ion-text-center\">\n      <button (click)='minuteAdd()'>\n        <ion-icon name=\"add-circle-outline\" size=\"large\"></ion-icon>\n      </button>\n    </div>\n    </ion-col>\n    <ion-col size=\"4\">\n      <div class=\"ion-text-center\">\n      <button (click)='secondAdd()' [disabled]=\"secDisabled()\">\n        <ion-icon name=\"add-circle-outline\" size=\"large\"></ion-icon>\n      </button>\n    </div>\n    </ion-col>\n\n  </ion-row>\n\n  <ion-row>\n    <ion-col size=\"4\" class=\"cell-class-border-right\">\n      <div class=\"ion-text-center\">\n      {{getPeriodNumber()}}  \n    </div>\n    </ion-col>\n    <ion-col size=\"4\">\n      <div class=\"ion-text-center\">\n      {{getSportReleateMinutes() | number:'2.0'}}\n    </div>\n    </ion-col>\n    <ion-col size=\"4\">\n      <div class=\"ion-text-center\">\n      {{currentGame?.currentSec | number:'2.0'}}\n    </div>\n    </ion-col>\n  </ion-row>\n\n\n\n\n  <ion-row>\n    <ion-col size=\"4\" class=\"cell-class-border-right\">\n      <div class=\"ion-text-center\">\n      <button (click)='periodSubstract()' [disabled]=\"periodSubstractDisabled()\">\n        <ion-icon name=\"remove-circle-outline\" size=\"large\"></ion-icon>\n        </button>\n      </div>\n    </ion-col>\n\n    <ion-col size=\"4\">\n      <div class=\"ion-text-center\">\n      <button (click)='minuteSubstract()' [disabled]=\"minuteSubstractDisabled()\">\n        <ion-icon name=\"remove-circle-outline\" size=\"large\"></ion-icon>\n      </button>\n    </div>\n    </ion-col>\n\n    <ion-col size=\"4\">\n      <div class=\"ion-text-center\">\n      <button (click)='secondSubstract()' [disabled]=\"secDisabled()\">\n        <ion-icon name=\"remove-circle-outline\" size=\"large\"></ion-icon>\n      </button>\n    </div>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"8\">\n      <span *ngIf=\"isFootballNormal()\">{{ 'HALBZEIT' | translate }}</span>\n      <span *ngIf=\"isFootballReneval()\">{{ 'VERLAENGERUNG' | translate }}</span>\n      <span *ngIf=\"isHockey()\">{{ 'PERIODE' | translate }}</span>\n      {{getPeriodNumber()}}/{{getTotalPeriods()}}\n      <br>\n      {{getMinPerPeriod() | number:'2.0' }}:00 Min  / \n      <span *ngIf=\"isFootballNormal()\">{{ 'HALBZEIT' | translate }}</span>\n      <span *ngIf=\"isFootballReneval()\">{{ 'VERLAENGERUNG' | translate }}yyy</span>\n      <span *ngIf=\"isHockey()\">{{ 'PERIODE' | translate }}</span>\n      <br>\n      {{ 'ENDE' | translate }} {{getEndMinPerGame() | number:'2.0'}}:00\n    </ion-col>\n    <ion-col size=\"4\">\n      <div class=\"ion-text-end\">\n        <button (click)='save()' title=\"{{ 'SPEICHERN' | translate }}\">\n          <ion-icon name=\"checkmark\" size=\"large\"></ion-icon>\n        </button>  \n        <button (click)='cancel()' title=\"{{ 'ABBRUCH' | translate }}\">\n          <ion-icon name=\"close\" size=\"large\"></ion-icon>\n        </button>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n<pre *ngIf=\"debugSettings.displayPreOnHtml\">\n  {{currentGame | json}}\n</pre>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/period/period-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/period/period-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: PeriodPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeriodPageRoutingModule", function() { return PeriodPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _period_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./period.page */ "./src/app/pages/period/period.page.ts");




const routes = [
    {
        path: '',
        component: _period_page__WEBPACK_IMPORTED_MODULE_3__["PeriodPage"]
    }
];
let PeriodPageRoutingModule = class PeriodPageRoutingModule {
};
PeriodPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PeriodPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/period/period.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/period/period.module.ts ***!
  \***********************************************/
/*! exports provided: PeriodPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeriodPageModule", function() { return PeriodPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _period_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./period-routing.module */ "./src/app/pages/period/period-routing.module.ts");
/* harmony import */ var _period_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./period.page */ "./src/app/pages/period/period.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let PeriodPageModule = class PeriodPageModule {
};
PeriodPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _period_routing_module__WEBPACK_IMPORTED_MODULE_5__["PeriodPageRoutingModule"]
        ],
        declarations: [_period_page__WEBPACK_IMPORTED_MODULE_6__["PeriodPage"]]
    })
], PeriodPageModule);



/***/ }),

/***/ "./src/app/pages/period/period.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/period/period.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BlcmlvZC9wZXJpb2QucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/period/period.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/period/period.page.ts ***!
  \*********************************************/
/*! exports provided: PeriodPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeriodPage", function() { return PeriodPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_shared_service_current_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/service/current-game.service */ "./src/app/shared/service/current-game.service.ts");
/* harmony import */ var src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/model/general-settings */ "./src/app/shared/model/general-settings.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/global-function */ "./src/app/shared/global-function.ts");
/* harmony import */ var src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/service/protocol.service */ "./src/app/shared/service/protocol.service.ts");
/* harmony import */ var src_app_shared_service_debug_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/service/debug.service */ "./src/app/shared/service/debug.service.ts");








let PeriodPage = class PeriodPage {
    constructor(currentGameService, router, protocolService, debugService) {
        this.currentGameService = currentGameService;
        this.router = router;
        this.protocolService = protocolService;
        this.debugService = debugService;
        this.classame = this.constructor.name;
    }
    ngOnInit() {
        this.subscription = this.currentGameService.getdata().subscribe(data => {
            //debugger;
            this.currentGame = data;
        });
        this.debugService.getdata().subscribe((data) => { this.debugSettings = data; });
        // this.subscription = this.currentGameService.GameChanges.subscribe(data =>
        // {  
        //   this.currentGame = data;        
        // });
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    ionViewWillEnter() {
        // nicht klar, ob noch 
        this.currentGame = this.currentGameService.currentGame;
        console.log(`${this.classame}  ionViewDidEnter`);
        if (this.currentGameService.currentGame.period == 0) {
            this.router.navigate(['settings-advanced']);
        }
    }
    isFootballNormal() {
        var _a, _b;
        return (((_a = this.currentGame) === null || _a === void 0 ? void 0 : _a.gameType) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["GameType"].Football && ((_b = this.currentGame) === null || _b === void 0 ? void 0 : _b.timePeriod) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal);
    }
    isFootballReneval() {
        var _a, _b;
        return (((_a = this.currentGame) === null || _a === void 0 ? void 0 : _a.gameType) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["GameType"].Football && ((_b = this.currentGame) === null || _b === void 0 ? void 0 : _b.timePeriod) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].renewal);
    }
    isHockey() {
        var _a, _b;
        return (((_b = (_a = this.currentGameService) === null || _a === void 0 ? void 0 : _a.currentGame) === null || _b === void 0 ? void 0 : _b.gameType) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["GameType"].Hockey);
    }
    getPeriodNumber() {
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.currentGame)) {
            return 0;
        }
        if (this.currentGame.timePeriod == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal) {
            return this.currentGame.period;
        }
        else {
            return (this.currentGame.period - this.currentGame.regularTimeNumPeriods);
        }
    }
    getTotalPeriods() {
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.currentGame)) {
            return 0;
        }
        if (this.currentGame.timePeriod == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal) {
            return this.currentGame.regularTimeNumPeriods;
        }
        else {
            return this.currentGame.extraTimeNumPeriods;
        }
    }
    getMinPerPeriod() {
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.currentGame)) {
            return 0;
        }
        if (this.currentGame.timePeriod == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal) {
            return this.currentGame.regularTimeTargetMinutes;
        }
        else {
            return this.currentGame.extraTimeTargetMinutes;
        }
    }
    getEndMinPerGame() {
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.currentGame)) {
            return 0;
        }
        if (this.currentGame.timerDirection == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimerDirection"].Backward) {
            return (0);
        }
        else if (this.currentGame.sumPeriods == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["SumPeriods"].Yes) {
            if (this.currentGame.timePeriod == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal) {
                //let test = this.getPeriodNumber();
                // todo: noch nicht ganz korret. Es wird jeweils Ende der letzten Periode angezeigt.
                return (this.currentGame.regularTimeTargetMinutes * this.getPeriodNumber());
            }
            else {
                return (this.currentGame.extraTimeTargetMinutes * this.getPeriodNumber());
            }
        }
        else {
            if (this.currentGame.timePeriod == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal) {
                return this.currentGame.regularTimeTargetMinutes;
            }
            else {
                return this.currentGame.extraTimeTargetMinutes;
            }
        }
    }
    periodAdd() {
        //if this.currentGame.period 
        var _a;
        let maxPeriods = this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods;
        // max Perioden mit verlängerung erreicht
        if (this.currentGame.period >= maxPeriods) {
            return;
        }
        this.currentGame.currentSec = 0;
        this.currentGame.period++;
        if (this.currentGame.period > this.currentGame.regularTimeNumPeriods) {
            this.currentGame.timePeriod = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].renewal;
        }
        else {
            this.currentGame.timePeriod = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal;
        }
        if (this.currentGame.timerDirection == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimerDirection"].Forward) {
            this.currentGame.currentMinutes = 0;
        }
        else {
            if (((_a = this.currentGame) === null || _a === void 0 ? void 0 : _a.timePeriod) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].renewal) {
                this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
            }
            else {
                this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
            }
        }
    }
    periodSubstract() {
        var _a;
        if (this.currentGame.period < 1) {
            return;
        }
        this.currentGame.period--;
        this.currentGame.currentSec = 0;
        if (this.currentGame.period <= this.currentGame.regularTimeNumPeriods) {
            this.currentGame.timePeriod = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal;
        }
        if (this.currentGame.timerDirection == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimerDirection"].Forward) {
            this.currentGame.currentMinutes = 0;
        }
        else {
            if (((_a = this.currentGame) === null || _a === void 0 ? void 0 : _a.timePeriod) == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].renewal) {
                this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
            }
            else {
                this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
            }
        }
    }
    goBack() {
        this.router.navigate(['play']);
    }
    periodAddDisabled() {
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.currentGame)) {
            return true;
        }
        let maxPeriods = 0;
        if (this.currentGame.extraTimeAvailable == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["ExtraTimeAvailable"].No) {
            maxPeriods = this.currentGame.regularTimeNumPeriods;
        }
        else {
            maxPeriods = this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods;
        }
        if (this.currentGame.period >= maxPeriods) {
            return true;
        }
        return false;
    }
    periodSubstractDisabled() {
        var _a;
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.currentGame)) {
            return true;
        }
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty((_a = this.currentGame) === null || _a === void 0 ? void 0 : _a.period)) {
            return true;
        }
        return (this.currentGame.period <= 1);
    }
    // 
    getSportReleateMinutes() {
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty(this.currentGame)) {
            return (0);
        }
        return (this.currentGameService.getSportReleateMinutesValues(this.currentGame));
    }
    minuteSubstractDisabled() {
        var _a;
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty((_a = this.currentGame) === null || _a === void 0 ? void 0 : _a.currentMinutes)) {
            return (true);
        }
        return (false);
    }
    secDisabled() {
        var _a;
        if (src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].isEmpty((_a = this.currentGame) === null || _a === void 0 ? void 0 : _a.currentSec)) {
            return (true);
        }
        if (this.currentGame.timePeriod == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal) {
            if (this.currentGame.currentMinutes >= this.currentGame.regularTimeTargetMinutes) {
                return true;
            }
        }
        else {
            if (this.currentGame.currentMinutes >= this.currentGame.extraTimeTargetMinutes) {
                return true;
            }
        }
        return false;
        //return (this.currentGame?.currentSec == 0)
    }
    // wenn max ueberschritten, wieder start bei 0. 
    minuteAdd() {
        // 
        if (this.currentGame.timePeriod == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal) {
            if (this.currentGame.currentMinutes < this.currentGame.regularTimeTargetMinutes) {
                this.currentGame.currentMinutes++;
                if (this.currentGame.currentMinutes >= this.currentGame.regularTimeTargetMinutes) {
                    this.currentGame.currentSec = 0;
                }
                //
            }
            else { // erneut starten bei 0
                this.currentGame.currentMinutes = 0;
                this.currentGame.currentSec = 0;
            }
        }
        else { // verlängerung
            if (this.currentGame.currentMinutes < this.currentGame.extraTimeTargetMinutes) {
                this.currentGame.currentMinutes++;
                if (this.currentGame.currentMinutes >= this.currentGame.extraTimeTargetMinutes) {
                    this.currentGame.currentSec = 0;
                }
            }
            else {
                this.currentGame.currentMinutes = 0;
                this.currentGame.currentSec = 0;
            }
        }
    }
    minuteSubstract() {
        if (this.currentGame.currentMinutes > 0) {
            this.currentGame.currentMinutes--;
        }
        else {
            if (this.currentGame.timePeriod == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal) {
                this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
                this.currentGame.currentSec = 0;
            }
            else {
                this.currentGame.currentSec = 0;
                this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
            }
        }
    }
    secondAdd() {
        // wenn Endspielzeit erreicht vorwärts, dann werden die sekunden nicht mehr hochgezählt z.B 45:00 max wert. 
        if (this.currentGame.timerDirection == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimerDirection"].Forward) {
            if (this.currentGame.timePeriod == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal) {
                if (this.currentGame.currentMinutes >= this.currentGame.regularTimeTargetMinutes) {
                    return;
                }
            }
            else {
                if (this.currentGame.currentMinutes >= this.currentGame.extraTimeTargetMinutes) {
                    return;
                }
            }
        }
        if (this.currentGame.currentSec < 59) {
            this.currentGame.currentSec++;
        }
        else {
            this.currentGame.currentSec = 0;
        }
    }
    secondSubstract() {
        // moeglichkeit von 
        if (this.currentGame.currentSec > 0) {
            this.currentGame.currentSec--;
        }
        else {
            if (this.currentGame.timePeriod == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal) {
                if (this.currentGame.currentMinutes >= this.currentGame.regularTimeTargetMinutes) {
                    return;
                }
            }
            else {
                if (this.currentGame.currentMinutes >= this.currentGame.extraTimeTargetMinutes) {
                    return;
                }
            }
            this.currentGame.currentSec = 59;
        }
    }
    save() {
        // ausganssituation für die einstellung
        this.currentGame.gameStatus = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["GameStatus"].pause;
        let relatedPeriod = 0;
        let relatedMinutes = 0;
        if (this.currentGame.period > this.currentGame.regularTimeNumPeriods) {
            this.currentGame.timePeriod = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].renewal;
            relatedPeriod = this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods;
            relatedMinutes = this.currentGame.extraTimeTargetMinutes;
        }
        else {
            this.currentGame.timePeriod = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimePeriod"].normal;
            relatedPeriod = this.currentGame.regularTimeNumPeriods;
            relatedMinutes = this.currentGame.regularTimeTargetMinutes;
        }
        if (this.currentGame.timerDirection == src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["TimerDirection"].Backward) {
            if ((this.currentGame.currentSec == 0) && (this.currentGame.currentMinutes == 0)) {
                debugger;
                if (this.currentGame.period == relatedPeriod) {
                    this.currentGame.gameStatus = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["GameStatus"].gameFinished;
                }
                else {
                    this.currentGame.gameStatus = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["GameStatus"].periodFinished;
                }
            }
        }
        else { // forward
            if ((this.currentGame.currentSec == 0) && (this.currentGame.currentMinutes == relatedMinutes)) {
                if (this.currentGame.period == relatedPeriod) {
                    this.currentGame.gameStatus = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["GameStatus"].gameFinished;
                }
                else {
                    this.currentGame.gameStatus = src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["GameStatus"].periodFinished;
                }
            }
        }
        //gegebenfalls nicht mehr notwendig.
        this.currentGameService.changeCurrentGameWithDependencies(this.currentGame);
        // ändern des Protokolls der sekündlchen Zeitübertragung
        // erst mit lokaler variable arbeiten
        let setting21H = new src_app_shared_model_general_settings__WEBPACK_IMPORTED_MODULE_3__["Setting21H"]();
        // Übernahme der bestehenden Einstellungen
        if (this.debugSettings.debugProtokolWorkflow) {
            debugger;
        }
        setting21H.flagMid = this.protocolService.setting21H.flagMid;
        setting21H.signalEnd = this.protocolService.setting21H.signalEnd;
        setting21H = src_app_shared_global_function__WEBPACK_IMPORTED_MODULE_5__["GlobalFunction"].setSettings21HFromCurrentGame(this.currentGame, setting21H);
        // Damit müssten die Werte bei der nächsten Protokollübertragung aktualisiert werden
        this.protocolService.setting21H = setting21H;
        // // senden der Periode. num ausgelagert
        // // beim Fussball vermutlich keine Bedetung.
        // // Wird aber trotzdem übertragen.
        // let setting22H = new Setting22H();
        // setting22H.period = this.currentGame.period.toString();
        // this.protocolService.prot22h(setting22H);
        this.router.navigate(['play']);
        // check if status of game should be changed to 
    }
    cancel() {
        this.currentGame = this.currentGameService.currentGame;
        this.router.navigate(['play']);
    }
};
PeriodPage.ctorParameters = () => [
    { type: src_app_shared_service_current_game_service__WEBPACK_IMPORTED_MODULE_2__["CurrentGameService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_shared_service_protocol_service__WEBPACK_IMPORTED_MODULE_6__["ProtocolService"] },
    { type: src_app_shared_service_debug_service__WEBPACK_IMPORTED_MODULE_7__["DebugService"] }
];
PeriodPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-period',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./period.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/period/period.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./period.page.scss */ "./src/app/pages/period/period.page.scss")).default]
    })
], PeriodPage);



/***/ })

}]);
//# sourceMappingURL=pages-period-period-module-es2015.js.map