import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';



const routes: Routes = [
  {
    path: '',
    redirectTo: 'settings',
    pathMatch: 'full'
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'play',
    loadChildren: () => import('./pages/play/play.module').then( m => m.PlayPageModule)
  },
  {
    path: 'period',
    loadChildren: () => import('./pages/period/period.module').then( m => m.PeriodPageModule)
  },
  {
    path: 'team-names',
    loadChildren: () => import('./pages/team-names/team-names.module').then( m => m.TeamNamesPageModule)
  },
  {
    path: 'settings-advanced',
    loadChildren: () => import('./pages/settings-advanced/settings-advanced.module').then( m => m.SettingsAdvancedPageModule)
  },
  {
    path: 'bluetooth',
    loadChildren: () => import('./pages/bluetooth/bluetooth.module').then( m => m.BluetoothPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
