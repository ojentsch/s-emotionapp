import { Injectable } from '@angular/core';
import { PeriodSettings, DebugSettings } from '../model/general-settings';
import { Observable, of } from 'rxjs';
import { DataStorageService } from './data-storage.service';
import { GlobalFunction } from '../global-function';
import { DebugService } from './debug.service';

@Injectable({
  providedIn: 'root'
})
export class HockeyService {

  public hockeySettings:PeriodSettings[] = [];
  public hockeySettingsExtraTime:PeriodSettings[] = []
  public newSettingId: number;
  private debugSettings:DebugSettings;

  constructor(
    private dataStorageService: DataStorageService,
    private debugService:DebugService,
  ) {
    this.getBasicSettings();

    this.debugService.getdata().subscribe(
      (data) => {this.debugSettings = data;}
    );
  }

  // Initialisieren der Werte: werden in den Local Storage geschrieben. 
  initBasicSettingsFromConfig(){
    fetch('./assets/data/hockeySettings.json').then(res => res.json())
    .then(json => {
      this.hockeySettings = json;
      let jsonString = JSON.stringify(this.hockeySettings);
      this.dataStorageService.setString('hockeySettings', jsonString);
    });
  }

  getdata(): Observable<PeriodSettings[]>{
    return of (this.hockeySettings);
  }

  getBasicSettings() {
    this.dataStorageService.getString('hockeySettings').then((data: any) => {
      if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
        this.hockeySettings = JSON.parse(data.value);

        if (this.debugSettings.displayJson){
          console.log(this.hockeySettings);
        }
      } else {
        this.initBasicSettingsFromConfig();
      }
    });
  }

  deleteCustomConfiguration(){
    this.dataStorageService.removeItem("hockeySettings");
  }

  deleteSetting(settingID: number){

    let index = this.hockeySettings.findIndex(x => x.periodSettingId == settingID);
    
    if (index == -1){
      return
    }
    
    this.hockeySettings.splice(index, 1);

    // alten Konfigurationswert überschreiben
    let jsonStr = JSON.stringify(this.hockeySettings);
    this.dataStorageService.setString('hockeySettings', jsonStr);
  }

  addNewNormalPeriod(periodSetting: PeriodSettings){
    
    this.newSettingId = Math.max(...this.hockeySettings.map(o => o.periodSettingId)) + 1;

    periodSetting.periodSettingId = this.newSettingId;
    this.hockeySettings.push(periodSetting);

    // alten Konfigurationswert überschreiben
    let jsonStr = JSON.stringify(this.hockeySettings);
    this.dataStorageService.setString('hockeySettings', jsonStr);

  }
}
