import { TestBed } from '@angular/core/testing';

import { BluetoothTimerService } from './bluetooth-timer.service';

describe('BluetoothTimerService', () => {
  let service: BluetoothTimerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BluetoothTimerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
