import { Injectable } from '@angular/core';
import { CurrentGameService } from './current-game.service';
import { ProtocolService } from './protocol.service';
import { Setting21H } from '../model/general-settings';
import { GlobalFunction } from '../global-function';

@Injectable({
  providedIn: 'root'
})
export class GameTimeCounterService {

  public IntervalTimer;

  constructor(
    private currentGameService:CurrentGameService,
    private protocolService:ProtocolService,
  ) {

  }

clearTimer(){
  clearInterval(this.IntervalTimer)

}

  SetIntervalTimer(){

    this.IntervalTimer = setInterval(() => {

      this.currentGameService.tick();
      let setting21H = new Setting21H();
      setting21H.flagMid = ProtocolService.setting21GameIsRunningFlagASCII; 
      setting21H = GlobalFunction.setSettings21HFromCurrentGame(this.currentGameService.currentGame, setting21H)
      
      // damit müsste das Protokoll aktualisiert werden
      this.protocolService.setting21H = setting21H;
    },1000)

  }

}
