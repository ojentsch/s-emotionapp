import { Injectable } from '@angular/core';
import { DebugSettings } from '../model/general-settings';
import { Observable, of } from 'rxjs';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@Injectable({
  providedIn: 'root'
})
export class DebugService {


  public debugSettings:DebugSettings;
  constructor() { 
    this.debugSettings = new DebugSettings();
  }

  getdata(): Observable<DebugSettings>{
    return of (this.debugSettings);
  }

}
