import { Injectable } from '@angular/core';
import { PeriodSettings, ExtraTimeSettings, DebugSettings } from '../model/general-settings';
import { Observable, of, Subject } from 'rxjs';
import { DataStorageService } from './data-storage.service';
import { GlobalFunction } from '../global-function';
import { DebugService } from './debug.service';

// https://edupala.com/ionic-capacitor-storage/
@Injectable({
  providedIn: 'root'
})
export class FootballService {

  public footballSettings:PeriodSettings[] = [];
  public footballSettingsFiltered:PeriodSettings[] = [];
  public footballSettingsExtraTime:ExtraTimeSettings[] = []
  public newSettingId: number;
  private debugSettings:DebugSettings;
  private newExraTimeSettingId :number;

  constructor(
    private dataStorageService: DataStorageService,
    private debugService:DebugService,
  ) { 

    this.getFootballSettings();
    this.getFootballSettingsExtraTime();

    this.debugService.getdata().subscribe(
      (data) => {this.debugSettings = data;}
    );
  }

  // Initialisieren der Werte, werden in den Local Storage geschrieben. 
  initBasicSettingsFromConfig(){
    fetch('./assets/data/footballSettings.json').then(res => res.json())
    .then(json => {
      this.footballSettings = json;
      let jsonString = JSON.stringify(this.footballSettings);
      this.dataStorageService.setString('footballSettings', jsonString);
    });
  }

  // Initialisieren der Werte für die Verlängerung, werden in den Local Storage geschrieben. 
  initExtraSettingsFromConfig(){
    fetch('./assets/data/footballSettingsExtraTime.json').then(res => res.json())
    .then(json => {
      this.footballSettingsExtraTime = json;
      let jsonString = JSON.stringify(this.footballSettingsExtraTime);

      
      this.dataStorageService.setString('footballSettingsExtraTime', jsonString);
    });
  }

  getFootballSettings() {
    this.dataStorageService.getString('footballSettings').then((data: any) => {
      if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
        this.footballSettings = JSON.parse(data.value);
        //this.filterdata();
        if (this.debugSettings.displayJson){
          console.log(this.footballSettings);
        }
      } else {
        this.initBasicSettingsFromConfig();
      }
    });
  }

  getFootballSettingsExtraTime() {
    this.dataStorageService.getString('footballSettingsExtraTime').then((data: any) => {
      if ((!GlobalFunction.isEmpty(data) && data.value != null)) {


        this.footballSettingsExtraTime = JSON.parse(data.value);
        if (this.debugSettings.displayJson){
          console.log(this.footballSettingsExtraTime);
        }
      } else {
        this.initExtraSettingsFromConfig();
      }
    });
  }

  // currently not used, but filterint the data with search would be possible.
  filterdata(){
 
      let filterSettings1 = "U 11";
      let filterSettings2 = "U 12";
      this.footballSettingsFiltered = this.footballSettings.filter(x => x.settingName.indexOf(filterSettings1) !== -1 
      || x.settingName.indexOf(filterSettings2) !== -1 );

  }


  getdata(): Observable<PeriodSettings[]>{
    return of (this.footballSettings);
  }

  deleteCustomConfiguration(){
    this.dataStorageService.removeItem("footballSettings");
    this.dataStorageService.removeItem("footballSettingsExtraTime");
  }

  deleteSetting(settingID: number){


    // elemente aus dem Array entfernen.
    let index = this.footballSettings.findIndex(x => x.periodSettingId == settingID);
    if (index == -1){
      return
    }
    
    this.footballSettings.splice(index, 1);

    // alten Konfigurationswerte überschreiben
    let jsonStr = JSON.stringify(this.footballSettings);
    this.dataStorageService.setString('footballSettings', jsonStr);


    // ggf. Verlängerungen ebenfalls löschen 
    let indexExtraTime = this.footballSettingsExtraTime.findIndex(x => x.relatedSettingId == settingID); 
    if (indexExtraTime != -1){
      this.footballSettingsExtraTime.splice(indexExtraTime, 1);
      jsonStr = JSON.stringify(this.footballSettingsExtraTime);
      this.dataStorageService.setString('footballSettingsExtraTime', jsonStr);
    }
  }

  addNewNormalPeriod(footballSetting: PeriodSettings){
    
    this.newSettingId = Math.max(...this.footballSettings.map(o => o.periodSettingId)) + 1;
    //footballSetting.settingName = footballSetting.durationMin.toString();
    footballSetting.periodSettingId = this.newSettingId;
    this.footballSettings.push(footballSetting);


    // alten Konfigurationswert überschreiben
    let jsonStr = JSON.stringify(this.footballSettings);
    this.dataStorageService.setString('footballSettings', jsonStr);

  }


  addNewExtraPeriod(footballExtraTimeSetting: ExtraTimeSettings){
    
    this.newExraTimeSettingId = Math.max(...this.footballSettingsExtraTime.map(o => o.extraTimeSettingId)) + 1;
    footballExtraTimeSetting.relatedSettingId = this.newSettingId;
    footballExtraTimeSetting.extraTimeSettingId = this.newExraTimeSettingId;
    this.footballSettingsExtraTime.push(footballExtraTimeSetting);


    // alten Konfigurationswert überschreiben
    let jsonString = JSON.stringify(this.footballSettingsExtraTime);
    this.dataStorageService.setString('footballSettingsExtraTime', jsonString);

  }
}





