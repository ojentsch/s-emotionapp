import { Injectable } from '@angular/core';
import { CurrentGame,GameType, TimerDirection, GameStatus, TimePeriod, PeriodSettings, SumPeriods, ExtraTimeAvailable, DebugSettings, Setting21H, TeamNames } from '../model/general-settings';
import { of, Observable, Subject } from 'rxjs';
import { FootballService } from './football.service';
import { HockeyService } from './hockey.service';
import { GlobalFunction } from '../global-function';
import { ProtocolService } from './protocol.service';
import { SettingsService } from './settings.service';

@Injectable({
  providedIn: 'root'
})

export class CurrentGameService {

  private debugSettings:DebugSettings;
  public currentGame:CurrentGame;
  GameChanges = new Subject<CurrentGame>();

  
  constructor(
    private footballService:FootballService,
    private hockeyService:HockeyService,
    private protocolService:ProtocolService,
    private settingsService:SettingsService
  ) {

    if (this.currentGame == null){
        this.currentGame = new CurrentGame();
    }
   }

   private changeHandler(){
    this.GameChanges.next(this.currentGame);

   }

   private setTeamNames(){
    this.currentGame.homeTeamName = this.settingsService.teamNames.homeTeamName;
    this.currentGame.guestTeamName = this.settingsService.teamNames.guestTeamName;
   }

    // Verlängerung hockey bisher nicht eingeplant.
    private internHockeySetter(hockeySetting: PeriodSettings){

      if ( !GlobalFunction.isEmpty(hockeySetting)){
  
        this.currentGame.regularTimeTargetMinutes = 0;
        this.currentGame.periodMinutes = hockeySetting.durationMin;
        this.currentGame.currentMinutes = hockeySetting.durationMin;   
        this.currentGame.regularTimeTargetMinutes = hockeySetting.durationMin; 
        this.currentGame.regularTimeNumPeriods = hockeySetting.periodAmounts;  
        this.currentGame.extraTimeTargetMinutes = 0; 
        this.currentGame.extraTimeNumPeriods = 0;
        this.currentGame.extraTimeAvailable = ExtraTimeAvailable.No;
        this.currentGame.gameStatus  = GameStatus.init;
        this.setTeamNames();
      }
    }


    private internFootballSetter(footballSetting: PeriodSettings, periodSettingId: number){
     
      this.currentGame.periodMinutes = footballSetting.durationMin;
      this.currentGame.targetMinutes = footballSetting.durationMin;
      this.currentGame.regularTimeTargetMinutes = footballSetting.durationMin; 
      this.currentGame.regularTimeNumPeriods = footballSetting.periodAmounts; 
      this.setTeamNames();

      // zeiten fuer die moegliche verlanderung schon zu beginn des Spiels laden
      let extraTime = this.footballService.footballSettingsExtraTime.find(x => x.relatedSettingId == periodSettingId); 
      
      if (!GlobalFunction.isEmpty(extraTime)){
        this.currentGame.extraTimeAvailable = ExtraTimeAvailable.Yes;
        this.currentGame.extraTimeTargetMinutes = extraTime.durationMin; 
        this.currentGame.extraTimeNumPeriods = extraTime.periodAmounts; 
      }
      
    }


    getdata():Observable<CurrentGame>{
      return  of (this.currentGame);
    }
  
   initHockeyValues(){

    this.currentGame.sumPeriods = SumPeriods.No;
    this.currentGame.gameType = GameType.Hockey;
    this.currentGame.period = 1;
   
    this.currentGame.gameStatus  = GameStatus.init;
    this.currentGame.timerDirection = TimerDirection.Backward;
    this.currentGame.timePeriod = TimePeriod.normal;
    this.currentGame.currentSec = 0;
    this.currentGame.homeGoals = 0;
    this.currentGame.guestGoals = 0;

    // es ist nur eine Spielzeit vorgesehen. Daher wird direkt initialisiert. 
    if (this.hockeyService.hockeySettings.length == 1){

      let hockeySetting = this.hockeyService.hockeySettings[0];
      this.internHockeySetter(hockeySetting);

    }

    this.changeHandler();
   }

   // noch nicht getstet. bei meheren Spielzeiten für hockey relevant
   initHockeyDetailValues(periodSettingId: number){
    
    let hockeySetting = this.hockeyService.hockeySettings.find(x => x.periodSettingId == periodSettingId);   
    this.internHockeySetter(hockeySetting);

   }


   initFootballValues(){

    this.currentGame = new CurrentGame();  
    this.currentGame.gameType = GameType.Football;  
    this.currentGame.sumPeriods = SumPeriods.Yes;
    this.changeHandler();

  }

  initFootballDetailValues(periodSettingId: number){
    
    this.currentGame.gameStatus  = GameStatus.init;
    this.currentGame.sumPeriods = SumPeriods.Yes
    this.currentGame.timePeriod = TimePeriod.normal;  
    this.currentGame.timerDirection = TimerDirection.Forward;
    this.currentGame.currentSec = 0;
    this.currentGame.currentMinutes = 0;
    this.currentGame.period = 1;
    this.currentGame.homeGoals = 0;
    this.currentGame.guestGoals = 0;
    
    let footballSetting = this.footballService.footballSettings.find(x => x.periodSettingId == periodSettingId);
    this.internFootballSetter(footballSetting,periodSettingId)

    this.changeHandler();
  }

  public hockeyPeriodFinished(){
    if (GlobalFunction.isEmpty(this.currentGame)){
      return 0;
    }

    if (this.currentGame.gameType == GameType.Hockey) {

      if ((this.currentGame.gameStatus == GameStatus.periodFinished)
      || (this.currentGame.gameStatus == GameStatus.gameFinished)
      ){
        return true;
      }
    }

    return false;

  }


  // Ausgabe in Abhängigkeit von Vorwartszähler Rückwärtszähler
  // und Ausgabe accumuiliert oder nicht.
  // nicht geprüft ob Rückwärtszähler akkumuliert ebenfalls funktioniert.
  getSportReleateMinutesValues(currentGame:CurrentGame){
  
    if (GlobalFunction.isEmpty(currentGame)){
      return 0;
    }

    let minFromPeriod = 0;
    let sumperiods = currentGame.sumPeriods;


    // akumuliert
    if (sumperiods == SumPeriods.Yes){

      if (currentGame.timePeriod == TimePeriod.normal){ //normale Spielzeit
        if (currentGame.period > 1){
          minFromPeriod =  (currentGame.period-1) * currentGame.regularTimeTargetMinutes;
          
        } 
      } else { // Verlängerung
        minFromPeriod =  (currentGame.period - 1 - currentGame.regularTimeNumPeriods) * this.currentGame.extraTimeTargetMinutes;          
      }

      let totalMin  = this.currentGame.currentMinutes + minFromPeriod;
      return (totalMin)

    }
    
    if (currentGame.currentMinutes  > 0){
        return currentGame.currentMinutes
    }

    return 0;
  }


  getSportReleateMinutesService(){

    let currentGame = this.currentGame;
    return (this.getSportReleateMinutesValues(currentGame));

  }

  // Die Anzeige wird auf Ende des Spiel gesetzt.
  // Wird zur Zeit nicht benutzt.
  setEndGameSettings(){

     this.currentGame.currentSec = 0;
     if (this.currentGame.timerDirection == TimerDirection.Backward){
       this.currentGame.currentMinutes = 0;
       this.currentGame.currentSec = 0;
       if (this.currentGame.timePeriod == TimePeriod.normal){ //normale Spielzeit
         this.currentGame.period = this.currentGame.regularTimeNumPeriods;
       } else {
         this.currentGame.period = this.currentGame.extraTimeNumPeriods;
       }
 
     } else {
       if (this.currentGame.timePeriod == TimePeriod.normal){ //normale Spielzeit
       
         this.currentGame.period = this.currentGame.regularTimeNumPeriods;
         if (this.currentGame.sumPeriods == SumPeriods.Yes){
           this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
         } else {
           this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
         }
       }  else {
         if (this.currentGame.sumPeriods == SumPeriods.Yes){
           this.currentGame.currentMinutes = this.currentGame.extraTimeNumPeriods * this.currentGame.extraTimeTargetMinutes;
         } else {
           this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
         }
       }  
     }
     
     
     this.currentGame.gameStatus = GameStatus.gameFinished;
  }

  finishGame(){  

    // Die Anzeige wird nicht verändert.
    this.currentGame.gameStatus = GameStatus.gameFinished;
    this.currentGame.currentMinutes = 0;
    this.currentGame.currentSec = 0;
    this.currentGame.period = 0;
    this.changeHandler();
 
  }

  deleteCurrentGame(){
    this.currentGame = null;
    this.changeHandler();
  }

  changeCurrentGameWithDependencies(currentGame:CurrentGame){
    
    let teamNames = new TeamNames();
    teamNames.homeTeamName = currentGame.homeTeamName;
    teamNames.guestTeamName = currentGame.guestTeamName;
    currentGame.guestTeamName;
    this.settingsService.updateTeamNames(teamNames);
    
    this.currentGame = currentGame;
    this.changeHandler();
  }


  startNextPeriod(){

    this.currentGame.period++;

    this.currentGame.currentSec = 0;
    
    if (this.currentGame.timerDirection == TimerDirection.Backward){ //rueckwarszähler z.B start bei 20:00 
      if (this.currentGame.timePeriod == TimePeriod.normal){
        this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
      } else {
        this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
      }
    } else { //vorwärts
      this.currentGame.currentMinutes = 0;
    }
   
    if (this.currentGame.period > this.currentGame.regularTimeNumPeriods){
      this.currentGame.timePeriod = TimePeriod.renewal;
      this.currentGame.targetMinutes = this.currentGame.extraTimeTargetMinutes; 
    } else {
      this.currentGame.targetMinutes = this.currentGame.regularTimeTargetMinutes;
    }

    this.currentGame.gameStatus = GameStatus.pause;
    
    // hack refresh the display 
    // not perfect from this point, but this short solution works.   
    let setting21H = new Setting21H();
    setting21H.flagMid = ProtocolService.setting21GameIsNotRunningFlagASCII;    
    setting21H.signalEnd = this.protocolService.setting21H.signalEnd;
    setting21H = GlobalFunction.setSettings21HFromCurrentGame(this.currentGame, setting21H)
    this.protocolService.setting21H = setting21H;

    this.changeHandler();
  
  };

  goalTeam1Add(){
    if (this.currentGame.homeGoals < 99){
      this.currentGame.homeGoals++;
    } else {
      this.currentGame.homeGoals = 0;
    } 
    this.changeHandler();
  }

  goalTeam2Add(){
    if (this.currentGame.guestGoals < 99){
      this.currentGame.guestGoals++;
    } else {
      this.currentGame.guestGoals = 0;
    }
    this.changeHandler();
  }

  goalTeam1Substract(){
    if (this.currentGame.homeGoals > 0){
      this.currentGame.homeGoals--;
    } else {
      this.currentGame.homeGoals = 99;
    } 
    this.changeHandler();
  }

  goalTeam2Substract(){
    if (this.currentGame.guestGoals > 0){
      this.currentGame.guestGoals--;
    } else {
      this.currentGame.guestGoals = 99;
    } 
    this.changeHandler();
  }

  // sekundenzaehler.
  tick(){

    if (this.currentGame.timerDirection == TimerDirection.Backward){

      if (this.currentGame.currentSec > 0){ //nur sekundenzähler
        this.currentGame.currentSec--;
      } else {

        if (this.currentGame.currentMinutes > 0){ //miuntenzähler
          this.currentGame.currentMinutes --;
          this.currentGame.currentSec = 59;
        } else { //Periode oder Spiel beendet
          let numTotalPeriods = 0;
          if (this.currentGame.timePeriod == TimePeriod.normal){
            numTotalPeriods = this.currentGame.regularTimeNumPeriods;
            //this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
          } else { // noch nicht getestet
            numTotalPeriods = this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods;
            //this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
          }
          
          if (this.currentGame.period < numTotalPeriods){ // Periode beendet.
            this.currentGame.gameStatus = GameStatus.periodFinished;
          } else { // Spiel oder Verlaengerung beendet
            this.currentGame.gameStatus = GameStatus.gameFinished;
          }
          this.changeHandler();
        };
        
      }

    } else { // forward fussball
      if (this.currentGame.currentSec < 59){
        this.currentGame.currentSec++;
      } else {

        this.currentGame.currentSec = 0;
        this.currentGame.currentMinutes++;

        if (   (this.currentGame.timePeriod == TimePeriod.normal) &&  (this.currentGame.currentMinutes) < this.currentGame.regularTimeTargetMinutes){
          // normale spielezeit
    
        } else if ( (this.currentGame.currentMinutes) < this.currentGame.extraTimeTargetMinutes) {
          // verlängerung
        } else { // spiel oder Verlängerung beendet


            if ((this.currentGame.period == this.currentGame.regularTimeNumPeriods) || 
            (this.currentGame.period == this.currentGame.regularTimeNumPeriods +  this.currentGame.extraTimeNumPeriods ))
            {
              // Spiel oder Verlängerung beendet.
              this.currentGame.gameStatus = GameStatus.gameFinished;

            } else {
              // Spielzeit beendet
              this.currentGame.gameStatus = GameStatus.periodFinished;
            }

            this.changeHandler();

          };
        }
    } // ende fussball
  }

}
