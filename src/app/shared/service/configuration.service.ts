import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigSettings } from '../model/general-settings';
import { Observable, of, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  public configSettings:ConfigSettings; 
  ConfigLoaded = new Subject<ConfigSettings>();
  constructor(
    ) { 

      this.getSettings();
    }

  private changeHandler(){
    this.ConfigLoaded.next(this.configSettings);
  }

  
  getSettings(){
    fetch('./assets/data/configSettings.json').then(res => res.json())
    .then(json => {
      //debugger;
      this.configSettings = json;
      this.changeHandler();
    });
  }

}
