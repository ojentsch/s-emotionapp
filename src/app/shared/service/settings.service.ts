import { Injectable } from '@angular/core';
import { Observable, of, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { GeneralSettings, TeamNames } from '../model/general-settings';
import { HttpClient } from '@angular/common/http';
import { DataStorageService } from './data-storage.service';
import { GlobalFunction } from '../global-function';
import { TranslateService } from '@ngx-translate/core';
@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  
  private subscription: Subscription;
  
  //languages: unknown;
  
  public teamNames:TeamNames = new TeamNames;
  public hasTimeTransfer:boolean = true;
  
  public generalSettings:GeneralSettings[] = []; 

  constructor(
    private dataStorageService: DataStorageService,
    private translate:TranslateService,
    ) { 
    this.getGeneralSettings();
    this.subscription = this.translate.get(['TEAM1_DEFAULT_NAME','TEAM2_DEFAULT_NAME']).subscribe((translation: [string]) => {  
      this.teamNames.homeTeamName = translation['TEAM1_DEFAULT_NAME'];
      this.teamNames.guestTeamName = translation['TEAM2_DEFAULT_NAME'];
    });

    // get values from storage
    this.dataStorageService.getString('HomeTeamName').then((data: any) => {
      if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
        this.teamNames.homeTeamName=  data.value;
      } 
    });

    this.dataStorageService.getString('GuestTeamName').then((data: any) => {
      if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
        this.teamNames.guestTeamName =  data.value;
      } 
    });

    this.dataStorageService.getString('HasTimeTransfer').then((data: any) => {
      if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
        this.hasTimeTransfer =  JSON.parse(data.value);
      } 
    });
  }

  getGeneralSettings$(): Observable<GeneralSettings[]>{
    return of (this.generalSettings);
  }

  getGeneralSettings(){
    fetch('./assets/data/generalSettings.json').then(res => res.json())
    .then(json => {
      this.generalSettings = json;
    });
  }

  updateTeamNames(teamNames:TeamNames){

    this.teamNames.guestTeamName = teamNames.guestTeamName;
    this.teamNames.homeTeamName = teamNames.homeTeamName;
    this.dataStorageService.setString('HomeTeamName', teamNames.homeTeamName);
    this.dataStorageService.setString('GuestTeamName', teamNames.guestTeamName);  

  }

  updateTimeTransfer(transferTime:boolean){

    this.hasTimeTransfer = transferTime;
    this.dataStorageService.setString('HasTimeTransfer', String(transferTime));

  }
}
