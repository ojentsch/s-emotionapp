import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class MessageService {


  duration = 3000;

  // top, bottom and middle
  public position  = "bottom";
  
  constructor(
    private toastCtrl: ToastController,
  ) { }


  async presentToasterPostionMessage(str:string, position:string) {
    
    this.position  = position;
    this.presentToasterMessage(str);
  }


  async presentToasterMiddleMessage(str:string) {
    
    const toast = await this.toastCtrl.create({
      message: str,
      duration: this.duration,
      position:"middle"
    });
    toast.present();
  }

  async presentToasterMessage(str:string) {
    const toast = await this.toastCtrl.create({
      message: str,
      duration: this.duration,
    });
    toast.present();
  }
}
