import { Injectable } from '@angular/core';
import { BluetoothService } from './bluetooth.service';
import { ConfigurationService } from './configuration.service';
import { ConfigSettings } from '../model/general-settings';
import { GlobalFunction } from '../global-function';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class BluetoothTimerService {


  // todo: subscribe on bluetooth connection lost.
  // todo: also react on refresh

  private connectTimerIsActive = false;
  
  // interval timer: how often try to connect, if disconnected.
  private connectIntervalTimer = 6000;


  private selectedDeviceID: string = "AVR-BLE_4DDA";

  private configSettings: ConfigSettings; 
  // interval für das übertragen der Zeit
  private interval;

  constructor(
    private configurationService:ConfigurationService,
    private bluetoothService:BluetoothService,
    private messageService: MessageService,
  ) { 

    this.connectTimerIsActive = false;
    this.configSettings = this.configurationService.configSettings;

  }


  getConfigSettings(){

  }
  startConnectTimer(){

    this.getConfigSettings();
    debugger;

    this.connectTimerIsActive = true;

  }

  public endConnectTimer(){
    clearInterval(this.interval);

    this.connectTimerIsActive = false;
  }

}
