import { TestBed } from '@angular/core/testing';

import { GameTimeCounterService } from './game-time-counter.service';

describe('GameTimeCounterService', () => {
  let service: GameTimeCounterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GameTimeCounterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
