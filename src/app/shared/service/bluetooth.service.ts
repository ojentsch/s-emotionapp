import { Injectable } from '@angular/core';
import { NgZone } from '@angular/core';
// es gibt zwei verschiedene BLEs

import { BLE } from '@ionic-native/ble/ngx';
//import { BLE } from '@ionic-native/ble';

import { MessageService } from './message.service';
import { GlobalFunction } from '../global-function';
import { DataStorageService } from './data-storage.service';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { ConfigurationService } from './configuration.service';
import { ConfigSettings } from '../model/general-settings';
import { Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

// Not clear if required.
import { Geolocation } from '@ionic-native/geolocation/ngx';

// ServiceIDs für AVR-BLE_4DDA und RN4871-EBAD
const serviceUUID = '49535343-fe7d-4ae5-8fa9-9fafd205e455';

// dev-kid für AVR-BLE_4DDA und RN4871-EBAD
const characteristicUUID = '49535343-1e4d-4bd9-ba61-23c647249616';

@Injectable({
  providedIn: 'root'
})



export class BluetoothService {

  private subscription: Subscription;
  private resSubscription: Subscription;
  public BluetoothAutoConnect:boolean;
  private configSettings: ConfigSettings; 
  public devices: any[] = [];
  public configSettingsbluetoothDeviceName = "";
  statusMessage: string;
  peripheral: any = {};
  public sendArray:Uint8Array = new Uint8Array();
  public selectedDeviceID: string;
  public deviceIdFromName: string;
  public selectedDeviceName: string;

  public testBle: any[]
  public BluetoothDevicesChanges = new Subject<any[]>();
  public changeBluetoothHandler(){
    this.BluetoothDevicesChanges.next(this.devices);
  }

  public IsConnected:boolean;
  ConnectionChanges = new Subject<boolean>();

  private conClosedMes:string;
  private conEstablishedMes:string;
  private conTryEstablishConMes:string;
  private scanOnDeviceNotPossibleMes:string;
  private sendStrDebugMes:string;
  private bluetoothDisconnectedMes:string;

  private interval;
  private intervalTimer = 3000;
  
  
  public changeHandler(){
    this.ConnectionChanges.next(this.IsConnected);
    if  (!GlobalFunction.isEmpty(this.selectedDeviceID)){
      if (this.IsConnected){
        this.clearConnectionIntervalTimer();
      }
    } 
   }

  constructor(
    private configurationService:ConfigurationService,
    private ble: BLE,
    private ngZone: NgZone,
    private messageService: MessageService,
    private dataStorageService: DataStorageService,
    private translate:TranslateService,
    public platform: Platform,

    ) {
      

      this.IsConnected = false;
      this.selectedDeviceID = "";

      // handle the device name
      this.subscription = this.configurationService.ConfigLoaded.subscribe(data =>
        {       

          // First load from config
          this.configSettings = data;

          if (!GlobalFunction.isEmpty(this.configSettings.bluetoothDeviceName)) {
            
            this.configSettingsbluetoothDeviceName = this.configSettings.bluetoothDeviceName;
            this.selectedDeviceName = this.configSettings.bluetoothDeviceName;
          }

          this.dataStorageService.getString('deviceName').then((data: any) => {
            if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
              this.selectedDeviceName = data.value;
            }
          });
        });

        this.dataStorageService.getString('BluetoothAutoConnect').then((data: any) => {
          if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
            this.BluetoothAutoConnect = JSON.parse(data.value);
          }
        });


      // use session connection if there is something else from the last connection.
      this.dataStorageService.getString('deviceId').then((data: any) => {
        if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
          this.selectedDeviceID = data.value;

          this.dataStorageService.getString('BluetoothAutoConnect').then((data: any) => {
            if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
              let autoconnect = JSON.parse(data.value);
              if (autoconnect){
                // connect to device if already stored.
                this.connect(this.selectedDeviceID);
              }
            }
          });
        }
      });

        

      this.resSubscription = this.translate.get(['CONNECTION_CLOSED_MESSAGE','BLUETOOTH_CONNECTION_ESTABLISHED','BLUETOOTH_TRY_ESTABLISH_CONNECTION','BLUETOOTH_SCAN_ON_DEVICE_NOT_POSSIBLE','BLUETOOTH_DISPLAY_SEND_STR_DEBUG','BLUETOOTH_UNEXPECTED_DISCONNECTED']).subscribe((translation: [string]) => {  
        this.conClosedMes = translation['CONNECTION_CLOSED_MESSAGE'];
        this.conEstablishedMes = translation['BLUETOOTH_CONNECTION_ESTABLISHED'];
        this.conTryEstablishConMes = translation['BLUETOOTH_TRY_ESTABLISH_CONNECTION'];
        this.scanOnDeviceNotPossibleMes = translation['BLUETOOTH_SCAN_ON_DEVICE_NOT_POSSIBLE'];
        this.sendStrDebugMes = translation['BLUETOOTH_DISPLAY_SEND_STR_DEBUG'];      
        this.bluetoothDisconnectedMes = translation['BLUETOOTH_UNEXPECTED_DISCONNECTED'];  
      });
   }


  setBlueToothAutoConnect(autoConnect:boolean){
    let strAutoConnect = autoConnect? "true":"false";

    this.BluetoothAutoConnect = autoConnect;
    this.dataStorageService.setString('BluetoothAutoConnect', strAutoConnect);
  }

  ngOnDestroy(){  
    this.subscription.unsubscribe();
    this.resSubscription.unsubscribe();
  }

  setConnectionIntervalTimer(){
    this.clearConnectionIntervalTimer();
    this.interval = setInterval(() => {
      this.connect(this.selectedDeviceID);
    },this.intervalTimer)
   
  }

  clearConnectionIntervalTimer(){
    clearInterval(this.interval);
  }


  scan() {
  
    this.devices = [];  // clear list
    this.ble.scan([], 30).subscribe(
      device => this.onDeviceDiscovered(device), 
      error => this.scanError(error)
    );

    //setTimeout(this.setStatus.bind(this), 5000, 'Scan complete');

  }

  disconnect(){
    this.ble.disconnect(this.selectedDeviceID).then(
      () => this.messageService.presentToasterMessage(this.conClosedMes),
      e => this.messageService.presentToasterMessage('Fehler: Verbindung konnte nicht beendet werden.')
    );
    this.IsConnected = false;
    this.changeHandler();
  }

  onDeviceDiscovered(device) {
 
    console.log('Gefunden:' + JSON.stringify(device, null, 2));
    this.ngZone.run(() => {
      
      this.devices.push(device);

      // todo: not clear if requred 
      this.changeBluetoothHandler();

    });
  }

   async scanError(error) {
    this.setStatus('Error ' + error);

    this.messageService.presentToasterMessage(this.scanOnDeviceNotPossibleMes);
  }

  setStatus(message) {
    console.log(message);
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }

  deviceSelected(device) {
    //console.log(JSON.stringify(device) + ' selected');

  }

  connect(deviceId){

    if (this.conTryEstablishConMes !== "") {
      this.messageService.presentToasterMessage(this.conTryEstablishConMes);
    }
    
    // This is not a promise, the device can call disconnect after it connects, so it's an observable
    this.ble.connect(deviceId).subscribe(
      peripheral => this.connectCallback(peripheral),
      () => this.disconnectCallback()
    );
  }

  // no error handling here as we just want to sent when there is a connection.
  sent(str) {
    //this.messageService.presentToasterMessage('Send if connected:' + str);
    this.sentMessage(str)
  }

  sentIfConnected(str) {
    if (this.IsConnected){
      //this.messageService.presentToasterMessage('Send if connected:' + str);
      this.ble.isConnected(this.selectedDeviceID).then(
        (e) => this.sentMessage(str)
      );
    }
  }

  sentMessage(str){
      let byteArray = GlobalFunction.stringToBytes(str); 
      this.ble.write(this.selectedDeviceID, serviceUUID, characteristicUUID, byteArray.buffer).then(
        (e) => this.bleWriteCallback(str),
        e => this.bleWriteErrorCallback(str)
      );
  }

  bleWriteCallback(str:string){

    if (!GlobalFunction.isEmpty(this.sendStrDebugMes)){
      this.messageService.presentToasterMessage(this.sendStrDebugMes + str);
    }
  }
  
  bleWriteErrorCallback(str:string){   
    if (!GlobalFunction.isEmpty(this.sendStrDebugMes)){
      this.messageService.presentToasterMessage("Fehler: " + str + " wurde nicht übertragen.");
    }
  }

  // Verbindung wird aufgebaut.
  connectCallback(peripheral) {
    this.selectedDeviceID = peripheral.id;
    this.dataStorageService.setString('deviceId', peripheral.id);
    this.peripheral = peripheral;

    // Devicename festlegen.
    if (!GlobalFunction.isEmpty(peripheral.Name)){
      this.dataStorageService.setString('deviceName', peripheral.Name);
      this.selectedDeviceName = peripheral.Name;
    } else {

      let device = this.devices.find(x => x.id == peripheral.id);
      if (!GlobalFunction.isEmpty(device)){
        if ((!GlobalFunction.isEmpty(device.name))  && (device.name !== undefined) ){
          this.dataStorageService.setString('deviceName', device.name);
          this.selectedDeviceName = device.name;
        
        } else if (!GlobalFunction.isEmpty(device.advertising)){
          this.dataStorageService.setString('deviceName', device.advertising);
          this.selectedDeviceName = device.advertising;
        }   
      }
    }

    // Verbindungsnachricht
    if (this.conEstablishedMes != ""){
      this.messageService.presentToasterMessage(this.conEstablishedMes + this.selectedDeviceName);
    }

    this.IsConnected = true;
    this.changeHandler(); 
    
    // Timer zum Verbingunsaufbau löschen.
    this.clearConnectionIntervalTimer();
  }

  // Die Verbindung kann aus verschiedenen Gründen unterbrochen werden.
  // Entfernung zu weit
  // Verbindung kann erst gar nicht aufgebaut werden.
  disconnectCallback() {
    this.messageService.presentToasterMessage(this.bluetoothDisconnectedMes);
    this.IsConnected = false;
    this.changeHandler();

    // sobald die Verbindung nicht aufgebaut werden kann startet ein Timer der versucht die Verbindung herzustellen
    this.setConnectionIntervalTimer();

  }

}
