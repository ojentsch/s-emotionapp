import { Injectable } from '@angular/core';
import { TeamNames,Setting21H,Setting22H,DecodeValues, DebugSettings } from '../model/general-settings';
import { GlobalFunction } from '../global-function';
import { DebugService } from './debug.service';
import { BluetoothService } from './bluetooth.service';
import { Subscription } from 'rxjs';
import { SettingsService } from './settings.service';

const SETTING21_GAME_SIGNAL_ENDE_Off_FLAG_ASCII = "0";
const SETTING21_GAME_SIGNAL_ENDE_ON_FLAG_ASCII = "1";
const SETTING22_Pfeil = " ";
const SETTING22_FLAG_LINKS = " ";
const SETTING22_FLAG_RECHTS = " ";


@Injectable({
  providedIn: 'root'
})


export class ProtocolService {


  subscription:Subscription;

  private debugSettings:DebugSettings;

  // Verbindung zur Anzeige
  public isConnected:boolean=false;

  // Bestimmt die Decodierung
  private decodeCounter = 0;

  // interval für das übertragen der Zeit
  private intervalTimer;

  // steuert die Zeitübertragung jede Sekunde
  private interval = 1000;

  // Erste Zeichen, das übertragen wird: ASCII STX
  private startByte = 0x2;

   // Letzete Zeichen, das übertragen wird: ASCII ETX
  private endByte= 0x3;

  // Uhr aktiv / nicht aktiv
  private setting20HUhrAktiv = "1"; // hex 31
  private setting20HUhrNichtAktiv = "0"; // hex 30

  // Zeahler senden Hupensignal 
  private signalSentCounter = 0;

  // die 1/10 Sekunden werden nicht übertragen.
  private setting21SecDiv10FlagASCII = " ";

  // zwei mögliche Einstellungen: Spiel läuft oder läuft nicht.
  public static setting21GameIsRunningFlagASCII = "1"; //hex 31
  public static setting21GameIsNotRunningFlagASCII = "2"; // hex 32

  // Stuerung der Hupe
  public static setting21GameSigEndeOnFlagASCII = "30";

  // zeigt an, ob die sekündliche Übertragung der Zeit aktiv ist.
  transmitTimeIsActive:boolean = false;

  // Zeiteinstellungen (settings) Protokoll für die sekündliche Übertragung.
  public setting21H:Setting21H = new Setting21H();

  // Decoderungstabelle
  private decodeValuesAr : DecodeValues[] = [
  {
    "index": 0,
    "identifier" : 0x30,
    "byte1": 0x85,
    "byte2": 0xBA,
    "byte3": 0x90,
    "byte4": 0xA5,
  },//A5 90 BA 85
  {
    "index": 1,
    "identifier" : 0x31,
    "byte1": 0xB1,
    "byte2": 0xA2,
    "byte3": 0x9F,
    "byte4": 0x8B,
  }, // 8B 9F A2 B1  
  {
    "index": 2,
    "identifier" : 0x32,
    "byte1": 0xAA,
    "byte2": 0xB2,
    "byte3": 0xA7,
    "byte4": 0x99,
  },
  {
    "index": 3,
    "identifier" : 0x33,
    "byte1": 0x97,
    "byte2": 0xA6,
    "byte3": 0xB3,
    "byte4": 0x85,
  },
  {
    "index": 4,
    "identifier" : 0x34,
    "byte1": 0x9D,
    "byte2": 0x84,
    "byte3": 0xBC,
    "byte4": 0xAD,
  },
  {
    "index": 5,
    "identifier" : 0x35,
    "byte1": 0x8F,
    "byte2": 0xA8,
    "byte3": 0xBD,
    "byte4": 0xB1,
  },
  {
    "index": 6,
    "identifier" :0x36,
    "byte1": 0x9C,
    "byte2": 0xA0,
    "byte3": 0x87,
    "byte4": 0xBA,
  },
  {
    "index": 7,
    "identifier" : 0x37,
    "byte1": 0xC1,
    "byte2": 0xC7,
    "byte3": 0x99,
    "byte4": 0x88,
  },
  {
    "index":8,
    "identifier" : 0x38,
    "byte1": 0xB2,
    "byte2": 0x86,
    "byte3": 0x95,
    "byte4": 0x84,
  },
  {
    "index": 9,
    "identifier": 0x39,
    "byte1": 0x83,
    "byte2": 0xB7,
    "byte3": 0x81,
    "byte4": 0xB0,
  }];


  constructor(
    private debugService:DebugService,
    private bluetoothService:BluetoothService,
    private settingsService:SettingsService
    ) {
  

      this.debugService.getdata().subscribe(
        (data) => {this.debugSettings = data;}
      );

      // react on connection changes
      this.isConnected  = false;
      this.subscription =  this.bluetoothService.ConnectionChanges.subscribe(
        (data) => {this.isConnected = data;}
      );



    // Solange eine Übertragung besteht
    // müssen beim Neustart die alten Werte übernommen werden
    // Settings für die sekündliche Übertragung
    this.setting21H.min10 = "0";
    this.setting21H.min1 = "0";
    this.setting21H.sec10 = "0";
    this.setting21H.sec1 = "0";
    this.setting21H.secDiv10 = " ";
    this.setting21H.flagMid = "2";   //Pausiert
    this.setting21H.signalEnd = "0"; //Kein Signal

   }

   ngOnDestroy(){
    this.subscription.unsubscribe();
  }

    // nur der Inhalt wird decodiert
   private decodeValues(numArray:number[],start: number, end: number){

    if (end >  numArray.length){
      // error
      return (null)
    }

    // das dritte Element enthält immer den Decodierungsschlüssel.
    let decodeElement = this.decodeValuesAr.find(x => x.identifier == numArray[2])

    if (GlobalFunction.isEmpty(decodeElement)){
      // error
      return (null)
    }


    // decodierung is deakiviert. Zu Testzwecken eingebaut.
    if (!this.debugSettings.decodeProtocol){
      return (numArray)
    }

    let count = 1;
    for (let index = start; index <= end; index++) {

      switch (count) {
        case 1:
          numArray[index] = GlobalFunction.xor(numArray[index],decodeElement.byte1);
        break;
        case 2:
          numArray[index] = GlobalFunction.xor(numArray[index],decodeElement.byte2);
        break;
        case 3:
          numArray[index] = GlobalFunction.xor(numArray[index],decodeElement.byte3);
        break;
        case 4:
          numArray[index] = GlobalFunction.xor(numArray[index],decodeElement.byte4);
        break;
      }

      count++;
      if (count == 5){ // es sind nur 4 Schlüssel vorhanden => Beginne wieder mit dem ersten Schlüssel
        count = 1
      }
    }

    return (numArray)
   }

   // Start der übertragung an die Anzeige
   public startTransmitTimer(){

    // sekündliche Übertragung wird gestartet
    if (this.transmitTimeIsActive){
      return;
    }

    if (this.debugSettings.deactiveSentProtocolTimer){
      return;
    }


    // Intevall für die sekündliche Übertragung der Zeit
    this.intervalTimer = setInterval(() => {
      this.prot21h();
    },this.interval)

    this.transmitTimeIsActive = true;


  }

  /// Ende der Übertragung an die Anzeige
  public endTransmitTimer(){

    if (this.debugSettings.deactiveSentProtocolTimer){
      return;
    }

    clearInterval(this.intervalTimer);
 
    this.transmitTimeIsActive = false;
    if (this.debugSettings.logAdditionalInformation){
      console.log("Die sekündliche Übertragung der stehenden oder laufenden Zeit wurde beendet.");
      console.log("Damit müsste sich die Anzeige auch ausschalten.")
    }
  }

    // Aktion wird bei Ende einer Periode Hockey und manuelles auslösen bei Hockey aktiviert.
    sentTimeWithSignal(){
      this.signalSentCounter = 5;
    }

    private getIdentDecodeFromIndex(index:number){

        let element = this.decodeValuesAr.find(x => x.index == index);
        if (element){
          return (element.identifier);
        }
        return 0;
    }


  // Erkennung der Sportart und Synchronisieren der Uhr
  prot20h(sportTypeVal1:number,sportTypeVal2:number)
  {

    let intArray = [];

    intArray[0] = this.startByte;
    intArray[1] = 0x20;
    intArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich

    // 4-5 Sportart
    intArray[3] = sportTypeVal1;
    intArray[4] = sportTypeVal2;


    // ob Uhrzeit uebertragen wird. Wenn nicht schaltet sich die Anzeige nach Beenden des Spiels ab, ansonsten läuft die Uhr weiter
    if  (this.settingsService.hasTimeTransfer){
      intArray[5] = this.setting20HUhrAktiv.charCodeAt(0); // todo: recheck and make sure that the ASCII of 1 is correct at this point
    } else {
      intArray[5] = this.setting20HUhrNichtAktiv.charCodeAt(0);
    }

    // Uhrzeit Stunden und Minuten werden übertragen.
    let hourStr = "";
    let minStr = "";

    var d = new Date();
    var h = d.getHours();

    if (h < 10){
      hourStr = "0" + h;

    } else {
      hourStr = "" + h;

    }
    var m = d.getMinutes();
    if (m < 10){

      minStr = "0" + m;
    } else {
      minStr = "" + m;

    }
    // 7-10: Stunden Minuten 10er 1er
    intArray[6] = hourStr.charCodeAt(0);
    intArray[7] = hourStr.charCodeAt(1);
    intArray[8] = minStr.charCodeAt(0);
    intArray[9] = minStr.charCodeAt(1);
    intArray[10] = this.endByte;

    if (this.debugSettings.logPreProtocolContentSettings){
      console.log("sportTypeVal1:" + sportTypeVal1);
      console.log("sportTypeVal2:" + sportTypeVal2);
      console.log("hourStr:" + hourStr);
      console.log("minStr:" + hourStr);
    }

    let sendArray = this.decodeValues(intArray,3,9)
    this.sentProtokoll(sendArray,"prot20h");      
  }

  // Spielzeit und Hupe
  // muss jede Sekunde übertragen werden, egal ob Uhr läuft oder nicht.
  prot21h()
  {

    this.setting21H.secDiv10 = this.setting21SecDiv10FlagASCII;

    // Falls aktiviert das signal 5 mal senden.
    if (this.signalSentCounter > 0){
      this.setting21H.signalEnd = SETTING21_GAME_SIGNAL_ENDE_ON_FLAG_ASCII;
    } else {
      this.setting21H.signalEnd = SETTING21_GAME_SIGNAL_ENDE_Off_FLAG_ASCII;
    }

    if (this.setting21H.min10 == undefined){
      this.setting21H.min10 = "0";
    }

    if (this.setting21H.min1 == undefined){
      this.setting21H.min1 = "0";
    }
    if (this.setting21H.sec10 == undefined){
      this.setting21H.sec10 = "0";
    }

    if (this.setting21H.sec1 == undefined){
      this.setting21H.sec1 = "0";
    }

    // Pausiert
    if (this.setting21H.flagMid == undefined){
      this.setting21H.flagMid = "2";
    }


    if (this.setting21H.signalEnd == undefined){
      this.setting21H.signalEnd = "0";
    }


    let intArray = [];
    intArray[0] = this.startByte;
    intArray[1] = 0x21;
    intArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich
    intArray[3] = this.setting21H.min10.charCodeAt(0);
    intArray[4] = this.setting21H.min1.charCodeAt(0);
    intArray[5] = this.setting21H.sec10.charCodeAt(0);
    intArray[6] = this.setting21H.sec1.charCodeAt(0);
    intArray[7] = this.setting21H.secDiv10.charCodeAt(0);   //20h: zehntel Sekunden werden nicht übertragen.
    intArray[8] = this.setting21H.flagMid.charCodeAt(0);    // 31: Spiel läuft 32: Spiel pausiert
    intArray[9] = this.setting21H.signalEnd.charCodeAt(0);  // 31: Hupe an     30: Hupe aus
    
    if (this.setting21H.signalEnd == "1"){
      // hupe test
      debugger;
    } 
    if (this.setting21H.flagMid == "1"){
      console.log("Spiel läuft");
    } 
    if (this.setting21H.flagMid == "2"){
      console.log("Spiel angehalten");
    } 
    
    
    intArray[10] = this.endByte;
    let sentArray = this.decodeValues(intArray,3,9)

    if (this.debugSettings.logPreProtocolContentSettings){
      console.log(this.setting21H);
    }
    this.sentProtokoll(sentArray,"prot21h");

    // senden des signals wieder herunter zählen.
    if (this.signalSentCounter > 0){
      this.signalSentCounter--;
    }


  }

  // Periode
  // hat beim Fußball keine Bedeutung
  // Ansonsten auch nur die Periode relevant
  // Für andere Parameter leere Werte übertragen
  prot22h(setting22H : Setting22H)
  {
    setting22H.flagLeft = SETTING22_FLAG_LINKS;
    setting22H.flagRight = SETTING22_FLAG_RECHTS;
    setting22H.pfeil  = SETTING22_Pfeil;

    let numArray = [];
    numArray[0] = this.startByte;
    numArray[1] = 0x22;
    numArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); //30h-39h möglich
    numArray[3] = setting22H.period.charCodeAt(0);
    numArray[4] = setting22H.pfeil.charCodeAt(0);
    numArray[5] = setting22H.flagLeft.charCodeAt(0);
    numArray[6] = setting22H.flagRight.charCodeAt(0);
    numArray[7] = this.endByte;

    if (this.debugSettings.logPreProtocolContentSettings){
      console.log(setting22H);
    }

    let sendArray = this.decodeValues(numArray,3,6)
    this.sentProtokoll(sendArray,"prot22h");


  }

  // Spielstand
  prot23h(homeGoals: number,guestGoals:number)
  {

    // als 100er Stelle wird immer eine 0 übertragen
    let strHomeGoals = "";
    let strGuestGoals = "";
    if (homeGoals < 10) {
      strHomeGoals = "00" + homeGoals.toString();
    } else {
      strHomeGoals = "0" +  homeGoals.toString();
    }

    if (guestGoals < 10) {
      strGuestGoals = "00" + guestGoals.toString();
    } else {
      strGuestGoals = "0" + guestGoals.toString();
    }
  
    let numArray = [];
    numArray[0] = this.startByte;
    numArray[1] = 0x23;
    numArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich

    // 4-6 Übertragung Tore Heimmanschaft
    numArray[3] = strHomeGoals.charCodeAt(0);
    numArray[4] = strHomeGoals.charCodeAt(1);
    numArray[5] = strHomeGoals.charCodeAt(2);

    // 4-6 Übertragung Tore Gastmannschaft
    // 10er, 1er, 100er
    numArray[6] = strGuestGoals.charCodeAt(0);
    numArray[7] = strGuestGoals.charCodeAt(1);
    numArray[8] = strGuestGoals.charCodeAt(2);
    numArray[9] = this.endByte;

    if (this.debugSettings.logPreProtocolContentSettings){
      console.log("Zahlenwerte von :");
      console.log("strHomeGoals:" + strHomeGoals);
      console.log("strGuestGoals:" + strGuestGoals);
    }

    let sendArray = this.decodeValues(numArray,3,8)
    this.sentProtokoll(sendArray,"prot23h");

  }

  //Mannschaftsnamen
  prot29h(teamNames:TeamNames)
  {

    // auffüllen beider Mannschaftsnamen mit Leerzeichen am Ende auf insgesamt 8 Zeichen für die Übertragung.
    let addCharsHomeNum = (8 -  teamNames.homeTeamName.length);
    let addCharsGuestNum = (8 -  teamNames.guestTeamName.length);

    let homeTeamWithFilledChars = teamNames.homeTeamName;
    for (let index = 0; index < addCharsHomeNum; index++) {
      homeTeamWithFilledChars = homeTeamWithFilledChars + " ";
    }
    let guestTeamWithFilledChars = teamNames.guestTeamName;
    for (let index = 0; index < addCharsGuestNum; index++) {
      guestTeamWithFilledChars = guestTeamWithFilledChars + " ";
    }

    let numArray = [];
    numArray[0] = this.startByte;
    numArray[1] = 0x29;
    numArray[2] = this.getIdentDecodeFromIndex(this.decodeCounter); // 30h-39h möglich

    // Heimmanschaft 8 Zeichen (4-11)
    numArray[3]  = homeTeamWithFilledChars.charCodeAt(0);
    numArray[4]  = homeTeamWithFilledChars.charCodeAt(1);
    numArray[5]  = homeTeamWithFilledChars.charCodeAt(2);
    numArray[6]  = homeTeamWithFilledChars.charCodeAt(3);
    numArray[7]  = homeTeamWithFilledChars.charCodeAt(4);
    numArray[8]  = homeTeamWithFilledChars.charCodeAt(5);
    numArray[9]  = homeTeamWithFilledChars.charCodeAt(6);
    numArray[10]  = homeTeamWithFilledChars.charCodeAt(7);

    // Gästemanschaft 8 Zeichen (12-18)
    numArray[11]  = guestTeamWithFilledChars.charCodeAt(0);
    numArray[12]  = guestTeamWithFilledChars.charCodeAt(1);
    numArray[13]  = guestTeamWithFilledChars.charCodeAt(2);
    numArray[14]  = guestTeamWithFilledChars.charCodeAt(3);
    numArray[15]  = guestTeamWithFilledChars.charCodeAt(4);
    numArray[16]  = guestTeamWithFilledChars.charCodeAt(5);
    numArray[17]  = guestTeamWithFilledChars.charCodeAt(6);
    numArray[18]  = guestTeamWithFilledChars.charCodeAt(7);
    numArray[19]  = this.endByte;

    if (this.debugSettings.logPreProtocolContentSettings){
      console.log("Zahlenwerte von :");
      console.log("homeTeamWithFilledChars:" + homeTeamWithFilledChars);
      console.log("guestTeamWithFilledChars:" + guestTeamWithFilledChars);
    }

    // Der Databereich wird verschlüsselt.
    let sendArray = this.decodeValues(numArray,3,18)

    this.sentProtokoll(sendArray,"prot29h");
  }

  sentProtokoll(sendArray:number[],name:string){

    let charArray = [];
    if (this.debugSettings.debugProtokolWorkflow){
      debugger;
    }

    for (let index = 0; index < sendArray.length; index++) {
      charArray[index] =  String.fromCharCode(sendArray[index]);
    }

    let str = "";
    if (this.debugSettings.logProtocolContent){
      console.log("Send Prot:" + name);
      console.log(charArray);
    }

    for (let index = 0; index < charArray.length; index++) {
      str +=  charArray[index];
    }

    // counter zum decodieren hochzählen
    this.decodeCounter++;
    if (this.decodeCounter == 10){ //letzte Element erreicht
      this.decodeCounter = 0;
    }

    if (this.isConnected){
      this.bluetoothService.sentIfConnected(str)
    } 
  }
}
