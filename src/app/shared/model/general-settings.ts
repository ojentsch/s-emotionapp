export class ConfigSettings{

    bluetoothDeviceId:string;
    bluetoothDeviceName:string;
    decodeProtocol:boolean;

}

export class DebugSettings{
    debugProtokolWorkflow:boolean=false;
    debugProtokolSent:boolean=true;
    decodeProtocol:boolean=true;// todo: must be set to true at the end.
    logProtocolTimerSettings = true;
    logAdditionalInformation = true;
    logProtocolContent:boolean=true;
    logPreProtocolContentSettings: boolean=true;
    deactiveSentProtocolTimer: boolean=false; //todo: must be set to false at the end.
    displayPreOnHtml:boolean=false;
    sendWithBluetooth:boolean=false;
    displayJson:boolean=false;
}

export enum TimerDirection {
    Forward = 1,
    Backward
}

export enum GameType {
    Football = 1,
    Hockey
}

export enum SumPeriods {
    Yes = 1,
    No
}

export enum ExtraTimeAvailable {
    Yes = 1,
    No
}

export enum GameStatus {
    init = 1,
    play,
    pause,
    periodFinished,
    gameFinished
}

export enum TimePeriod {
  normal = 1,
  renewal = 2,
  Backward
}

export class TeamNames {
    homeTeamName:string= "";;
    guestTeamName:string="";
}


export class Setting21H {
    min10:string;
    min1:string;
    sec10:string;
    sec1:string;
    secDiv10:string; 
    flagMid:string;  
    signalEnd:string;
}


//todo: mit richtigen standardwerten definieren
export class Setting22H{

    period:string;
    pfeil:string = " ";
    flagLeft:string = " ";
    flagRight:string = " ";

}

// todo: muss in zahlen umbewendelt werden
// werden nur die HexWerte in string geschrieben
export class DecodeValues{

    index: number;
    identifier: number;
    byte1: number;
    byte2: number;
    byte3: number;
    byte4: number;

}



export class CurrentGame {
    gameType: GameType;
    sumPeriods: SumPeriods;
    period = 0;
    homeGoals:number = 0;
    guestGoals:number = 0;
    currentMinutes:number= 0;
    currentSec:number= 0;
    totalSec:number= 0;
    gameStatus:GameStatus;
    homeTeamName:string;
    guestTeamName:string;
    timerDirection:TimerDirection;
    timePeriod:TimePeriod = TimePeriod.normal;
    targetMinutes:number; // either periodMinutes or 0 (count backwards)
    periodMinutes:number; // total minutes of current periods
    
    extraTimeAvailable: ExtraTimeAvailable = ExtraTimeAvailable.No;
    regularTimeTargetMinutes:number;
    regularTimeNumPeriods:number;
    
    extraTimeTargetMinutes:number;
    extraTimeNumPeriods:number;
}


export class GeneralSettings {
    
    settingId: GameType;
    title: string;
    icon:string;
    status: number;
}

export class PeriodSettings {   
    settingName: string;
    periodSettingId: number;
    durationMin: number;
    periodAmounts: number;
    timerDirection:TimerDirection
}

export class ExtraTimeSettings {   
    extraTimeSettingId: number;
    durationMin: number;
    periodAmounts: number;
    relatedSettingId: number; 
}
/* alternative timer, currently not used. */
export interface ITimer {
    seconds: number;
    secondsRemaining: number;
    runTimer: boolean;
    hasStarted: boolean;
    hasFinished: boolean;
    displayTime: string;
  }
