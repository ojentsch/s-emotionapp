import { CurrentGame, Setting21H, SumPeriods, TimePeriod } from './model/general-settings';

export class GlobalFunction {


    public static isEmpty(val){
        return (val === undefined || val == null || val.length <= 0) ? true : false;
    }

    //Soll eine XOR Verknüpfung des HexArrays mit Zeichen.
    public static xor(val1: number,val2: number){

      // todo: xor val1 and val2
      return val1 ^ val2;
  
    }

    public static  stringToBytes(str: string):Uint8Array {
      var array = new Uint8Array(str.length);
      for (let i = 0, l = str.length; i < l; i++) {

        array[i] = str.charCodeAt(i);
       }
       return array;
   }


   // ursprungsfunktion
   public static  stringToBytesBuffer(str: string) {
    var array = new Uint8Array(str.length);
    for (let i = 0, l = str.length; i < l; i++) {

      array[i] = str.charCodeAt(i);
     }
     return array.buffer;
 }



    public static async wait(ms){
      var start = new Date().getTime();
      var end = start;
      while(end < start + ms) {
        end = new Date().getTime();
      }
    }
      
    public static charFromASCII(numArray:number[]){

      let strArr = [];

      for (let index = 0; index < numArray.length; index++) {
        
        strArr[index] = String.fromCharCode(index);

        
      }

      return (strArr);
    }

    public static setSettings21HFromCurrentGame(currentGame: CurrentGame,setting21H: Setting21H):Setting21H{
  

      if (currentGame.currentSec > 9){
  
        let str = currentGame.currentSec.toString();
        setting21H.sec10 = str.charAt(0)
        setting21H.sec1 = str.charAt(1)
  
      } else {
        setting21H.sec10 = "0"
        setting21H.sec1 = currentGame.currentSec.toString();
      }

      // todo: modify the minutes with period in
      let modifiedMin = 0;
      if (currentGame.sumPeriods == SumPeriods.Yes) {
        //modifiedMin = currentGame.currentMinutes;

        if (currentGame.timePeriod == TimePeriod.normal){ //normale Spielzeit
          if (currentGame.period > 1){
            modifiedMin =  ((currentGame.period-1) * (currentGame.regularTimeTargetMinutes)) + currentGame.currentMinutes;   
          } else {
            modifiedMin = currentGame.currentMinutes;
          }
        } else { // Verlängerung
          modifiedMin =  ((currentGame.period - 1 - currentGame.regularTimeNumPeriods) * currentGame.extraTimeTargetMinutes) + currentGame.currentMinutes;          
        }
      }  else {
        modifiedMin = currentGame.currentMinutes;
      }
  


      if (modifiedMin > 9){
  
        let str = modifiedMin.toString();
        setting21H.min10 = str.charAt(0)
        setting21H.min1 = str.charAt(1)
      } else {
        setting21H.min10 = "0"
        setting21H.min1 = modifiedMin.toString();
      }
 
      return setting21H;
  
        
    }


}


