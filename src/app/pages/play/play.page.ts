import { Component, OnInit } from '@angular/core';
import { CurrentGame, TimerDirection, GameStatus, GameType, TimePeriod,Setting22H,Setting21H, ExtraTimeAvailable, DebugSettings } from 'src/app/shared/model/general-settings';
import { CurrentGameService } from 'src/app/shared/service/current-game.service';
import { Subscription} from 'rxjs';
import { Router } from '@angular/router';
import { GlobalFunction } from 'src/app/shared/global-function';
import { FootballService } from 'src/app/shared/service/football.service';
import { HockeyService } from 'src/app/shared/service/hockey.service';
import { ProtocolService } from 'src/app/shared/service/protocol.service';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core'; 
import { DebugService } from 'src/app/shared/service/debug.service';
import { MessageService } from 'src/app/shared/service/message.service';
import { BluetoothService } from 'src/app/shared/service/bluetooth.service';
import { ConfigurationService } from 'src/app/shared/service/configuration.service';
import { GameTimeCounterService } from 'src/app/shared/service/game-time-counter.service';

@Component({
  selector: 'app-play',
  templateUrl: './play.page.html',
  styleUrls: ['./play.page.scss'],
})
export class PlayPage implements OnInit {


  private subscription: Subscription;
  private classame = (<any>this).constructor.name;

  
  private oldPeriod: number;
  public debugSettings:DebugSettings;

  public pageGameStatus : GameStatus;
  public currentGame: CurrentGame;
  public gameType: GameType;

  private subscriptionRes: Subscription;
  private naechste_periode_res:string;
  private uebertragung_beenden_res:string;
  private spiel_beenden_res:string;
  private ja_res:string;
  private nein_res:string;
  private spiel_beendet_res: string
  private bluetooth_error_res: string


  constructor(

    private configurationService:ConfigurationService,
    public currentGameService:CurrentGameService,
    private footballService:FootballService,
    private hockeyService:HockeyService,
    private router:Router,
    private protocolService:ProtocolService,
    public alertController: AlertController,
    public translate:TranslateService,
    private debugService:DebugService,
    private messageService: MessageService,
    private bluetoothService:BluetoothService,
    private gameTimeCounterService: GameTimeCounterService
  ) { 
    // Immer wenn sich der Wert ändert wird das Protokoll prot22h ausgelöst. 
    this.oldPeriod = 0;
    this.translate.setDefaultLang('de');

    this.debugService.getdata().subscribe(
      (data) => {this.debugSettings = data;}
    );

  }

  ngOnInit(): void {
    console.log(`${this.classame}  ngOnInit`);

    this.subscriptionRes = this.translate.get(['NAECHSTE_PERIODE','UEBERTRAGUNG_BEENDEN','SPIEL_BEENDEN','JA','NEIN','SPIEL_BEENDET',"BLUETOOTH_GERAET_NICHT_VERFUEGBAR"]).
    subscribe((translation: [string]) => {  
      this.naechste_periode_res = translation['NAECHSTE_PERIODE'];
      this.uebertragung_beenden_res = translation['UEBERTRAGUNG_BEENDEN'];
      this.spiel_beenden_res = translation['SPIEL_BEENDEN'];
      this.ja_res = translation['JA'];
      this.nein_res = translation['NEIN'];
      this.spiel_beendet_res = translation['SPIEL_BEENDET'];
      this.bluetooth_error_res = translation['BLUETOOTH_GERAET_NICHT_VERFUEGBAR'];
    });


    this.subscription = this.currentGameService.getdata().subscribe(data =>
      {            
        this.reactOnCurrentSettingChanges(data);
    });

    // in einigen Fällen besser auf diesen Wed die Daten aktualisieren.
    this.subscription = this.currentGameService.GameChanges.subscribe(data =>
    {       
      this.reactOnCurrentSettingChanges(data);
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
    this.subscriptionRes.unsubscribe();
  }

  async confirmFinishGame() {
    const alert = await this.alertController.create({
      header: this.spiel_beenden_res,
      buttons: [
        {
          text:this.ja_res,
          handler: () => this.finishGame()
        },
        {
          text:this.nein_res,
          handler: () => this.cancel()
        }
      ],
    });
    await alert.present();
  }


  async confirmNextPeriod() {
    const alert = await this.alertController.create({
      header: this.naechste_periode_res,
      buttons: [
        {
          text:this.ja_res,
          handler: () => this.nextPeriod()
        },
        {
          text:this.nein_res,
          handler: () => this.cancel()
        }
      ],
    });
    await alert.present();
  }

  async confirmStopTransmit() {
    const alert = await this.alertController.create({
      header: this.uebertragung_beenden_res,
      buttons: [
        {
          text:this.ja_res,
          handler: () => this.stopTransmit()
        },
        {
          text:this.nein_res,
          handler: () => this.cancel()
        }
      ],
    });
    await alert.present();
  }

  cancel() {
    console.log("Cancel");
  }


  ionViewWillEnter () {

    // todo: not clear if this works as expected
    if ( GlobalFunction.isEmpty(this.currentGameService.currentGame)){
      this.router.navigate(['settings']);
    }     
    else if (GlobalFunction.isEmpty(this.currentGameService.currentGame.gameStatus)) {
      this.router.navigate(['settings-advanced']);
    }

    //this.messageService.presentToasterMiddleMessage(this.bluetooth_error_res);

    console.log(`${this.classame}  ionViewDidEnter`);

  }


  private reactOnCurrentSettingChanges(data:CurrentGame){
   
    this.pageGameStatus = data.gameStatus;

    this.currentGame = data;
    if (this.oldPeriod != this.currentGame.period){ // senden der Periodeninformation an die Anzeige
 
      // nur wenn das Spiel noch nicht angefangen hat. 
      // Im anderen Fall wird von den Setting aus das erste Mal das Protokoll aus aufgerufen
      if (this.oldPeriod != 0){ 

        // senden der Periode
        // beim Fussball keine Bedetung, wird aber trotzdem mit übertragen  
        let setting22H = new Setting22H();
        setting22H.period = this.currentGame.period.toString();

        if (this.debugSettings?.debugProtokolWorkflow){
          debugger;
        }
        this.protocolService.prot22h(setting22H);
      }
 
      // wenn der Timer nich gestartet ist, dann Timer starten.
      if (!this.protocolService.transmitTimeIsActive){
        
        if (this.debugSettings?.debugProtokolWorkflow){
          debugger;
        }
        
        // Beginn der sekündlichen Übertragung der stehenden und laufenden Zeit.
        this.protocolService.startTransmitTimer();  

      }

    // Die Uhrzeit aus den Settings wieder aktualisieren.
    let setting21H = new Setting21H();
    setting21H.flagMid = "2"; // Pause 
    setting21H.signalEnd = "0"; // Kein Signal
    setting21H = GlobalFunction.setSettings21HFromCurrentGame(this.currentGameService.currentGame, setting21H)
          

    // Damit müssten die Werte bei der nächsten Protokollübertragung aktualisiert werden
    this.protocolService.setting21H = setting21H;
    }

    this.oldPeriod = this.currentGame.period;

    if ((data.gameStatus == GameStatus.pause)  
    || (data.gameStatus == GameStatus.periodFinished)
    || (data.gameStatus == GameStatus.gameFinished)
    || (data.gameStatus == GameStatus.init)
    ){
      // Den Timer der Seite anhalten.

      this.pauseTimer();
    }

    //Bei Hockey nach der Beendigung einer Periode die Hupe auslösen
    if (this.currentGameService.hockeyPeriodFinished()){

      if (this.debugSettings?.debugProtokolWorkflow){
        debugger;
      }
      // Signal wird nur ausgelöst, wenn Zeit automatisch runter läuft und nicht manuell eine neue Periode gestartet wird. 
      this.protocolService.sentTimeWithSignal();
    }

    // Eine Nachricht anzeigen, falls eine Ressource dafür vorhanden ist.
    if ( (data.gameStatus == GameStatus.gameFinished) && !GlobalFunction.isEmpty(this.spiel_beendet_res)) {
      if (data.period != 0){
        this.messageService.presentToasterMessage(this.spiel_beendet_res);
      }
    }
  } 

  getSportReleateMinutes(){
    return (this.currentGameService.getSportReleateMinutesService())
  }

  isFootball(){
    return (this.currentGameService?.currentGame?.gameType == GameType.Football)
  }

  isHockey(){
    return (this.currentGameService?.currentGame?.gameType == GameType.Hockey)
  }

  gameIsRunning(){
    if (GlobalFunction.isEmpty(this.pageGameStatus)){

      return false
    }
    return (this.pageGameStatus == GameStatus.play)
  }

  isFootballNormal(){
    return (this.currentGameService?.currentGame?.gameType == GameType.Football && this.currentGameService?.currentGame?.timePeriod == TimePeriod.normal)
  }
  isFootballReneval(){
    return (this.currentGameService?.currentGame?.gameType == GameType.Football && this.currentGameService?.currentGame?.timePeriod == TimePeriod.renewal)
  }

  getTotalPeriods(){

    if (GlobalFunction.isEmpty(this.currentGameService?.currentGame)){
      return 0;
    } else {
      if (this.currentGameService?.currentGame.timePeriod == TimePeriod.normal){
        return (this.currentGameService?.currentGame.regularTimeNumPeriods);

      } else {
        return (this.currentGameService?.currentGame.extraTimeNumPeriods);
      }
    }
  }

  getPeriodNumber(){
    if (GlobalFunction.isEmpty(this.currentGameService?.currentGame)){
      return 0
    }
    if (this.currentGameService?.currentGame.timePeriod == TimePeriod.normal){

      return this.currentGameService?.currentGame.period
    } else {

      return (this.currentGameService?.currentGame.period - this.currentGameService?.currentGame.regularTimeNumPeriods);
    }
  }


  goalTeam1Add(){
    this.currentGameService.goalTeam1Add();
    this.changeGoalHandling()
  }

  goalTeam1Substract(){
    this.currentGameService.goalTeam1Substract();
    this.changeGoalHandling()
  }

  goalTeam2Add(){
    this.currentGameService.goalTeam2Add();
    this.changeGoalHandling()
  }

  goalTeam2Substract(){
    this.currentGameService.goalTeam2Substract();
    this.changeGoalHandling()
  }

  changeGoalHandling(){ // Anzeige informieren,
    
    if (this.debugSettings?.debugProtokolWorkflow){
      debugger;
    }
    this.protocolService.prot23h(this.currentGameService.currentGame.homeGoals,this.currentGameService.currentGame.guestGoals);

  }


  // deaktivert wenn das spiel läuft oder das ende der möglichen Verlängerung erreicht ist.
  nextPeriodDisabled(){
    
    if (this.pageGameStatus == GameStatus.play){
      return true;
    } 

    let maxPeriod = 0;
    if (this.currentGame.extraTimeAvailable == ExtraTimeAvailable.No) {
      maxPeriod = this.currentGame?.regularTimeNumPeriods
    } else {
      maxPeriod = this.currentGame?.regularTimeNumPeriods + this.currentGame?.extraTimeNumPeriods;
    }


    if (!isNaN(maxPeriod)){
      if (this.currentGame.period >= maxPeriod){
        return true;
      }
    }

    return false;
  }

  manualChangeDisabled(){
    if (this.pageGameStatus == GameStatus.play){
      return true;
    } 
    return false;

  }

  finishButtonDisabled(){

    if (this.pageGameStatus == GameStatus.play){
      return true;
    } 
    return false;
  }

  playDisabled(){

    if (GlobalFunction.isEmpty(this.pageGameStatus)){
      return true;
    }

    if (this.pageGameStatus  == GameStatus.periodFinished){
      return true;
    }

    if (this.pageGameStatus  == GameStatus.gameFinished){
      return true
    }
  
    if (this.pageGameStatus  == GameStatus.play)
    {
      return true
    }
    return false;
  }

  playButtonDisabled() {
    return this.playDisabled();  
  }

  breakButtonDisabled(){
    if (GlobalFunction.isEmpty(this.pageGameStatus)){
      return true;
    }

    if (this.pageGameStatus == GameStatus.pause){
      return true;
    } 

    if (this.pageGameStatus == GameStatus.gameFinished){
      return true;
    }
    
    if (this.pageGameStatus == GameStatus.periodFinished){
      return true;
    } 

    return false;
  }


  nextPeriod(){
    debugger;
    this.pauseTimer();
    this.currentGameService.startNextPeriod();


  }

  manualChange(){
    this.router.navigate(['period']);
  }

  sendSignalDisabled(){

    // Beim Fußball gibt es die Signanübertragund nicht.
    // besser ausblenden des Buttons.
  }

  // für 5 Sekunden muss das Flag SigEnde auf "1" stehen danach wieder auf "0" setzen
  // todo: fix review : obwohl die Methode async ist 
  // läuft der Service zum verschicken (prot22h) nicht weiter
  sendSignal(){

    if (this.debugSettings?.debugProtokolWorkflow){
      debugger;
    }
    this.protocolService.sentTimeWithSignal();
  }


  // am besten noch mal in einem extra step. 
  stopTransmit(){

    this.gameTimeCounterService.clearTimer();
    // damit wird auch die Übertragung beendet!
    this.currentGameService.finishGame();

    let setting21H = new Setting21H();
    setting21H.flagMid = ProtocolService.setting21GameIsNotRunningFlagASCII;
    // damit müsste das Protokoll aktualisiert werden
    this.protocolService.setting21H = setting21H; 
    

    // Die Übertragung wird nach Spielende ebenfalls beendet. 
    if (this.debugSettings?.debugProtokolWorkflow){
      debugger;
    }

    this.protocolService.endTransmitTimer();

    // // zurück zur Hauptseite
    if (this.currentGame.gameType == GameType.Hockey){
      if (this.hockeyService.hockeySettings.length > 1){
        this.router.navigate(['settings-advanced']);
      } else {
        this.router.navigate(['settings']);
      }
    } else if (this.currentGame.gameType == GameType.Football){
      if (this.footballService.footballSettings.length > 1){
        this.router.navigate(['settings-advanced']);
      } else {
        this.router.navigate(['settings']);
      } 
    } else {
      this.router.navigate(['settings']);
    }
    // Timer anhalten
    

    // if ( GlobalFunction.isEmpty(this.currentGameService.currentGame)){
    //   this.router.navigate(['settings']);
    // } else if (this.currentGameService.currentGame.period == 0) {
    //   this.router.navigate(['settings-advanced']);
    // }

  }

  getSportCellClass1(){


    if (GlobalFunction.isEmpty(this.currentGameService?.currentGame)){
      return "cell-class"
    } else {

      if (this.currentGameService.currentGame.gameType == GameType.Football){

        return "football1 cell-class"
      } else {
        return "hockey1 cell-class"

      }
    }
  }

  getSportCellClass2(){

    if (GlobalFunction.isEmpty(this.currentGameService?.currentGame)){
      return "cell-class"
    } else {

      if (this.currentGameService.currentGame.gameType == GameType.Football){
        return "football2 cell-class"
      } else {
        return "hockey2 cell-class"

      }
    }
  }

  

  playGo() {


    // Zusätzliche Bedingung für Weiterführung des Spiels wird hier noch einmal überrprüft
    if (this.playDisabled()){
      return;
    }

    // timer transmit time stop for a short moment
    this.protocolService.endTransmitTimer();
    this.gameTimeCounterService.SetIntervalTimer();


    this.currentGameService.currentGame.gameStatus = GameStatus.play;
    this.pageGameStatus = GameStatus.play;

    // timer transmit start againg after second count.
    this.protocolService.startTransmitTimer();

  }
    
 

  break(){

    this.pauseTimer()
    this.currentGameService.currentGame.gameStatus = GameStatus.pause;
    this.pageGameStatus = GameStatus.pause;
  }

  pauseTimer() {

    this.gameTimeCounterService.clearTimer();
    let setting21H = this.protocolService.setting21H; 
    setting21H.flagMid = ProtocolService.setting21GameIsNotRunningFlagASCII;
    // damit müsste das Protokoll aktualisiert werden
    this.protocolService.setting21H = setting21H; 


  }

  // spiel beenden
  finishGame() {

    
    this.gameTimeCounterService.clearTimer();

    // damit wird auch die Übertragung beendet!
    this.currentGameService.finishGame();

    let setting21H = new Setting21H();
    setting21H.flagMid = ProtocolService.setting21GameIsNotRunningFlagASCII;
    // damit müsste das Protokoll aktualisiert werden
    this.protocolService.setting21H = setting21H; 

    //this.currentGameService.setEndGameSettings();
    

    // Die Übertragung wird nach Spielende nictht beendet. 
    // Problem hierbei ist, dass Hockey am Ende einer Periode 5 Sekundein ein Signal schickt.
    this.protocolService.endTransmitTimer();


    // zurück zur Hauptseite
    if (this.currentGame.gameType == GameType.Hockey){
      if (this.hockeyService.hockeySettings.length > 1){
        this.router.navigate(['settings-advanced']);
      } else {
        this.router.navigate(['settings']);
      }
    } else if (this.currentGame.gameType == GameType.Football){
      if (this.footballService.footballSettings.length > 1){
        this.router.navigate(['settings-advanced']);
      } else {
        this.router.navigate(['settings']);
      } 
    } else {
      this.router.navigate(['settings']);
    }
  }


  // wird zur Zeit nicht verwendet.
  deleteCurrentGame()
  {
    this.currentGameService.deleteCurrentGame();
    this.router.navigate(['settings']);
  }









  getSportButtonClass1(){
    if (GlobalFunction.isEmpty(this.currentGameService?.currentGame)){
      return ""
    } else {

      if (this.currentGameService.currentGame.gameType == GameType.Football){

        return "football1"
      } else {
        return "hockey1"

      }
    }    
  }

  getSportButtonClass2(){
    if (GlobalFunction.isEmpty(this.currentGameService?.currentGame)){
      return ""
    } else {

      if (this.currentGameService.currentGame.gameType == GameType.Football){

        return "football2"
      } else {
        return "hockey2"

      }
    }    
  }

}
