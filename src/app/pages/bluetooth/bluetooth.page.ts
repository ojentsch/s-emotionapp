import { Component, OnInit} from '@angular/core';
import { BluetoothService } from 'src/app/shared/service/bluetooth.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { of, Subscription } from 'rxjs';
import { GlobalFunction } from 'src/app/shared/global-function';
import { DataStorageService } from 'src/app/shared/service/data-storage.service';
import { SettingsService } from 'src/app/shared/service/settings.service';

@Component({
  selector: 'app-bluetooth',
  templateUrl: './bluetooth.page.html',
  styleUrls: ['./bluetooth.page.scss'],
})
export class BluetoothPage implements OnInit {

  private subscription: Subscription;
  private subscriptionBluetooth: Subscription; 
 
  public isConnected;
  public isConnected2;
  public TimeTransferActive;
  public form : FormGroup;
  public bluetoothDevices: any[] = [];
  //public IsTimeTransfer = true;
  public bluetoothScan = false;
  public devices:any[];
  public checkForm : string;
  // the configuration service must be loaded first.
  constructor(
    public bluetoothService:BluetoothService,
    private formBuilder: FormBuilder,
    private dataStorageService: DataStorageService,
    private settingsService: SettingsService
  ) {
    this.checkForm = "";
    this.form = this.formBuilder.group({
      testSend: "",
      autoConnect:false,
      checkTimeTransfer:false,
      checkDummy:false
    });
    this.isConnected  = false;
   }


  ngOnInit() {

    this.checkForm = this.checkForm + " ngOnInit";
    this.bluetoothScan = false;
    this.subscription = this.bluetoothService.ConnectionChanges.subscribe(connected =>
    {       
        this.isConnected = connected;
    });

    this.bluetoothDevices = this.bluetoothService.devices;
    
    this.dataStorageService.getString('BluetoothAutoConnect').then((data: any) => {
      if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
        this.form.controls['autoConnect'].setValue(JSON.parse(data.value));
      }
    });



    
    this.subscriptionBluetooth = this.bluetoothService.BluetoothDevicesChanges.subscribe(devices =>
    {       
      this.bluetoothDevices = devices;
    });
  }

  ionViewWillEnter () {
    // check status of connection when entering the page.
    this.isConnected  = this.bluetoothService.IsConnected;

    // nur beim ersten mal
    this.form.controls['checkTimeTransfer'].setValue(this.settingsService.hasTimeTransfer);




    // this.dataStorageService.getString('HasTimeTransfer').then((data: any) => {
    //   if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
        
    //     this.form.controls['checkTimeTransfer'].setValue(JSON.parse(data.value));
    //     this.IsTimeTransfer =  JSON.parse(data.value)
    //     this.checkForm = this.checkForm + " checkTimeTransfer init";
    //    }
    // });


    // get value of transfering time from session if exists 
    // this.dataStorageService.getString('HasTimeTransfer').then((data: any) => {
    //   if ((!GlobalFunction.isEmpty(data) && data.value != null)) {
    //     this.TimeTransferActive =  JSON.parse(data.value)
    //     this.form.controls['checkTimeTransfer'].setValue(JSON.parse(data.value));
    //     this.checkForm = this.checkForm + " checkTimeTransfer from session";
    //   } else { // default value is true
    //     this.form.controls['checkTimeTransfer'].setValue(true);
    //     this.TimeTransferActive =  true;
    //     this.checkForm = this.checkForm + " checkTimeTransfer init";
    //   }
    // });
  }

  updateAutoConnect(e: any){
    this.bluetoothService.setBlueToothAutoConnect(e.detail.checked);
  }

  ngOnDestroy(){
    //this.subscription.unsubscribe();
    this.subscriptionBluetooth.unsubscribe();
  }

  scanBluetoothDevices(){
    this.bluetoothScan = true;
    this.bluetoothService.scan();
  }

  disconnect(){
    this.bluetoothService.disconnect();

  }

  clearBluetoothConnectTimer(){
    this.bluetoothService.clearConnectionIntervalTimer();
  }

  onChangeTimeTransfer(e: any){
    //this.IsTimeTransfer = e.detail.checked;

    this.checkForm = this.checkForm + " new value of HasTimeTransfer " + e.detail.checked; 
    this.settingsService.updateTimeTransfer(e.detail.checked);

  }




  checkIsConnected(){
    return this.bluetoothService.IsConnected;
  }



  hasSelectedDevice(){
    if (!GlobalFunction.isEmpty(this.bluetoothService.selectedDeviceID)){
      return true;
    }
    return false
  }

  hasDeviceName(device:any){
    if (GlobalFunction.isEmpty(device.name)){
      return false;
    }   
    return (true);
  }

  connect(deviceId){
    this.bluetoothService.connect(deviceId);
  }

  // async presentToasterMessage(str:string) {
  //   const toast = await this.toastCtrl.create({
  //     message: str,
  //     duration: 2000
  //   });
  //   toast.present();
  // }

}
