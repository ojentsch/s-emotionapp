import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeamNamesPage } from './team-names.page';

const routes: Routes = [
  {
    path: '',
    component: TeamNamesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeamNamesPageRoutingModule {}
