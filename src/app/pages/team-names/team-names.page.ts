import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { CurrentGame, TeamNames, DebugSettings } from 'src/app/shared/model/general-settings';
import { ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentGameService } from 'src/app/shared/service/current-game.service';
import { Subscription } from 'rxjs';
import { GlobalFunction } from 'src/app/shared/global-function';
import { ProtocolService } from 'src/app/shared/service/protocol.service';
import { DebugService } from 'src/app/shared/service/debug.service';
import { DataStorageService } from 'src/app/shared/service/data-storage.service';
import { SettingsService } from 'src/app/shared/service/settings.service';
@Component({
  selector: 'app-team-names',
  templateUrl: './team-names.page.html',
  styleUrls: ['./team-names.page.scss'],
})
export class TeamNamesPage implements OnInit {

  public form : FormGroup;
  private subscription: Subscription;
  public currentGame: CurrentGame;
  public teamId: number;
  private debugSettings: DebugSettings;
  constructor(
    private formBuilder: FormBuilder,
    private router:Router,
    public currentGameService: CurrentGameService,
    private route: ActivatedRoute,
    private protocolService:ProtocolService,
    private debugService:DebugService,
    private dataStorageService: DataStorageService,
    private settingsService:SettingsService
    ) {
    // this.form = this.formBuilder.group({
    //   teamName: ['', Validators.required]
    // });
    this.form = this.formBuilder.group({
      teamName: ['']
    });

   }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.teamId = params.id;
    });

    this.debugService.getdata().subscribe(
      (data) => {this.debugSettings = data;}
    );

  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }


  ionViewWillEnter(){


    this.subscription = this.currentGameService.getdata().subscribe(data =>
      {  
        this.currentGame = data;
        if (this.teamId == 2){
          this.form.controls['teamName'].setValue(this.settingsService.teamNames.guestTeamName);
        } else {
          this.form.controls['teamName'].setValue(this.settingsService.teamNames.homeTeamName);
        }
      });
    }

  save(){

    if (this.teamId == 2){
      this.currentGame.guestTeamName = this.form.controls['teamName'].value;
    } else {
      this.currentGame.homeTeamName = this.form.controls['teamName'].value;
    }

   let teamNames  = new TeamNames();
   teamNames.homeTeamName = this.currentGame.homeTeamName;
   teamNames.guestTeamName = this.currentGame.guestTeamName;

   //this.dataStorageService.setString('HasTimeTransfer', teamNames.homeTeamName);
    this.currentGameService.changeCurrentGameWithDependencies(this.currentGame);


    if (this.debugSettings.debugProtokolWorkflow){
      debugger;
    }
    this.protocolService.prot29h(teamNames);

    //console.log(this.form.value);
    this.router.navigate(['play']);
  }

  cancel(){
    this.router.navigate(['play']);
  }
  back(){
    this.router.navigate(['play']);
  }

  logForm(){
    
  }


  // console.log("Übertragung Protokoll Adresse 22h,

}
