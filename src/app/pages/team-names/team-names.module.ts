import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TeamNamesPageRoutingModule } from './team-names-routing.module';
import { TeamNamesPage } from './team-names.page';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    TeamNamesPageRoutingModule
  ],
  declarations: [TeamNamesPage]
})
export class TeamNamesPageModule {}
