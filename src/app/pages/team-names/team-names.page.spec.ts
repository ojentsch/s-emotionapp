import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TeamNamesPage } from './team-names.page';

describe('TeamNamesPage', () => {
  let component: TeamNamesPage;
  let fixture: ComponentFixture<TeamNamesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamNamesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TeamNamesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
