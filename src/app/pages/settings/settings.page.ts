import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { map } from 'rxjs/operators'
import { GeneralSettings, GameType, PeriodSettings, Setting22H, DebugSettings, ConfigSettings } from 'src/app/shared/model/general-settings';
import { SettingsService } from 'src/app/shared/service/settings.service';
import { CurrentGameService } from 'src/app/shared/service/current-game.service';


import { HockeyService } from 'src/app/shared/service/hockey.service';
import { ProtocolService } from 'src/app/shared/service/protocol.service';
import { DebugService } from 'src/app/shared/service/debug.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { BluetoothTimerService } from 'src/app/shared/service/bluetooth-timer.service';
import { BluetoothService } from 'src/app/shared/service/bluetooth.service';
import { ConfigurationService } from 'src/app/shared/service/configuration.service';
import { MessageService } from 'src/app/shared/service/message.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  public hockeySettings:PeriodSettings[];
  private classame = (<any>this).constructor.name;
  private debugSettings:DebugSettings;
  public generalSettings:GeneralSettings[];
  //private subscription: Subscription;


  constructor(
    private router:Router,
    private settingsService:SettingsService,
    private currentGameService:CurrentGameService,
    private hockeyService:HockeyService,
    private debugService:DebugService,
    ) { }

  ngOnInit() {

    this.debugService.getdata().subscribe(
      (data) => {this.debugSettings = data;}
    );

    this.settingsService.getGeneralSettings$().subscribe(
      (data) => {this.generalSettings = data;}
    );

  }


  ionViewWillEnter () {

    this.settingsService.getGeneralSettings$().subscribe(
      (data) => {this.generalSettings = data;}
    );
    //this.generalSettings=this.settingsService.generalSettings;
  }

  ngOnDestroy(){  
    //this.subscription.unsubscribe();
  }


  setSettings(settingId:GameType){
    
    this.currentGameService.finishGame();
    if (settingId == 1){ // fussball
       
      this.currentGameService.initFootballValues();
      this.router.navigate(['settings-advanced']);

    } else if (settingId == 2){ //hockey
 
      this.hockeySettings = this.hockeyService.hockeySettings;     
      this.currentGameService.initHockeyValues();
      this.router.navigate(['settings-advanced']);
    }
  }
  
}
