import { Component, OnInit } from '@angular/core';
import { CurrentGameService } from 'src/app/shared/service/current-game.service';
import { Subscription } from 'rxjs';
import { CurrentGame, TimePeriod, GameType, TimerDirection, SumPeriods, GameStatus, Setting21H, Setting22H, ExtraTimeAvailable, DebugSettings } from 'src/app/shared/model/general-settings';
import { Router } from '@angular/router';
import { GlobalFunction } from 'src/app/shared/global-function';
import { ProtocolService } from 'src/app/shared/service/protocol.service';
import { DebugService } from 'src/app/shared/service/debug.service';

@Component({
  selector: 'app-period',
  templateUrl: './period.page.html',
  styleUrls: ['./period.page.scss'],
})
export class PeriodPage implements OnInit {


  // Die Werte werden erst beim speichern auf das andere Dokument uebertragen
  private subscription: Subscription;
  public debugSettings:DebugSettings;
  private classame = (<any>this).constructor.name;
  public currentGame: CurrentGame;
  constructor(
    public currentGameService: CurrentGameService,
    private router: Router,
    private protocolService: ProtocolService,
    private debugService: DebugService
  ) { }

  ngOnInit() {   

    this.subscription = this.currentGameService.getdata().subscribe(data =>
    {  
      //debugger;

      this.currentGame = data;
    });

    this.debugService.getdata().subscribe(
      (data) => {this.debugSettings = data;}
    );
    
    // this.subscription = this.currentGameService.GameChanges.subscribe(data =>
    // {  
    //   this.currentGame = data;        
    // });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  ionViewWillEnter () {
    // nicht klar, ob noch 
    this.currentGame = this.currentGameService.currentGame;
    console.log(`${this.classame}  ionViewDidEnter`);

    if (this.currentGameService.currentGame.period == 0) {
      this.router.navigate(['settings-advanced']);
    }


  }


  isFootballNormal(){
    return (this.currentGame?.gameType == GameType.Football && this.currentGame?.timePeriod == TimePeriod.normal)
  }
  isFootballReneval(){
    return (this.currentGame?.gameType == GameType.Football && this.currentGame?.timePeriod == TimePeriod.renewal)
  }
  isHockey(){
    return (this.currentGameService?.currentGame?.gameType == GameType.Hockey)
  }

  getPeriodNumber(){
    
    
    if (GlobalFunction.isEmpty(this.currentGame)){
      return 0
    }
    if (this.currentGame.timePeriod == TimePeriod.normal){

      return this.currentGame.period
    } else {

      return (this.currentGame.period - this.currentGame.regularTimeNumPeriods);
    }
  }

  getTotalPeriods(){

    if (GlobalFunction.isEmpty(this.currentGame)){
      return 0
    }
    if (this.currentGame.timePeriod == TimePeriod.normal){

      return this.currentGame.regularTimeNumPeriods
    } else {
      return this.currentGame.extraTimeNumPeriods;
    }

  }

  getMinPerPeriod(){
    if (GlobalFunction.isEmpty(this.currentGame)){
      return 0
    }
    if (this.currentGame.timePeriod == TimePeriod.normal){

      return this.currentGame.regularTimeTargetMinutes
    } else {
      return this.currentGame.extraTimeTargetMinutes
    }

  }

  getEndMinPerGame(){

    if (GlobalFunction.isEmpty(this.currentGame)){
      return 0
    }

    if (this.currentGame.timerDirection== TimerDirection.Backward){
      return (0)

    } else if (this.currentGame.sumPeriods == SumPeriods.Yes) {
      if (this.currentGame.timePeriod == TimePeriod.normal){
        //let test = this.getPeriodNumber();
        // todo: noch nicht ganz korret. Es wird jeweils Ende der letzten Periode angezeigt.
        return (this.currentGame.regularTimeTargetMinutes * this.getPeriodNumber());
        } else {

          return (this.currentGame.extraTimeTargetMinutes *  this.getPeriodNumber());
        }
      
    } else {
      if (this.currentGame.timePeriod == TimePeriod.normal){
      return this.currentGame.regularTimeTargetMinutes;
      } else {
        return this.currentGame.extraTimeTargetMinutes;
      }

    }


  }


  periodAdd(){


    //if this.currentGame.period 


    let maxPeriods = this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods;
    

    // max Perioden mit verlängerung erreicht
    if (this.currentGame.period >= maxPeriods){
      return;
    }
    
    this.currentGame.currentSec = 0;
    this.currentGame.period++;
   
    if (this.currentGame.period > this.currentGame.regularTimeNumPeriods){
      this.currentGame.timePeriod = TimePeriod.renewal;
    } else {
      this.currentGame.timePeriod = TimePeriod.normal;
    }

    
    if (this.currentGame.timerDirection == TimerDirection.Forward){
      this.currentGame.currentMinutes = 0;    
    } else {      
      if (this.currentGame?.timePeriod == TimePeriod.renewal){
        this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes
      } else {
        this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
      }
    }

  }
  
  periodSubstract(){

    if (this.currentGame.period < 1){
      return;
    }

    this.currentGame.period--;
    this.currentGame.currentSec = 0;

    if (this.currentGame.period <= this.currentGame.regularTimeNumPeriods){
      this.currentGame.timePeriod = TimePeriod.normal;
    }

    if (this.currentGame.timerDirection == TimerDirection.Forward){
      this.currentGame.currentMinutes = 0;
      
    } else {      
      if (this.currentGame?.timePeriod == TimePeriod.renewal){
        this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes
      } else {
        this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
      }
    }
  }

  goBack(){

    this.router.navigate(['play']);

  }

  periodAddDisabled(){

    if (GlobalFunction.isEmpty(this.currentGame)){
      return true
    }

    let maxPeriods = 0;
    if (this.currentGame.extraTimeAvailable == ExtraTimeAvailable.No) {
      maxPeriods = this.currentGame.regularTimeNumPeriods
    } else {
      maxPeriods = this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods;
    }

    if (this.currentGame.period >= maxPeriods){
  
      return true;
    }

    
    return false;
  }

  periodSubstractDisabled(){

    if (GlobalFunction.isEmpty(this.currentGame)){

      return true;
    }
    
    if (GlobalFunction.isEmpty(this.currentGame?.period)) {

      return true
    }

    return (this.currentGame.period <= 1)

  }


  // 
  getSportReleateMinutes(){ 
    if (GlobalFunction.isEmpty(this.currentGame)){
      return (0)
    }

    return (this.currentGameService.getSportReleateMinutesValues(this.currentGame));

  }


  minuteSubstractDisabled(){
    
    if (GlobalFunction.isEmpty(this.currentGame?.currentMinutes)) {

      return (true);
    }

    return (false);
  }



  secDisabled(){

    if (GlobalFunction.isEmpty(this.currentGame?.currentSec)) {
      return (true);
    }


    if (this.currentGame.timePeriod == TimePeriod.normal){
      if (this.currentGame.currentMinutes >= this.currentGame.regularTimeTargetMinutes){    
        return true
      }
    } else {
      if (this.currentGame.currentMinutes >= this.currentGame.extraTimeTargetMinutes){    
        return true
      }
    }


    return false;

    //return (this.currentGame?.currentSec == 0)

  }



  // wenn max ueberschritten, wieder start bei 0. 
  minuteAdd(){


  // 

    if (this.currentGame.timePeriod == TimePeriod.normal){
      if (this.currentGame.currentMinutes < this.currentGame.regularTimeTargetMinutes){    
        this.currentGame.currentMinutes++;
        

        if (this.currentGame.currentMinutes >= this.currentGame.regularTimeTargetMinutes){ 
          this.currentGame.currentSec = 0;
        }
        //

      } else { // erneut starten bei 0
        this.currentGame.currentMinutes = 0;
        this.currentGame.currentSec = 0;
      }
    
    } else { // verlängerung
      if (this.currentGame.currentMinutes < this.currentGame.extraTimeTargetMinutes){    
        this.currentGame.currentMinutes++;  


        if (this.currentGame.currentMinutes >= this.currentGame.extraTimeTargetMinutes){ 
          this.currentGame.currentSec = 0;
        }


      } else {
        this.currentGame.currentMinutes = 0;
        this.currentGame.currentSec = 0;
      }


    }

  }

  minuteSubstract(){

    if (this.currentGame.currentMinutes > 0){

      this.currentGame.currentMinutes--;
    } else {
      if (this.currentGame.timePeriod == TimePeriod.normal){
        this.currentGame.currentMinutes = this.currentGame.regularTimeTargetMinutes;
        this.currentGame.currentSec = 0;
      } else {
        this.currentGame.currentSec = 0;
        this.currentGame.currentMinutes = this.currentGame.extraTimeTargetMinutes;
      } 
    }
  }

  secondAdd(){



    // wenn Endspielzeit erreicht vorwärts, dann werden die sekunden nicht mehr hochgezählt z.B 45:00 max wert. 
    if (this.currentGame.timerDirection == TimerDirection.Forward){
      if (this.currentGame.timePeriod == TimePeriod.normal){
        if (this.currentGame.currentMinutes >= this.currentGame.regularTimeTargetMinutes){
          return;
        }   
      } else {
        if (this.currentGame.currentMinutes >= this.currentGame.extraTimeTargetMinutes){
          return;
        }
      }

    } 
    

    if (this.currentGame.currentSec < 59){

      this.currentGame.currentSec++;
    } else {

      this.currentGame.currentSec = 0;
    }
    

  }


  secondSubstract(){


    // moeglichkeit von 
    if (this.currentGame.currentSec > 0){
      this.currentGame.currentSec--;

    } else {

      if (this.currentGame.timePeriod == TimePeriod.normal){
        if (this.currentGame.currentMinutes >= this.currentGame.regularTimeTargetMinutes){
          return;
        }   
      } else {
        if (this.currentGame.currentMinutes >= this.currentGame.extraTimeTargetMinutes){
          return;
        }
      }
      this.currentGame.currentSec = 59;
    }
  }


  save(){

    // ausganssituation für die einstellung
    this.currentGame.gameStatus = GameStatus.pause;
    let relatedPeriod = 0;
    let relatedMinutes = 0;
    if (this.currentGame.period > this.currentGame.regularTimeNumPeriods)
    {
      this.currentGame.timePeriod = TimePeriod.renewal;
      relatedPeriod = this.currentGame.regularTimeNumPeriods + this.currentGame.extraTimeNumPeriods;
      relatedMinutes = this.currentGame.extraTimeTargetMinutes
    } else {
      this.currentGame.timePeriod = TimePeriod.normal;
      relatedPeriod = this.currentGame.regularTimeNumPeriods;
      relatedMinutes = this.currentGame.regularTimeTargetMinutes;
    }

    if (this.currentGame.timerDirection == TimerDirection.Backward){
      if ((this.currentGame.currentSec == 0)  && (this.currentGame.currentMinutes == 0)){
        debugger;  
        if (this.currentGame.period == relatedPeriod){
            this.currentGame.gameStatus = GameStatus.gameFinished;
          }  else {
            this.currentGame.gameStatus = GameStatus.periodFinished;
          }
      }
    } else { // forward
      if ((this.currentGame.currentSec == 0)  && (this.currentGame.currentMinutes == relatedMinutes)){
        if (this.currentGame.period == relatedPeriod){
          this.currentGame.gameStatus = GameStatus.gameFinished;
        }  else {
          this.currentGame.gameStatus = GameStatus.periodFinished;
        }

      } 

    }




    //gegebenfalls nicht mehr notwendig.
    this.currentGameService.changeCurrentGameWithDependencies(this.currentGame);



    // ändern des Protokolls der sekündlchen Zeitübertragung
    // erst mit lokaler variable arbeiten
    let setting21H = new Setting21H();
    
    
    // Übernahme der bestehenden Einstellungen


    if (this.debugSettings.debugProtokolWorkflow){
      debugger;
    }
    setting21H.flagMid = this.protocolService.setting21H.flagMid;
    setting21H.signalEnd = this.protocolService.setting21H.signalEnd;
    setting21H = GlobalFunction.setSettings21HFromCurrentGame(this.currentGame, setting21H)
    

    // Damit müssten die Werte bei der nächsten Protokollübertragung aktualisiert werden
    this.protocolService.setting21H = setting21H;
  

    // // senden der Periode. num ausgelagert
    // // beim Fussball vermutlich keine Bedetung.
    // // Wird aber trotzdem übertragen.
    // let setting22H = new Setting22H();
    // setting22H.period = this.currentGame.period.toString();
    // this.protocolService.prot22h(setting22H);


    this.router.navigate(['play']);


    // check if status of game should be changed to 

  }

  cancel(){
    this.currentGame = this.currentGameService.currentGame;
    this.router.navigate(['play']);
  }

}
