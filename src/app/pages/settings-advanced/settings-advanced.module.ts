import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingsAdvancedPageRoutingModule } from './settings-advanced-routing.module';

import { SettingsAdvancedPage } from './settings-advanced.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    SettingsAdvancedPageRoutingModule
  ],
  declarations: [SettingsAdvancedPage]
})
export class SettingsAdvancedPageModule {}
