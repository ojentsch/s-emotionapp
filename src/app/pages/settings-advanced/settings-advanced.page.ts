import { Component, OnInit } from '@angular/core';
import { PeriodSettings, GameType, Setting21H, Setting22H, DebugSettings, CurrentGame, ExtraTimeSettings, TeamNames, TimerDirection } from 'src/app/shared/model/general-settings';
import { FootballService } from 'src/app/shared/service/football.service';
import { Router } from '@angular/router';
import { CurrentGameService } from 'src/app/shared/service/current-game.service';
import { HockeyService } from 'src/app/shared/service/hockey.service';
import { ProtocolService } from 'src/app/shared/service/protocol.service';
import { GlobalFunction } from 'src/app/shared/global-function';
import { DebugService } from 'src/app/shared/service/debug.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DataStorageService } from 'src/app/shared/service/data-storage.service';
import { SettingsService } from 'src/app/shared/service/settings.service';



@Component({
  selector: 'app-settings-advanced',
  templateUrl: './settings-advanced.page.html',
  styleUrls: ['./settings-advanced.page.scss'],
})
export class SettingsAdvancedPage implements OnInit {


  private classame = (<any>this).constructor.name;

  public regularTimeTargetMinutes: number = 0;
  public regularTimeNumPeriods: number= 0;
  public extraTimeTargetMinutes: number = 0;
  public debugSettings:DebugSettings;
  public advancedSportSettings:PeriodSettings[];
  public hockeySettings:PeriodSettings[];
  public form : FormGroup;
  private teamNames: TeamNames;
  private forwardCounter: boolean = false;

  constructor(
    public footballService:FootballService,
    private hockeyService:HockeyService,
    private router:Router,
    private currentGameService:CurrentGameService,
    private protocolService:ProtocolService,
    private debugService:DebugService,
    private formBuilder: FormBuilder,
    private settingsService:SettingsService
  ) {

    this.debugService.getdata().subscribe(
      (data) => {this.debugSettings = data;}
    );

    this.form = this.formBuilder.group({
      periodName: "",
      isForwardCounter:0
    });


   }

  ngOnInit() {
  
    this.teamNames = new TeamNames;
  }


  ionViewWillEnter () {


    if (GlobalFunction.isEmpty(this.currentGameService.currentGame)){
      this.router.navigate(['settings']);
    }

    if (GlobalFunction.isEmpty(this.currentGameService.currentGame.gameType)){
      this.router.navigate(['settings']);
    }
    

    // his.currentGame.gameType
    
    console.log(`${this.classame}  ionViewDidEnter`);
    if (this.currentGameService.currentGame.gameType == GameType.Hockey){
      this.hockeyService.getdata().subscribe(
        (data) => {this.advancedSportSettings = data;}
      );

      
    } else {
      this.footballService.getdata().subscribe(
        (data) => {this.advancedSportSettings = data;}
      );
      this.regularTimeNumPeriods = 2;
    }

  }



  setSettings(periodSettingId:number){


    if (this.currentGameService.currentGame.gameType == GameType.Football){
      
      this.currentGameService.initFootballDetailValues(periodSettingId);
      // Kennung Fußball
      if (this.debugSettings.debugProtokolWorkflow){
        debugger;
      }
      this.protocolService.prot20h(0x31,0x30);
    } else { //hockey


      let advancedSetting = this.advancedSportSettings.find(x=> x.periodSettingId == periodSettingId);
      if (this.debugSettings.debugProtokolWorkflow){
        debugger;
      }
      this.currentGameService.initHockeyDetailValues(periodSettingId);
  
      //nachsteuern des Timers
      
      if (!GlobalFunction.isEmpty(advancedSetting.timerDirection)){  
        debugger;
        this.currentGameService.currentGame.timerDirection = advancedSetting.timerDirection;
        if (this.currentGameService.currentGame.timerDirection == TimerDirection.Forward){
          this.currentGameService.currentGame.currentMinutes = 0;
        }        
      } else if (this.forwardCounter){
        debugger;
        this.currentGameService.currentGame.timerDirection = TimerDirection.Forward;
        this.currentGameService.currentGame.currentMinutes = 0
      }
      
   
      //
      this.protocolService.prot20h(0x30,0x36);
      // sicherstellen, das die Zeit wieder auf 0 gestellt wird!
    }

    if (this.debugSettings.debugProtokolWorkflow){
      debugger;
    }
    // senden der Periode
    // beim Fussball keine Bedetung, wird aber trotzdem mit übertragen  
    let setting22H = new Setting22H();
    setting22H.period = "1";
    this.protocolService.prot22h(setting22H);

    
    // Die Uhrzeit aus den Settings wieder aktualisieren.
    let setting21H = new Setting21H();
    setting21H.flagMid = "2"; // Pause 
    setting21H.signalEnd = "0"; // Kein Signal
    setting21H = GlobalFunction.setSettings21HFromCurrentGame(this.currentGameService.currentGame, setting21H)
    
    // Damit müssten die Werte bei der nächsten Protokollübertragung aktualisiert werden
    this.protocolService.setting21H = setting21H;

    // refresh the goals. 
    // there might be settings from the old game.
    this.protocolService.prot23h(this.currentGameService.currentGame.homeGoals,this.currentGameService.currentGame.guestGoals);
    

    this.teamNames.homeTeamName =  this.settingsService.teamNames.homeTeamName;
    this.teamNames.guestTeamName = this.settingsService.teamNames.guestTeamName

    this.protocolService.prot29h(this.teamNames);

    this.router.navigate(['play']);
  
  }



    goToSettings(){

      this.router.navigate(['settings']);

    }

    isUndefined(){
      if (!this.isFootball() && !this.isHockey()){

        return true;
      }
      return false


    }

    getSportCellClass1(){
      if (this.currentGameService.currentGame.gameType == GameType.Hockey){
        return "hockey1 cell-class"
      } else {
        return "football1 cell-class"
      }   
    }

    getSportButtonClass1(){
      if (GlobalFunction.isEmpty(this.currentGameService?.currentGame)){
        return ""
      } else {
  
        if (this.currentGameService.currentGame.gameType == GameType.Football){
  
          return "football1"
        } else {
          return "hockey1"
  
        }
      }    
    }

    getSportButtonClass2(){
      if (GlobalFunction.isEmpty(this.currentGameService?.currentGame)){
        return ""
      } else {
  
        if (this.currentGameService.currentGame.gameType == GameType.Football){
  
          return "football2"
        } else {
          return "hockey2"
  
        }
      }    
    }
    

    isFootball(){
      return (this.currentGameService?.currentGame?.gameType == GameType.Football)
    }
  
    isHockey(){
      return (this.currentGameService?.currentGame?.gameType == GameType.Hockey)
    }

    minNormalTimeAdd(){

      if (this.regularTimeTargetMinutes > 48){
        this.regularTimeTargetMinutes = 1;
      } else {
        this.regularTimeTargetMinutes++;
      }
    }

    minNormalTimeSubstract(){
      if (this.regularTimeTargetMinutes < 1){
        this.regularTimeTargetMinutes = 49;
      } else {
        this.regularTimeTargetMinutes--;
      }
      
    }

    minExtraTimeAdd(){

      if (this.extraTimeTargetMinutes > 48){
        this.extraTimeTargetMinutes = 1;
      } else {
        this.extraTimeTargetMinutes++;
      }

    }
    minExtraTimeSubstract(){
  
      if (this.extraTimeTargetMinutes < 1){
        this.extraTimeTargetMinutes = 49;
      } else {
        this.extraTimeTargetMinutes--;
      }
    }

    periodAdd(){
      this.regularTimeNumPeriods++;

    }
    periodSubstract(){
  
      this.regularTimeNumPeriods--;
    }

    deleteSetting(settingId: number){
      if (this.currentGameService.currentGame.gameType == GameType.Hockey){
        this.hockeyService.deleteSetting(settingId);
      } else {
        this.footballService.deleteSetting(settingId);
      }
    }

    
    saveNewGame(){

        // muss Größer 0 sein.
        if (this.regularTimeTargetMinutes == 0){
          return;
        }

        let periodName = this.form.controls['periodName'].value;
        this.forwardCounter = this.form.controls['isForwardCounter'].value;

        debugger;
        let normalTimeSetting = new PeriodSettings();
        normalTimeSetting.durationMin = this.regularTimeTargetMinutes;
        normalTimeSetting.settingName = periodName;
        if (this.currentGameService.currentGame.gameType == GameType.Hockey){

          // Ist nur für Hockey dynamisch
          normalTimeSetting.periodAmounts = this.regularTimeNumPeriods;

          this.hockeyService.addNewNormalPeriod(normalTimeSetting);
          this.setSettings(this.hockeyService.newSettingId);




        } else {
          // fussball json hinzufügen

          normalTimeSetting.periodAmounts = this.regularTimeNumPeriods;

          this.footballService.addNewNormalPeriod(normalTimeSetting);
          // Standard 2 Halbzeiten
          
          if (this.extraTimeTargetMinutes > 0){

            let extraTimeSetting = new ExtraTimeSettings();

            // Standard 2 Verlängerungen
            extraTimeSetting.periodAmounts = 2;
            extraTimeSetting.durationMin = this.extraTimeTargetMinutes;
            this.footballService.addNewExtraPeriod(extraTimeSetting);
          }

          this.setSettings(this.footballService.newSettingId);

        }
        
    }
}
