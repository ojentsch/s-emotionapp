import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsAdvancedPage } from './settings-advanced.page';

const routes: Routes = [
  {
    path: '',
    component: SettingsAdvancedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsAdvancedPageRoutingModule {}
