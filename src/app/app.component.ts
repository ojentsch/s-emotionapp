import { Component, OnInit, ViewChild } from '@angular/core';

import { IonRouterOutlet,Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core'; 
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { PowerManagement } from '@ionic-native/power-management/ngx';

import { Plugins } from '@capacitor/core';
const { App } = Plugins;




@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  
  
  
  @ViewChild(IonRouterOutlet, { static: false }) routerOutlet: IonRouterOutlet;
  
  public selectedIndex = 0;

  private subscriptionRes: Subscription;
  public appPages = [

    {
      title: 'Sportauswahl',
      url: 'settings',
      icon: 'settings'
    },
    {
      title: 'Spiel',
      url: 'play',
      icon: 'caret-forward'
    },
    {
      title: 'Konfiguration',
      url: 'bluetooth',
      icon: 'bluetooth'
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private powerManagement: PowerManagement,

  ) {
    this.initializeApp();
  
    // finish app. 
    this.platform.backButton.subscribeWithPriority(-1, () => {
       if (!this.routerOutlet.canGoBack()) {
         App.exitApp();
       }
    });
  
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.powerManagement.dim();
      this.powerManagement.setReleaseOnPause(false);
    });

    this.translate.setDefaultLang('de');
  }

  ngOnInit() {
  
    //this.subscriptionRes = this.translate.get(['NAECHSTE_PERIODE','UEBERTRAGUNG_BEENDEN','SPIEL_BEENDEN','JA','NEIN']).
    // subscribe((translation: [string]) => {  
    //   this.sportauswahl_res = translation['SPIEL_BEENDEN'];
    //   this.spiel_res = translation['JA'];
    //   this.bluetooth_res = translation['NEIN'];
    // });
  
  }

  ngOnDestroy(){
    //this.subscriptionRes.unsubscribe();
  }

}
